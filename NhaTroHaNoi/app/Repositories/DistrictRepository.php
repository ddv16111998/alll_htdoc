<?php
namespace App\Repositories;
use App\Models\District;
class DistrictRepository
{
    public function getDataIndex()
    {
        $data = District::whereNull('deleted_at')->get();
        return $data;
    }
}
