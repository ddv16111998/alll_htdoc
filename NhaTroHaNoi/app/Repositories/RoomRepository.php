<?php
namespace App\Repositories;
use App\Models\Room;

class RoomRepository
{
    public function getRoomByCountView()
    {
        $data = Room::orderBy('count_views','desc')->take(8)->get();
        return $data;
    }
    public function getDataIndex()
    {
        $data = Room::whereNull('deleted_at')->where('approve',1)->orderBy('created_at','DESC')->paginate(8);
        return $data;
    }
    public function getDataById($id)
    {
        $data = Room::find($id);
        return $data;
    }
    public function updateOrCreate($id,$data)
    {
        $data = Room::updateOrCreate(['id'=>$id],$data);
        if($data)
        {
            return true;
        }
        return false;
    }
    public function destroy($id)
    {
        $data = Room::find($id);
        $data->delete();
        if($data->trashed())
        {
            return true;
        }
        return false;
    }
    public function search($district_id,$commune_id,$cate_id,$minprice,$maxprice)
    {
        $data = Room::whereNull('deleted_at')->where([['district_id',$district_id],['commune_id',$commune_id],['cate_id',$cate_id],['price','>',$minprice],['price','<',$maxprice]])->get();
        return $data;
    }
    public function getDataRoomNeedApproved()
    {
        $data = Room::whereNull('deleted_at')->where('approve',0)->get();
        return $data;
    }
    public function getCountRoomNeedApproved()
    {
        $data = Room::whereNull('deleted_at')->where('approve',0)->count();
        return $data;
    }
    public function getRoomRandom()
    {
        $data = Room::whereNull('deleted_at')->where('approve',1)->inRandomOrder()->limit(6)->get();
        return $data;
    }
    public function getRoomByCate($cate_id){
        $data = Room::whereNull('deleted_at')->where([['approve',1],['cate_id',$cate_id]])->orderBy('created_at','DESC')->paginate(8);
        return $data;
    }
    public function getRoomByFilter($title,$order)
    {
        $data = Room::whereNull('deleted_at')->where('approve',1)->orderBy($title,$order)->paginate(8);
        return $data;
    }
    public function getRoomByCateAndFilter($cate_id,$title,$order)
    {
        $data = Room::whereNull('deleted_at')->where([['approve',1],['cate_id',$cate_id]])->orderBy($title,$order)->paginate(8);
        return $data;
    }


}
