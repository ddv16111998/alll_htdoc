               <aside id="sidebar">


                    <div id="sidebar-wrap">

                        <div class="panel-group slim-scroll" role="tablist">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#sidebarNav">
                                            Navigation <i class="fa fa-angle-up"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="sidebarNav" class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">


                                        <!-- ===================================================
                                        ================= NAVIGATION Content ===================
                                        ==================================================== -->
                                        <ul id="navigation">
                                            <li class="{{Request::is('admin/dashboard') ? "active" : ""}}"><a href="{{ route('admin.dashboard.index') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                                            <li>
                                                <a role="button" tabindex="0"><i class="fa fa-envelope-o"></i> <span>Mail</span> <span class="badge bg-lightred">6</span></a>
                                                <ul>
                                                    <li><a href="mail-inbox.html"><i class="fa fa-caret-right"></i> Inbox</a></li>
                                                    <li><a href="mail-compose.html"><i class="fa fa-caret-right"></i> Compose Mail</a></li>
                                                    <li><a href="mail-single.html"><i class="fa fa-caret-right"></i> Single Mail</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a role="button" tabindex="0"><i class="fa fa-list"></i><span class="badge bg-lightred">{{$countApproved}}</span> <span>Phê duyệt / Phòng trọ </span></a>
                                                <ul>

                                                    <li class="{{Request::is('admin/approved/list') ? "active" : ""}}"><a href="{{route('admin.approved.list')}}"><i class="fa fa-caret-right"></i>Phê Duyệt<span class="badge badge-danger">{{$countApproved}}</span></a></li>
                                                    <li class="{{Request::is('admin/rooms/list') ? "active" : ""}}"><a href="form-common.html"><i class="fa fa-caret-right"></i>Danh sách</a></li>
                                                </ul>
                                            </li>
                                            <li class="">
                                                <a role="button" tabindex="0"><i class="fa fa-user"></i> <span>Danh sách quản trị viên</span></a>
                                                <ul>
                                                    <li class="{{Request::is('admin/admin/create') ? "active" : ""}}"><a href="{{route('admin.admin.create')}}"><i class="fa fa-caret-right"></i> Thêm mới</a></li>
                                                    <li class="{{Request::is('admin/admin/index') ? "active" : ""}}"><a href="{{route('admin.admin.index')}}"><i class="fa fa-caret-right"></i> Danh sách quản trị viên</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a role="button" tabindex="0"><i class="fa fa-user"></i> <span>Danh sách người dùng</span></a>
                                                <ul>
                                                    <li class="{{Request::is('admin/user/index') ? "active" : ""}}"><a href="{{route('admin.user.index')}}"><i class="fa fa-caret-right"></i> Danh sách người dùng</a></li>
                                                </ul>
                                            </li>
{{--                                            <li>--}}
{{--                                                <a role="button" tabindex="0"><i class="fa fa-shopping-cart"></i> <span>Shop</span> <span class="label label-success">new</span></a>--}}
{{--                                                <ul>--}}
{{--                                                    <li><a href="shop-orders.html"><i class="fa fa-caret-right"></i> Orders</a></li>--}}
{{--                                                    <li><a href="shop-single-order.html"><i class="fa fa-caret-right"></i> Single Order</a></li>--}}
{{--                                                    <li><a href="shop-products.html"><i class="fa fa-caret-right"></i> Products</a></li>--}}
{{--                                                    <li><a href="shop-single-product.html"><i class="fa fa-caret-right"></i> Single Product</a></li>--}}
{{--                                                    <li><a href="shop-invoices.html"><i class="fa fa-caret-right"></i> Invoices</a></li>--}}
{{--                                                    <li><a href="shop-single-invoice.html"><i class="fa fa-caret-right"></i> Single Invoice</a></li>--}}
{{--                                                </ul>--}}
{{--                                            </li>--}}
                                            <li>
                                                <a role="button" tabindex="0"><i class="fa fa-table"></i> <span>Danh mục</span></a>
                                                <ul>
                                                    <li class="{{Request::is('admin/cate/create') ? "active" : ""}}"><a href="{{route('admin.cate.create')}}"><i class="fa fa-caret-right"></i>Thêm mới</a></li>
                                                    <li class="{{Request::is('admin/cate/index') ? "active" : ""}}"><a href="{{route('admin.cate.index')}}"><i class="fa fa-caret-right"></i>Danh sách</a></li>
                                                </ul>
                                            </li>
{{--                                            <li>--}}
{{--                                                <a role="button" tabindex="0"><i class="fa fa-desktop"></i> <span>Extra Pages</span></a>--}}
{{--                                                <ul>--}}
{{--                                                    <li><a href="login.html"><i class="fa fa-caret-right"></i> Login Page</a></li>--}}
{{--                                                    <li><a href="signup.html"><i class="fa fa-caret-right"></i> Signup Page</a></li>--}}
{{--                                                    <li><a href="forgotpass.html"><i class="fa fa-caret-right"></i> Forgot Password Page</a></li>--}}
{{--                                                    <li><a href="page404.html"><i class="fa fa-caret-right"></i> Page 404</a></li>--}}
{{--                                                    <li><a href="page500.html"><i class="fa fa-caret-right"></i> Page 500</a></li>--}}
{{--                                                    <li><a href="page-offline.html"><i class="fa fa-caret-right"></i> Page Offline</a></li>--}}
{{--                                                    <li><a href="locked.html"><i class="fa fa-caret-right"></i> Locked Screen</a></li>--}}
{{--                                                    <li><a href="gallery.html"><i class="fa fa-caret-right"></i> Gallery</a></li>--}}
{{--                                                    <li><a href="timeline.html"><i class="fa fa-caret-right"></i> Timeline</a></li>--}}
{{--                                                    <li><a href="chat.html"><i class="fa fa-caret-right"></i> Chat</a></li>--}}
{{--                                                    <li><a href="search-results.html"><i class="fa fa-caret-right"></i> Search Results</a></li>--}}
{{--                                                    <li><a href="profile.html"><i class="fa fa-caret-right"></i> Profile Page</a></li>--}}
{{--                                                </ul>--}}
{{--                                            </li>--}}
{{--                                            <li>--}}
{{--                                                <a role="button" tabindex="0"><i class="fa fa-delicious"></i> <span>Layouts</span></a>--}}
{{--                                                <ul>--}}
{{--                                                    <li><a href="layout-boxed.html"><i class="fa fa-caret-right"></i> Boxed layout</a></li>--}}
{{--                                                    <li><a href="layout-fullwidth.html"><i class="fa fa-caret-right"></i> Full-width layout</a></li>--}}
{{--                                                    <li><a href="layout-sidebar-sm.html"><i class="fa fa-caret-right"></i> Small sidebar</a></li>--}}
{{--                                                    <li><a href="layout-sidebar-xs.html"><i class="fa fa-caret-right"></i> Extra-small sidebar</a></li>--}}
{{--                                                    <li><a href="layout-offcanvas.html"><i class="fa fa-caret-right"></i> Off-canvas sidebar  <span class="label label-success">new</span></a></li>--}}
{{--                                                    <li><a href="layout-hz-menu.html"><i class="fa fa-caret-right"></i> Horizontal menu</a></li>--}}
{{--                                                    <li><a href="layout-rtl.html"><i class="fa fa-caret-right"></i> RTL layout</a></li>--}}
{{--                                                </ul>--}}
{{--                                            </li>--}}
{{--                                            <li>--}}
{{--                                                <a role="button" tabindex="0"><i class="fa fa-file-o"></i> <span>Front Themes</span> <span class="label label-success">new</span></a>--}}
{{--                                                <ul>--}}
{{--                                                    <li><a href="http://www.tattek.sk/minovate-corp" target="_blank"><i class="fa fa-caret-right"></i> Corporate</a></li>--}}
{{--                                                    <li><a href="http://www.tattek.sk/minovate-commerce" target="_blank"><i class="fa fa-caret-right"></i> Commerce</a></li>--}}
{{--                                                </ul>--}}
{{--                                            </li>--}}
                                            <li>
                                                <a role="button" tabindex="0"><i class="fa fa-map-marker"></i> <span>Quận/Huyện</span></a>
                                                <ul>
                                                    <li class="{{Request::is('admin/district/create') ? "active" : ""}}"><a href="#"><i class="fa fa-caret-right"></i>Thêm mới</a></li>
                                                    <li class="{{Request::is('admin/district/index') ? "active" : ""}}"><a href="{{route('admin.district.index')}}"><i class="fa fa-caret-right"></i>Danh sách</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a role="button" tabindex="0"><i class="fa fa-map-marker"></i> <span>Phường</span></a>
                                                <ul>
                                                    <li class="{{Request::is('admin/commune/create') ? "active" : ""}}"><a href="#"><i class="fa fa-caret-right"></i>Thêm mới</a></li>
                                                    <li class="{{Request::is('admin/commune/index') ? "active" : ""}}"><a href="{{route('admin.commune.index')}}"><i class="fa fa-caret-right"></i>Danh sách</a></li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a role="button" href="{{route('room.index')}}" tabindex="0"><i class="fa fa-arrow-circle-right"></i> <span>Websize</span></a>
                                            </li>
{{--                                            <li><a href="calendar.html"><i class="fa fa-calendar-o"></i> <span>Calendar</span> <span class="label label-success">new events</span></a></li>--}}
{{--                                            <li><a href="charts.html"><i class="fa fa-bar-chart-o"></i> <span>Charts & Graphs</span></a></li>--}}

{{--                                            <li>--}}
{{--                                                <a role="button" tabindex="0"><i class="fa fa-magic"></i> <span>Menu Levels</span></a>--}}
{{--                                                <ul>--}}
{{--                                                    <li>--}}
{{--                                                        <a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 1.1</a>--}}
{{--                                                        <ul>--}}
{{--                                                            <li><a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 2.1</a></li>--}}
{{--                                                            <li><a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 2.2</a></li>--}}
{{--                                                            <li>--}}
{{--                                                                <a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 2.3</a>--}}
{{--                                                                <ul>--}}
{{--                                                                    <li><a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 3.1</a></li>--}}
{{--                                                                    <li><a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 3.2</a></li>--}}
{{--                                                                </ul>--}}
{{--                                                            </li>--}}
{{--                                                        </ul>--}}
{{--                                                    </li>--}}
{{--                                                    <li>--}}
{{--                                                        <a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 1.2</a>--}}
{{--                                                        <ul>--}}
{{--                                                            <li>--}}
{{--                                                                <a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 2.1</a>--}}
{{--                                                                <ul>--}}
{{--                                                                    <li><a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 3.1</a></li>--}}
{{--                                                                    <li>--}}
{{--                                                                        <a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 3.2</a>--}}
{{--                                                                        <ul>--}}
{{--                                                                            <li><a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 4.1</a></li>--}}
{{--                                                                            <li><a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 4.2</a></li>--}}
{{--                                                                        </ul>--}}
{{--                                                                    </li>--}}
{{--                                                                    <li><a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 3.3</a></li>--}}
{{--                                                                </ul>--}}
{{--                                                            </li>--}}
{{--                                                            <li><a role="button" tabindex="0"><i class="fa fa-caret-right"></i> Menu Level 2.2</a></li>--}}
{{--                                                        </ul>--}}
{{--                                                    </li>--}}
{{--                                                </ul>--}}
{{--                                            </li>--}}


{{--                                        </ul>--}}
                                        <!--/ NAVIGATION Content -->

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel charts panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#sidebarCharts">
                                            Orders Summary <i class="fa fa-angle-up"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="sidebarCharts" class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">
                                        <div class="summary">

                                            <div class="media">
                                                <a class="pull-right" role="button" tabindex="0">
                                                    <span class="sparklineChart"
                                                          values="5, 8, 3, 4, 6, 2, 1, 9, 7"
                                                          sparkType="bar"
                                                          sparkBarColor="#92424e"
                                                          sparkBarWidth="6px"
                                                          sparkHeight="36px">
                                                    Loading...</span>
                                                </a>
                                                <div class="media-body">
                                                    This week sales
                                                    <h4 class="media-heading">26, 149</h4>
                                                </div>
                                            </div>

                                            <div class="media">
                                                <a class="pull-right" role="button" tabindex="0">
                                                    <span class="sparklineChart"
                                                          values="2, 4, 5, 3, 8, 9, 7, 3, 5"
                                                          sparkType="bar"
                                                          sparkBarColor="#397193"
                                                          sparkBarWidth="6px"
                                                          sparkHeight="36px">
                                                    Loading...</span>
                                                </a>
                                                <div class="media-body">
                                                    This week balance
                                                    <h4 class="media-heading">318, 651</h4>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel settings panel-default">
                                <div class="panel-heading" role="tab">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#sidebarControls">
                                            General Settings <i class="fa fa-angle-up"></i>
                                        </a>
                                    </h4>
                                </div>
                                <div id="sidebarControls" class="panel-collapse collapse in" role="tabpanel">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <div class="row">
                                              <label class="col-xs-8 control-label">Switch ON</label>
                                              <div class="col-xs-4 control-label">
                                                <div class="onoffswitch greensea">
                                                  <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="switch-on" checked="">
                                                  <label class="onoffswitch-label" for="switch-on">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                  </label>
                                                </div>
                                              </div>
                                            </div>
                                          </div>

                                          <div class="form-group">
                                            <div class="row">
                                              <label class="col-xs-8 control-label">Switch OFF</label>
                                              <div class="col-xs-4 control-label">
                                                <div class="onoffswitch greensea">
                                                  <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="switch-off">
                                                  <label class="onoffswitch-label" for="switch-off">
                                                    <span class="onoffswitch-inner"></span>
                                                    <span class="onoffswitch-switch"></span>
                                                  </label>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </aside>
