@extends('rooms.master')
@section('content')
    <!-- Begin Main -->
    <div role="main" class="main pgl-bg-grey">

        <!-- Begin Properties -->
        <section class="pgl-properties pgl-bg-grey">
            <div class="container">
                <h2>Nhà / Phòng chưa sử dụng</h2>
                <div class="properties-full properties-listing properties-listfull">
                    <div class="listing-header clearfix">
                        <ul class="list-inline list-icons pull-left">
                            <li><a href="{{route('room.index')}}"><i class="fa fa-th"></i></a></li>
                            <li class="active"><a href="{{route('room.listFullWidth')}}"><i class="fa fa-th-list"></i></a></li>
                            <li><a href="list-map.html"><i class="fa fa-map-marker"></i></a></li>
                        </ul>
                        <ul class="list-inline list-sort pull-right">
                            <li><label for="order-status">Order</label></li>
                            <li>
                                <select id="order-status" name="order-status" data-placeholder="Order" class="chosen-select">
                                    <option value="Descending">Descending</option>
                                    <option value="Ascending">Ascending</option>
                                </select>
                            </li>
                            <li><label for="sortby-status">Sort by</label></li>
                            <li>
                                <select id="sortby-status" name="sortby-status" data-placeholder="Sort by" class="chosen-select">
                                    <option value="Name">Name</option>
                                    <option value="Area">Area</option>
                                    <option value="Date">Date</option>
                                </select>
                            </li>
                        </ul>
                    </div>

                    @foreach($rooms as $room)
                        <div class="pgl-property animation">
                            <div class="row">
                                <div class="col-sm-6 col-md-4">
                                    <div class="property-thumb-info-image">
                                        <img alt="" class="img" src="{{ URL::to('/rooms/images/rooms') }}/{{ json_decode($room->images)[0] }}" height="250px" width="100%">
                                        <span class="property-thumb-info-label">
											<span class="label price">{{number_format($room->price)}}</span>
										</span>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-md-8">
                                    <div class="property-thumb-info">

                                        <div class="property-thumb-info-content">
                                            <h3><a href="property-detail.html"><b>{{$room->title}}</b></a></h3>
                                            <address><b>Địa điểm:</b> {{$room->address}}</address>
                                            <p><b>Thời gian đăng:</b> {{$room->created_at}}</p>
                                        </div>
                                        <div class="amenities clearfix">
                                            <ul class="pull-left">
                                                <li><strong>Diện tích:</strong> {{$room->acreage}}<sup>m2</sup></li>
                                            </ul>
                                            <ul class="pull-right">
                                                <li><i class="fa fa-eye"></i> Lượt xem:{{$room->count_views}}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    {!! $rooms->links() !!}
                </div>
            </div>
        </section>
        <!-- End Properties -->

    </div>
    <!-- End Main -->
@stop
