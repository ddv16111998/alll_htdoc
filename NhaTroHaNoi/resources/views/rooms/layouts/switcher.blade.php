<!-- Begin Style Switcher -->
<div id="style-switcher">
    <div id="toggle_button"> <a href="#"><i class="fa fa-pencil"></i></a> </div>
    <div id="style-switcher-menu">
        <h4 class="text-center">Style Switcher</h4>
        <div class="segment">
            <ul class="theme cookie_layout_style level-0" id="bd_value">
                <li> <a style="background: #36c" title="colors/blue/style" href="#"></a> </li>
                <li> <a style="background: #8a745f" title="colors/brown/style" href="#"></a> </li>
                <li> <a style="background: #8bc473" title="colors/green/style" href="#"></a> </li>
                <li> <a style="background: #f9b256" title="colors/orange/style" href="#"></a> </li>
                <li> <a style="background: #4dbfd9" title="colors/cyan/style" href="#"></a> </li>
                <li> <a style="background: #c578c8" title="colors/violet/style" href="#"></a> </li>
            </ul>
        </div>
        <div class="segment">
            <div id="reset"> <a href="#" class="btn btn-sm reset">Reset</a> </div>
        </div>
    </div>
</div>
<!-- Begin Style Switcher -->
