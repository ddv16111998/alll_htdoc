<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix'=>'admin'], function () {
    //Admin Welcome
    Route::get('/', function () {
        return view('welcome');
    })->name('admin.welcome');
    Auth::routes();
    Route::middleware(['loginCheck'])->group(function (){

// Admin DashBoard
        Route::get('/dashboard','Admin\DashBoardController@index')->name('admin.dashboard.index');

//Admin Admin
        Route::group(['prefix' => 'admin','namespace'=>'Admin'], function () {
            Route::get('index','AdminController@index')->name('admin.admin.index');
            Route::get('create','AdminController@create')->name('admin.admin.create');
            Route::post('store','AdminController@store')->name('admin.admin.store');
            Route::get('edit/{id}','AdminController@edit')->name('admin.admin.edit');
            Route::post('update/{id}','AdminController@update')->name('admin.admin.update');
            Route::get('destroy/{id}','AdminController@destroy')->name('admin.admin.destroy');
        });
        Route::group(['prefix' => 'approved','namespace'=>'Admin'], function () {
            Route::get('list','ApprovedController@list')->name('admin.approved.list');
            Route::get('handle/{id}','ApprovedController@handle')->name('admin.approved.handle');
        });

//Admin User
        Route::group(['prefix' => 'user','namespace'=>'Admin'], function () {
            Route::get('index','UserController@index')->name('admin.user.index');
            Route::post('store','UserController@store')->name('admin.user.store');
            Route::get('edit/{id}','UserController@edit')->name('admin.user.edit');
            Route::post('update/{id}','UserController@update')->name('admin.user.update');
            Route::get('destroy/{id}','UserController@destroy')->name('admin.user.destroy');
        });
//Admin Cate
        Route::group(['prefix' => 'cate','namespace'=>'Admin'], function () {
            Route::get('index','CateController@index')->name('admin.cate.index');
            Route::get('create','CateController@create')->name('admin.cate.create');
            Route::post('store','CateController@store')->name('admin.cate.store');
            Route::get('edit/{id}','CateController@edit')->name('admin.cate.edit');
            Route::post('update/{id}','CateController@update')->name('admin.cate.update');
            Route::get('destroy/{id}','CateController@destroy')->name('admin.cate.destroy');
        });

//Admin District
        Route::group(['prefix' => 'district','namespace'=>'Admin'], function () {
            Route::get('index','DistrictController@index')->name('admin.district.index');
        });

//Admin Commune
        Route::group(['prefix' => 'commune','namespace'=>'Admin'], function () {
            Route::get('index','CommuneController@index')->name('admin.commune.index');
        });

    });
});


//giao diện người dùng -client
Route::group(['prefix' => 'room','namespace'=>'Room'], function () {

        //      info user // trang cá nhân
        Route::group(['middleware' => 'loginCheckUser'], function () {
            Route::get('user/information','RoomController@informationUser')->name('room.user.info');
            Route::post('user/update/{id}','RoomController@updateUser')->name('room.user.update');
            Route::get('user/destroy/{id}','RoomController@destroyUser')->name('room.user.destroy');
            Route::get('user/check-password','RoomController@checkPassword')->name('room.user.checkPassword');
            Route::post('user/check-password','RoomController@checkPass')->name('room.user.checkPass');
            Route::post('user/handle-change-password','RoomController@handleChangePassword')->name('room.user.handleChangePass');
        });

        // index
        Route::get('index','RoomController@index')->name('room.index');
        Route::get('listFullWidth','RoomController@listFullWidth')->name('room.listFullWidth');  // full width
        Route::get('detailsRoom/{id}','RoomController@detailsRoom')->name('room.details');
        //post
        Route::group(['prefix'=>'post','middleware'=>'loginCheckUser'], function () {
            Route::get('index','RoomController@post')->name('room.post');
            Route::post('store','RoomController@handlePost')->name('room.store');
            Route::get('destroy/{id}','RoomController@destroy')->name('room.post.destroy');
            Route::post('comment/{id_post}','CommentController@store')->name('room.comment');
            Route::get('key/{id_post}/{status}','RoomController@key')->name('room.post.key');
        });

        Route::get('create','RoomController@create')->name('room.create'); // đăng ký tạo tài khoản
        Route::post('search','RoomController@search')->name('room.search');
        Route::get('byCate/{cateId}','RoomController@getRoomByCate')->name('room.byCate');
        Route::post('byCate/{cateId}','RoomController@getRoomByCate')->name('room.byCateAndFilter');
        Route::get('byCateFullWidth/{cateId}','RoomController@getRoomByCateFullWidth')->name('room.byCateFullWidth');
        Route::post('filter','RoomController@filterRoom')->name('room.filter');
        // login // logout
        Route::get('login','Auth\LoginController@showLoginForm')->name('room.login'); // đăng nhập tài khoản
        Route::post('login','Auth\LoginController@handleLogin')->name('room.handleLogin'); // xử lý đăng nhập tài khoản
        Route::get('logout','Auth\LoginController@logout')->name('room.logout');


});

        // Home Controller
        Route::get('get-location','HomeController@getLocation')->name('get_location');

