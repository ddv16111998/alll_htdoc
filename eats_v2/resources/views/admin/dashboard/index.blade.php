@extends('admin.theme.master')
@section('title') Dashboard @endsection
@section('content')

    <div class="row">
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title text-success"> Datas </h5>
                    <div class="stats-row">
                        <div class="stat-item">
                            <h6>Tổng số bản ghi</h6>
                            <b>{{$COUNT_NEW_DATA+$COUNT_DATA_GET_INFO}}</b>
                        </div>
                        <div class="stat-item">
                            <h6>Dữ liệu mới</h6>
                            <b>{{$COUNT_NEW_DATA}}</b>
                        </div>
                        <div class="stat-item">
                            <h6>Đã lấy thông tin</h6>
                            <b>{{$COUNT_DATA_GET_INFO}}</b>
                        </div>
                    </div>
                </div>
                <div id="sparkline8" class="minus-mar"></div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><small class="pull-right text-danger"><i class="fa fa-sort-desc"></i> 18% High then last month</small>Total Vendor</h5>
                    <div class="stats-row">
                        <div class="stat-item">
                            <h6>Overall Growth</h6>
                            <b>80.40%</b></div>
                        <div class="stat-item">
                            <h6>Montly</h6>
                            <b>15.40%</b></div>
                        <div class="stat-item">
                            <h6>Day</h6>
                            <b>5.50%</b></div>
                    </div>
                </div>
                <div id="sparkline9" class="minus-mar"></div>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><small class="pull-right text-success"><i class="fa fa-sort-asc"></i> 18% High then last month</small>Invoice</h5>
                    <div class="stats-row">
                        <div class="stat-item">
                            <h6>Overall Growth</h6>
                            <b>80.40%</b></div>
                        <div class="stat-item">
                            <h6>Montly</h6>
                            <b>15.40%</b></div>
                        <div class="stat-item">
                            <h6>Day</h6>
                            <b>5.50%</b></div>
                    </div>
                </div>
                <div id="sparkline10" class="minus-mar"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title m-b-0">New Customers List</h5>
                    <p class="text-muted">this is the sample data here for crm</p>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Username</th>
                                <th>Role</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>Deshmukh</td>
                                <td>Prohaska</td>
                                <td>@Genelia</td>
                                <td><span class="label label-danger">admin</span> </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Deshmukh</td>
                                <td>Gaylord</td>
                                <td>@Ritesh</td>
                                <td><span class="label label-info">member</span> </td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>Sanghani</td>
                                <td>Gusikowski</td>
                                <td>@Govinda</td>
                                <td><span class="label label-warning">developer</span> </td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Roshan</td>
                                <td>Rogahn</td>
                                <td>@Hritik</td>
                                <td><span class="label label-success">supporter</span> </td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>Joshi</td>
                                <td>Hickle</td>
                                <td>@Maruti</td>
                                <td><span class="label label-info">member</span> </td>
                            </tr>
                            <tr>
                                <td>6</td>
                                <td>Nigam</td>
                                <td>Eichmann</td>
                                <td>@Sonu</td>
                                <td><span class="label label-success">supporter</span> </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title m-b-0">New Product List</h5>
                    <p class="text-muted">this is the sample data here for crm</p>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Products</th>
                                <th>Popularity</th>
                                <th>Sales</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>Milk Powder</td>
                                <td><span class="peity-line" data-width="120" data-peity='{ "fill": ["#13dafe"], "stroke":["#13dafe"]}' data-height="40">0,-3,-2,-4,-5,-4,-3,-2,-5,-1</span> </td>
                                <td><span class="text-danger text-semibold"><i class="fa fa-level-down" aria-hidden="true"></i> 28.76%</span> </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>Air Conditioner</td>
                                <td><span class="peity-line" data-width="120" data-peity='{ "fill": ["#13dafe"], "stroke":["#13dafe"]}' data-height="40">0,-1,-1,-2,-3,-1,-2,-3,-1,-2</span> </td>
                                <td><span class="text-warning text-semibold"><i class="fa fa-level-down" aria-hidden="true"></i> 8.55%</span> </td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>RC Cars</td>
                                <td><span class="peity-line" data-width="120" data-peity='{ "fill": ["#13dafe"], "stroke":["#13dafe"]}' data-height="40">0,3,6,1,2,4,6,3,2,1</span> </td>
                                <td><span class="text-success text-semibold"><i class="fa fa-level-up" aria-hidden="true"></i> 58.56%</span> </td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>Down Coat</td>
                                <td><span class="peity-line" data-width="120" data-peity='{ "fill": ["#13dafe"], "stroke":["#13dafe"]}' data-height="40">0,3,6,4,5,4,7,3,4,2</span> </td>
                                <td><span class="text-info text-semibold"><i class="fa fa-level-up" aria-hidden="true"></i> 35.76%</span> </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection