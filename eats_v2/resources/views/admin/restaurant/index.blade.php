@extends('admin.theme.master')
@section('title', 'Nhà Hàng')
@section('js')
    <script>
        $(function() {
            var url = '{{route('admin.restaurant.index')}}?ds=true';
            @if(isset($_GET['trash']))
                url += "&trash={{$_GET['trash']}}";
            @endif
            $('#listData').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'avatar', name: 'avatar' },
                    { data: 'name', name: 'name' },
                    { data: 'address', name: 'address' },
                    { data: 'province_id', name: 'province_id' },
                    { data: 'user_id', name: 'user_id' },
                    { data: 'is_crawl', name: 'is_crawl' },
                    { data: 'status', name: 'status' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'action', name : 'action',orderable: false },
                ],
                // "aoColumnDefs": [
                //     { "bSearchable": false, "aTargets": [ 5 ] }
                // ]
            });
        });
    </script>
@endsection
@section('content')
    <div id="list-crud">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">
                        Nhà Hàng
                        @if(isset($_GET['trash']))
                            @if($_GET['trash']==1)
                                <a style="margin-left:10px" href="{{route('admin.restaurant.index')}}" class="btn btn-sm btn-info">Danh sách</a>
                            @else
                                <a style="margin-left:10px" href="{{route('admin.restaurant.index')}}?trash=1" class="btn btn-sm btn-warning">Thùng rác</a>
                            @endif
                        @else
                            <a style="margin-left:10px" href="{{route('admin.restaurant.index')}}?trash=1" class="btn btn-sm btn-warning">Thùng rác</a>
                        @endif
                    </h4>
                    <form method="GET">
                   {{--  <ul style="list-style:none">
                        <input type="hidden" name="trash" value="{{ isset($_GET['trash']) ? $_GET['trash'] : 0 }}">
                        <li style="display:inline-block;">
                            <select class="form-control form-control-sm" name="province">
                                <option value="">-- Tỉnh/TP --</option>
                                @foreach ($province as $elm_pr)
                                    <option @if (isset($_GET['province']))
                                        @if($_GET['province']==$elm_pr['slug']) selected @endif
                                    @endif value="{{$elm_pr['slug']}}">{{$elm_pr['name']}}</option>
                                @endforeach
                            </select>
                        </li>
                        <li style="display:inline-block;">
                            <select class="form-control form-control-sm" name="category">
                                <option value="">-- Hình thức --</option>
                                @foreach ($category as $elm_ct)
                                    <option @if (isset($_GET['category']))
                                        @if($_GET['category']==$elm_ct['id']) selected @endif
                                    @endif value="{{$elm_ct['id']}}">{{$elm_ct['name']}}</option>
                                @endforeach
                            </select>
                        </li>
                        <li style="display:inline-block;">
                            <select class="form-control form-control-sm" name="source">
                                <option value="">-- Nguồn --</option>
                                @foreach ($source as $k_s => $elm_s)
                                    <option @if (isset($_GET['source']))
                                        @if($_GET['source']==$elm_s) selected @endif
                                    @endif value="{{$elm_s}}">{{$k_s}}</option>
                                @endforeach
                            </select>
                        </li>
                        <li style="display:inline-block;">
                            <select class="form-control form-control-sm" name="status">
                                <option value="">-- Trạng thái --</option>
                                @foreach ($status as $k_st => $v_st)
                                    <option @if (isset($_GET['status']))
                                        @if($_GET['status']==$k_st) selected @endif
                                    @endif value="{{$k_st}}">{{$v_st}}</option>
                                @endforeach
                            </select>
                        </li>
                        <li style="display:inline-block;">
                            <button class="btn btn-primary"> Lọc </button>
                        </li>
                    </ul> --}}
                    </form>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="">Nhà Hàng </a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <table class="table table-hover mt-4" id="listData">
                                    <thead>
                                        <th>#</th>
                                        <th>Ảnh đại diện</th>
                                        <th style="width:225px">Nhà hàng,Quán ăn</th>
                                        <th>Địa chỉ</th>
                                        <th>Khu vực</th>
                                        <th>Người đăng</th>
                                        <th>Nguồn</th>
                                        <th>Trạng thái</th>
                                        <th>Ngày tạo</th>
                                        <th>Action</th>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
