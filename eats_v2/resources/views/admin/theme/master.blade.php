<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="/favicon.ico" type="image/x-icon" rel="icon">
    <link href="/favicon.ico" type="image/x-icon" rel="shortcut icon">
    <title>@yield('title')</title>
    <link href="https://cdn.datatables.net/v/bs4-4.1.1/jq-3.3.1/jszip-2.5.0/dt-1.10.18/af-2.3.3/b-1.5.6/b-colvis-1.5.6/b-flash-1.5.6/b-html5-1.5.6/b-print-1.5.6/r-2.2.2/datatables.min.css" rel="stylesheet">
    {{-- <link href="/assets/css/morris.css" rel="stylesheet"> --}}
    <link href="/assets/css/jquery.toast.css" rel="stylesheet">
    <link href="/assets/css/style.min.css" rel="stylesheet">
    <link href="/assets/css/select2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('css')
    <link href="/assets/css/crud.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i&display=swap&subset=vietnamese" rel="stylesheet">
    <style>.skin-default-dark .left-sidebar,.skin-default-dark .topbar .top-navbar .navbar-header{background:#fff!important}</style>
</head>
<body class="skin-default-dark fixed-layout">
<div class="preloader">
    <div class="loader">
        <div class="loader__figure"></div>
        <p class="loader__label">Loading...</p>
    </div>
</div>
<div id="main-wrapper">
    @include('admin.layouts.top_bar')
    @include('admin.layouts.left_sidebar')
    <div class="page-wrapper">
        <div class="container-fluid" id="main__content">
            @yield('content')
        </div>
    </div>
    @include('admin.layouts.footer')
</div>
<script src="/assets/js/jquery-3.2.1.min.js"></script>
<script src="/assets/js/popper.min.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/perfect-scrollbar.jquery.min.js"></script>
<script src="/assets/js/waves.js"></script>
<script src="/assets/js/sidebarmenu.js"></script>
<script src="/assets/js/custom.min.js"></script>
<script src="/assets/js/raphael-min.js"></script>
<script src="/assets/js/jquery.sparkline.min.js"></script>
<script src="/assets/js/jquery.toast.js"></script>
<script src="/assets/js/jquery.peity.min.js"></script>
<script src="/assets/js/jquery.peity.init.js"></script>
<script src="/assets/js/select2.min.js"></script>
<script src="/assets/js/jquery.dataTables.min.js"></script>

<script src="/assets/js/crud.js"></script>
@yield('js')
<script>
    @if(Session::has('message'))
        var type = "{{ Session::get('type', 'success') }}";
        var message = "{{ Session::get('message') }}";
        notification(type, message);
    @endif
</script>
</body>
</html>
