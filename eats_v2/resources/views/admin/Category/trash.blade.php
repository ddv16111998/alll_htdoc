@extends('admin.theme.master')
@section('title','Danh mục')
@section('js')
    <script>
        $(function() {
            $('#listData').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.cate.trash') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'slug', name: 'slug' },
                    { data: 'action', name: 'action' },
                ]
            });
        });
    </script>
@endsection
@section('content')
    <div id="list-crud">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">
                        Danh mục
                        @if(isset($_GET['trash']))
                            @if($_GET['trash']==1)
                                <a style="margin-left:10px" href="{{route('admin.cate.index')}}" class="btn btn-sm btn-info">Danh sách</a>
                            @else
                                <a style="margin-left:10px" href="{{route('admin.cate.trash')}}?trash=1" class="btn btn-sm btn-warning">Thùng rác <span class="badge btn-crud-danger"></span></a>
                            @endif
                        @else
                            <a style="margin-left:10px" href="{{route('admin.cate.trash')}}?trash=1" class="btn btn-sm btn-warning">Thùng rác <span class="badge badge-light">{{$count}}</span><span class="badge btn-crud-danger"></span></a>
                        @endif
                    </h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('admin.cate.index')}}">Danh mục</a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div>
                                    <table class="table table-hover mt-4" id="listData">
                                        <thead>
                                        <th>ID</th>
                                        <th>Ẩm thực</th>
                                        <th>Đường dẫn</th>
                                        <th>Action</th>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
