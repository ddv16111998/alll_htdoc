<style>
    .skin-default-dark .sidebar-nav > ul > li.active > a {border-color:#fff}
</style>
<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- QUẢN LÝ -->
                <li class="nav-small-cap">--- BÁO CÁO</li>
                <li>
                    <a class="waves-effect waves-dark" href="{{route('admin.dashboard')}}" aria-expanded="false">
                        <i class="icon-speedometer"></i>
                        <span class="hide-menu">Dashboard</span>
                    </a>
                </li>
                <!-- Nhà Hàng -->
                <li class="nav-small-cap">--- NHÀ HÀNG _ QUÁN ĂN</li>
                <li>
                    <a class="waves-effect waves-dark" href="{{route('admin.restaurant.index')}}" aria-expanded="false">
                        <i class="ti-help-alt"></i>
                        <span class="hide-menu">Danh sách</span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="{{route('admin.cate.index')}}" aria-expanded="false">
                        <i class="ti-help-alt"></i>
                        <span class="hide-menu">Danh mục</span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="{{route('admin.cuisine.index')}}" aria-expanded="false">
                        <i class="ti-help-alt"></i>
                        <span class="hide-menu">Ẩm Thực</span>
                    </a>
                </li>

                <!-- THÀNH VIÊN -->
                <li class="nav-small-cap">--- THÀNH VIÊN</li>
                <li>
                    <a class="waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="ti-help-alt"></i>
                        <span class="hide-menu">Ban quản trị</span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="ti-help-alt"></i>
                        <span class="hide-menu">Khách</span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="#" aria-expanded="false">
                        <i class="ti-help-alt"></i>
                        <span class="hide-menu">Chủ địa điểm</span>
                    </a>
                </li>

                <!-- HỆ THỐNG -->
                <li class="nav-small-cap">--- KHU VỰC _ QUẬN/HUYỆN</li>
                <li>
                    <a class="waves-effect waves-dark" href="{{route('admin.province.index')}}" aria-expanded="false">
                        <i class="ti-help-alt"></i>
                        <span class="hide-menu">Khu Vực</span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="{{route('admin.district.index')}}" aria-expanded="false">
                        <i class="ti-help-alt"></i>
                        <span class="hide-menu">Quận/Huyện</span>
                    </a>
                </li>

                <!-- Tools -->
                <li class="nav-small-cap">--- Tools</li>
                <li>
                    <a class="waves-effect waves-dark" href="#">
                        <i class="ti-search"></i>
                        <span class="hide-menu">Crawl</span>
                    </a>
                </li>

                <!-- HỆ THỐNG -->
                <li class="nav-small-cap">--- HỆ THỐNG</li>
                <li>
                    <a class="waves-effect waves-dark" href="pages-faq.html" aria-expanded="false">
                        <i class="ti-help-alt"></i>
                        <span class="hide-menu">FAQs</span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="#">
                        <i class="ti-settings"></i>
                        <span class="hide-menu">System</span>
                    </a>
                </li>

                <li>
                    <a class="waves-effect waves-dark" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" aria-expanded="false">
                        <i class="icon-power text-danger"></i>
                        <span class="hide-menu">Log Out</span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </nav>
    </div>
</aside>
