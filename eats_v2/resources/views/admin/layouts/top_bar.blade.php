<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-dark">
        
        <div class="navbar-header" style="padding-left:0">
            <a class="navbar-brand" href="{{ route('admin.dashboard') }}" style="width:100%;text-align:center;">
                <img src="/images/logo.png" alt="homepage" class="light-logo" />
            </a>
        </div>
       
        <div class="navbar-collapse" style="background:#67b8dc;height:56px;">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item"> <a class="nav-link nav-toggler d-block d-md-none waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                <li class="nav-item"> <a class="nav-link sidebartoggler d-none d-lg-block d-md-block waves-effect waves-dark" href="javascript:void(0)"><i class="icon-menu"></i></a> </li>
            </ul>
            <!-- ============================================================== -->
            <ul class="navbar-nav my-lg-0">

                <li class="nav-item dropdown u-pro top-bar-user">
                    <div class="nav-link dropdown-toggle waves-effect waves-dark profile-pic" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="height:100%">
                        <div class="d-flex" style="height:100%">
                            <div class="bor-img">
                                <div class="img">
                                    <img src="@if(Auth::user()->avatar) {{ Auth::user()->avatar }} @else {{'/images/noimage.jpg'}} @endif" alt="user-img" class="w-100">
                                </div>
                            </div>
                            <div class="name" style="align-items:center;align-content:center;display:flex;">
                                {{ Auth::user()->username }}&nbsp;<i class="fa fa-angle-down"></i>
                            </div>
                        </div>
                    </div>
                    <div class="dropdown-menu dropdown-menu-right animated flipInY">
                        <a href="#" class="dropdown-item"><i class="ti-user"></i> @lang('user.account_personal_information')</a>
                        <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" aria-expanded="false"><i class="fa fa-power-off"></i> @lang('system.logout')</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>
