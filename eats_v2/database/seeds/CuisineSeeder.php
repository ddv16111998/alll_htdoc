<?php

use Illuminate\Database\Seeder;

class CuisineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            ["name" => "Món Việt", "slug" => "phong-cach-viet" , "cs_fd_id" => 1],
            ["name" => "Món Miền Nam", "slug" => "phong-cach-mien-nam" , "cs_fd_id" => 12],
            ["name" => "Món Bắc", "slug" => "phong-cach-bac" , "cs_fd_id" => 2],
            ["name" => "Món Miền Trung", "slug" => "phong-cach-mien-trung" , "cs_fd_id" => 7],
            ["name" => "Tây Nguyên", "slug" => "phong-cach-tay-nguyen" , "cs_fd_id" => 29],
            ["name" => "Món Á", "slug" => "phong-cach-a" , "cs_fd_id" => 36],
            ["name" => "Món Hàn", "slug" => "phong-cach-han" , "cs_fd_id" => 22],
            ["name" => "Món Nhật", "slug" => "phong-cach-nhat" , "cs_fd_id" => 21],
            ["name" => "Món Trung Hoa", "slug" => "phong-cach-trung-hoa" , "cs_fd_id" => 4],
            ["name" => "Món Thái", "slug" => "phong-cach-thai" , "cs_fd_id" => 17],
            ["name" => "Singapore", "slug" => "phong-cach-singapore" , "cs_fd_id" => 30],
            ["name" => "Malaysia", "slug" => "phong-cach-malaysia" , "cs_fd_id" => 31],
            ["name" => "Campuchia", "slug" => "phong-cach-campuchia" , "cs_fd_id" => 38],
            ["name" => "Philippines", "slug" => "phong-cach-philippines" , "cs_fd_id" => 42],
            ["name" => "Indonesia", "slug" => "phong-cach-indonesia" , "cs_fd_id" => 37],
            ["name" => "Món Ấn Độ", "slug" => "phong-cach-an-do" , "cs_fd_id" => 16],
            ["name" => "Đài Loan", "slug" => "phong-cach-dai-loan" , "cs_fd_id" => 51],
            ["name" => "Iran", "slug" => "phong-cach-iran" , "cs_fd_id" => 53],
            ["name" => "Món Âu", "slug" => "phong-cach-au" , "cs_fd_id" => 34],
            ["name" => "Pháp", "slug" => "phong-cach-phap" , "cs_fd_id" => 19],
            ["name" => "Ý", "slug" => "phong-cach-y" , "cs_fd_id" => 18],
            ["name" => "Đức", "slug" => "phong-cach-duc" , "cs_fd_id" => 20],
            ["name" => "Tây Ban Nha", "slug" => "phong-cach-tay-ban-nha" , "cs_fd_id" => 35],
            ["name" => "Thụy sĩ", "slug" => "phong-cach-thuy-si" , "cs_fd_id" => 25],
            ["name" => "Bánh Pizza", "slug" => "banh-pizza" , "cs_fd_id" => 52],
            ["name" => "Châu Mỹ", "slug" => "phong-cach-chau-my" , "cs_fd_id" => 24],
            ["name" => "Mỹ", "slug" => "phong-cach-my" , "cs_fd_id" => 33],
            ["name" => "Brazil", "slug" => "phong-cach-brazil" , "cs_fd_id" => 32],
            ["name" => "Mexico", "slug" => "phong-cach-mexico" , "cs_fd_id" => 39],
            ["name" => "Canada", "slug" => "phong-cach-canada" , "cs_fd_id" => 40],
            ["name" => "Argentina", "slug" => "phong-cach-argentina" , "cs_fd_id" => 46],
            ["name" => "Quốc tế", "slug" => "phong-cach-quoc-te" , "cs_fd_id" => 50],
            ["name" => "Đông Âu", "slug" => "phong-cach-dong-au" , "cs_fd_id" => 28],
            ["name" => "Thổ Nhĩ Kỳ", "slug" => "phong-cach-tho-nhi-ky" , "cs_fd_id" => 43],
            ["name" => "Tiệp (Séc)", "slug" => "phong-cach-tiep-sec" , "cs_fd_id" => 44],
            ["name" => "Úc", "slug" => "phong-cach-uc" , "cs_fd_id" => 47],
            ["name" => "Bắc Âu", "slug" => "phong-cach-bac-au" , "cs_fd_id" => 26],
            ["name" => "Trung Đông", "slug" => "phong-cach-trung-dong" , "cs_fd_id" => 27],
            ["name" => "Ả Rập", "slug" => "phong-cach-a-rap" , "cs_fd_id" => 41],
            ["name" => "Châu Phi", "slug" => "phong-cach-chau-phi" , "cs_fd_id" => 23],
        ];
        \DB::table('cuisines')->insert($arr);
    }
}
