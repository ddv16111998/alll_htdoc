<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	\DB::table('categories')->insert([
        	[ 'name' => 'Sang trọng', 'slug' => 'sang-trong', 'ct_fd_id' => 12],
            [ 'name' => 'Buffet', 'slug' => 'buffet', 'ct_fd_id' => 39],
            [ 'name' => 'Nhà hàng', 'slug' => 'nha-hang', 'ct_fd_id' => 1],
            [ 'name' => 'Ăn vặt/vỉa hè', 'slug' => 'an-vat-via-he', 'ct_fd_id' => 11],
            [ 'name' => 'Ăn chay', 'slug' => 'an-chay', 'ct_fd_id' => 56],
            [ 'name' => 'Café/Dessert', 'slug' => 'cafe-dessert', 'ct_fd_id' => 2],
            [ 'name' => 'Quán ăn', 'slug' => 'quan-an', 'ct_fd_id' => 3],
            [ 'name' => 'Bar/Pub', 'slug' => 'bar-pub', 'ct_fd_id' => 4],
            [ 'name' => 'Quán nhậu', 'slug' => 'quan-nhau', 'ct_fd_id' => 54],
            [ 'name' => 'Beer club', 'slug' => 'beer-club', 'ct_fd_id' => 43],
            [ 'name' => 'Tiệm bánh', 'slug' => 'tiem-banh', 'ct_fd_id' => 6],
            [ 'name' => 'Tiệc tận nơi', 'slug' => 'tiec-tan-noi', 'ct_fd_id' => 44],
            [ 'name' => 'Shop Online', 'slug' => 'shop-online', 'ct_fd_id' => 27],
            [ 'name' => 'Khu Ẩm Thực', 'slug' => 'khu-am-thuc', 'ct_fd_id' => 79],
            [ 'name' => 'Giao cơm văn phòng', 'slug' => 'giao-com-van-phong', 'ct_fd_id' => 28],
        ]);
    }
}
