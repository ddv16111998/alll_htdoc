<?php

use Illuminate\Database\Seeder;

class ProvinceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('provinces')->insert([
        	[ 'prv_fd_id' => 217, 'name' => 'Hồ Chí Minh', 'slug' => 'ho-chi-minh'],
			[ 'prv_fd_id' => 218, 'name' => 'Hà Nội', 'slug' => 'ha-noi'],
			[ 'prv_fd_id' => 219, 'name' => 'Đà Nẵng', 'slug' => 'da-nang'],
			[ 'prv_fd_id' => 221, 'name' => 'Cần Thơ', 'slug' => 'can-tho'],
			[ 'prv_fd_id' => 248, 'name' => 'Khánh Hòa', 'slug' => 'khanh-hoa'],
			[ 'prv_fd_id' => 223, 'name' => 'Vũng Tàu', 'slug' => 'vung-tau'],
			[ 'prv_fd_id' => 220, 'name' => 'Hải Phòng', 'slug' => 'hai-phong'],
			[ 'prv_fd_id' => 233, 'name' => 'Bình Thuận', 'slug' => 'binh-thuan'],
			[ 'prv_fd_id' => 254, 'name' => 'Lâm Đồng', 'slug' => 'lam-dong'],
			[ 'prv_fd_id' => 222, 'name' => 'Đồng Nai', 'slug' => 'dong-nai'],
			[ 'prv_fd_id' => 265, 'name' => 'Quảng Ninh', 'slug' => 'quang-ninh'],
			[ 'prv_fd_id' => 273, 'name' => 'Huế', 'slug' => 'hue'],
			[ 'prv_fd_id' => 230, 'name' => 'Bình Dương', 'slug' => 'binh-duong'],
			[ 'prv_fd_id' => 244, 'name' => 'Hải Dương', 'slug' => 'hai-duong'],
			[ 'prv_fd_id' => 259, 'name' => 'Ninh Thuận', 'slug' => 'ninh-thuan'],
			[ 'prv_fd_id' => 256, 'name' => 'Nam Định', 'slug' => 'nam-dinh'],
			[ 'prv_fd_id' => 274, 'name' => 'Tiền Giang', 'slug' => 'tien-giang'],
			[ 'prv_fd_id' => 460, 'name' => 'Phú Quốc', 'slug' => 'phu-quoc'],
			[ 'prv_fd_id' => 250, 'name' => 'Kon Tum', 'slug' => 'kon-tum'],
			[ 'prv_fd_id' => 263, 'name' => 'Quảng Nam', 'slug' => 'quang-nam'],
			[ 'prv_fd_id' => 253, 'name' => 'Lào Cai', 'slug' => 'lao-cai'],
			[ 'prv_fd_id' => 257, 'name' => 'Nghệ An', 'slug' => 'nghe-an'],
			[ 'prv_fd_id' => 255, 'name' => 'Long An', 'slug' => 'long-an'],
			[ 'prv_fd_id' => 231, 'name' => 'Bình Định', 'slug' => 'binh-dinh'],
			[ 'prv_fd_id' => 261, 'name' => 'Phú Yên', 'slug' => 'phu-yen'],
			[ 'prv_fd_id' => 236, 'name' => 'Đắk Lắk', 'slug' => 'dak-lak'],
			[ 'prv_fd_id' => 224, 'name' => 'An Giang', 'slug' => 'an-giang'],
			[ 'prv_fd_id' => 272, 'name' => 'Thanh Hóa', 'slug' => 'thanh-hoa'],
			[ 'prv_fd_id' => 249, 'name' => 'Kiên Giang', 'slug' => 'kien-giang'],
			[ 'prv_fd_id' => 264, 'name' => 'Quảng Ngãi', 'slug' => 'quang-ngai'],
			[ 'prv_fd_id' => 269, 'name' => 'Tây Ninh', 'slug' => 'tay-ninh'],
			[ 'prv_fd_id' => 240, 'name' => 'Gia Lai', 'slug' => 'gia-lai'],
			[ 'prv_fd_id' => 277, 'name' => 'Vĩnh Long', 'slug' => 'vinh-long'],
			[ 'prv_fd_id' => 234, 'name' => 'Cà Mau', 'slug' => 'ca-mau'],
			[ 'prv_fd_id' => 239, 'name' => 'Đồng Tháp', 'slug' => 'dong-thap'],
			[ 'prv_fd_id' => 262, 'name' => 'Quảng Bình', 'slug' => 'quang-binh'],
			[ 'prv_fd_id' => 266, 'name' => 'Quảng Trị', 'slug' => 'quang-tri'],
			[ 'prv_fd_id' => 229, 'name' => 'Bến Tre', 'slug' => 'ben-tre'],
			[ 'prv_fd_id' => 232, 'name' => 'Bình Phước', 'slug' => 'binh-phuoc'],
			[ 'prv_fd_id' => 228, 'name' => 'Bắc Ninh', 'slug' => 'bac-ninh'],
			[ 'prv_fd_id' => 267, 'name' => 'Sóc Trăng', 'slug' => 'soc-trang'],
			[ 'prv_fd_id' => 278, 'name' => 'Vĩnh Phúc', 'slug' => 'vinh-phuc'],
			[ 'prv_fd_id' => 275, 'name' => 'Trà Vinh', 'slug' => 'tra-vinh'],
			[ 'prv_fd_id' => 271, 'name' => 'Thái Nguyên', 'slug' => 'thai-nguyen'],
			[ 'prv_fd_id' => 258, 'name' => 'Ninh Bình', 'slug' => 'ninh-binh'],
			[ 'prv_fd_id' => 225, 'name' => 'Bạc Liêu', 'slug' => 'bac-lieu'],
			[ 'prv_fd_id' => 243, 'name' => 'Hà Tĩnh', 'slug' => 'ha-tinh'],
			[ 'prv_fd_id' => 227, 'name' => 'Bắc Giang', 'slug' => 'bac-giang'],
			[ 'prv_fd_id' => 260, 'name' => 'Phú Thọ', 'slug' => 'phu-tho'],
			[ 'prv_fd_id' => 245, 'name' => 'Hậu Giang', 'slug' => 'hau-giang'],
			[ 'prv_fd_id' => 270, 'name' => 'Thái Bình', 'slug' => 'thai-binh'],
			[ 'prv_fd_id' => 268, 'name' => 'Sơn La', 'slug' => 'son-la'],
			[ 'prv_fd_id' => 252, 'name' => 'Lạng Sơn', 'slug' => 'lang-son'],
			[ 'prv_fd_id' => 246, 'name' => 'Hòa Bình', 'slug' => 'hoa-binh'],
			[ 'prv_fd_id' => 247, 'name' => 'Hưng Yên', 'slug' => 'hung-yen'],
			[ 'prv_fd_id' => 241, 'name' => 'Hà Giang', 'slug' => 'ha-giang'],
			[ 'prv_fd_id' => 237, 'name' => 'Đắk Nông', 'slug' => 'dak-nong'],
			[ 'prv_fd_id' => 276, 'name' => 'Tuyên Quang', 'slug' => 'tuyen-quang'],
			[ 'prv_fd_id' => 279, 'name' => 'Yên Bái', 'slug' => 'yen-bai'],
			[ 'prv_fd_id' => 242, 'name' => 'Hà Nam', 'slug' => 'ha-nam'],
			[ 'prv_fd_id' => 238, 'name' => 'Điện Biên', 'slug' => 'dien-bien'],
			[ 'prv_fd_id' => 235, 'name' => 'Cao Bằng', 'slug' => 'cao-bang'],
			[ 'prv_fd_id' => 251, 'name' => 'Lai Châu', 'slug' => 'lai-chau'],
			[ 'prv_fd_id' => 226, 'name' => 'Bắc Kạn', 'slug' => 'bac-kan'],
        ]);
    }
}
