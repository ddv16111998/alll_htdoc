<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert(
            [
                [
                    'email' => 'develop@gmail.com',
                    'username' => 'Develop',
                    'acc' => 'develop',
                    'password' => bcrypt('123456'),
                    'status' => 1,
                    'is_admin' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'email' => 'admin@gmail.com',
                    'username' => 'Admin',
                    'acc' => 'admin',
                    'password' => bcrypt('123456'),
                    'status' => 1,
                    'is_admin' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'email' => 'test@gmail.com',
                    'acc' => 'Test',
                    'username' => 'test',
                    'password' => bcrypt('123456'),
                    'status' => 1,
                    'is_admin' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
            ]
        );
    }
}
