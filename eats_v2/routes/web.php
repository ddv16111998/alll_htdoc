<?php

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin'], function() {
	Auth::routes();
	Route::middleware(['logincheck'])->group(function () {
		foreach(glob(base_path('routes')."/modules/*.php") as $file){
		    require $file;
		}
	});
});
