<?php

Route::get('province/list', [
	'as' => 'admin.province.index', 'uses' => 'Admin\ProvinceController@index'
]);
Route::get('province/create', [
    'as' => 'admin.province.create', 'uses' => 'Admin\ProvinceController@create'
]);
Route::post('province/store', [
    'as' => 'admin.province.store', 'uses' => 'Admin\ProvinceController@store'
]);
Route::get('province/edit/{id}',[
   'as'=> 'admin.province.edit', 'uses'=>'Admin\ProvinceController@edit'
]);
Route::post('province/update/{id}',[
    'as'=> 'admin.province.update', 'uses'=>'Admin\ProvinceController@update'
]);
Route::get('province/destroy/{id}',[
   'as'=>'admin.province.destroy','uses'=>'Admin\ProvinceController@destroy'
]);
Route::get('province/trash',[
    'as'=>'admin.province.trash','uses'=>'Admin\ProvinceController@trash'
]);
Route::get('province/restore/{id}',[
    'as'=>'admin.province.restore','uses'=>'Admin\ProvinceController@restore'
]);

