<?php
/* Route loại Ẩm Thực */
Route::get('cuisine/list', [
	'as' => 'admin.cuisine.index', 'uses' => 'Admin\CuisineController@index'
]);
Route::get('cuisine/create', [
	'as' => 'admin.cuisine.create', 'uses' => 'Admin\CuisineController@create'
]);
Route::post('cuisine/store', [
    'as' => 'admin.cuisine.store', 'uses' => 'Admin\CuisineController@store'
]);
Route::get('cuisine/edit/{id}', [
    'as' => 'admin.cuisine.edit', 'uses' => 'Admin\CuisineController@edit'
]);
Route::post('cuisine/update/{id}', [
    'as' => 'admin.cuisine.update', 'uses' => 'Admin\CuisineController@update'
]);
Route::get('cuisine/destroy/{id}', [
    'as' => 'admin.cuisine.destroy', 'uses' => 'Admin\CuisineController@destroy'
]);
Route::get('cuisine/trash', [
    'as' => 'admin.cuisine.trash', 'uses' => 'Admin\CuisineController@trash'
]);
Route::get('cuisine/restore/{id}', [
    'as' => 'admin.cuisine.restore', 'uses' => 'Admin\CuisineController@restore'
]);
