<?php

Route::get('restaurant/list', [
	'as' => 'admin.restaurant.index', 'uses' => 'Admin\RestaurantController@index'
]);
Route::get('restaurant/destroy/{id}', [
	'as' => 'admin.restaurant.destroy', 'uses' => 'Admin\RestaurantController@destroy'
]);
Route::get('restaurant/restore/{id}', [
	'as' => 'admin.restaurant.restore','uses' => 'Admin\RestaurantController@restore'
]);
