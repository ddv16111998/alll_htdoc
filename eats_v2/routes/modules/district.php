<?php

Route::get('district/list', [
	'as' => 'admin.district.index', 'uses' => 'Admin\DistrictController@index'
]);
Route::get('district/create', [
    'as' => 'admin.district.create', 'uses' => 'Admin\DistrictController@create'
]);
Route::post('district/store', [
    'as' => 'admin.district.store', 'uses' => 'Admin\DistrictController@store'
]);
Route::get('district/edit/{id}', [
    'as' => 'admin.district.edit', 'uses' => 'Admin\DistrictController@edit'
]);
Route::post('district/update/{id}', [
    'as' => 'admin.district.update', 'uses' => 'Admin\DistrictController@update'
]);
Route::get('district/trash', [
    'as' => 'admin.district.trash', 'uses' => 'Admin\DistrictController@trash'
]);
Route::get('district/restore/{id}', [
    'as' => 'admin.district.restore', 'uses' => 'Admin\DistrictController@restore'
]);
Route::get('district/destroy/{id}', [
    'as' => 'admin.district.destroy', 'uses' => 'Admin\DistrictController@destroy'
]);

