<?php
//    Route danh mục
Route::get('cate/list',[
    'as'=> 'admin.cate.index', 'uses'=>'Admin\CateController@index'
]);
Route::get('cate/create',[
    'as'=> 'admin.cate.create', 'uses'=>'Admin\CateController@create'
]);
Route::get('cate/trash',[
    'as'=> 'admin.cate.trash', 'uses'=>'Admin\CateController@trash'
]);
Route::post('cate/store',[
    'as'=> 'admin.cate.store', 'uses'=>'Admin\CateController@store'
]);
Route::get('cate/edit/{id}',[
    'as'=> 'admin.cate.edit', 'uses'=>'Admin\CateController@edit'
]);
Route::post('cate/update/{id}',[
    'as'=> 'admin.cate.update', 'uses'=>'Admin\CateController@update'
]);
Route::get('cate/destroy/{id}',[
    'as'=> 'admin.cate.destroy', 'uses'=>'Admin\CateController@destroy'
]);
Route::get('cate/restore/{id}',[
    'as'=> 'admin.cate.restore', 'uses'=>'Admin\CateController@restore'
]);
