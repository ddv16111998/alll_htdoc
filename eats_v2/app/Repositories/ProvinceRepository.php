<?php
namespace App\Repositories;
use App\Models\Province;
class ProvinceRepository
{
    public function getDataIndex()
    {
        $data = Province::whereNull('deleted_at')->get();
        return $data;
    }
    public function getDataById($id)
    {
        $data = Province::find($id);
        return $data;
    }
    public function updateOrCreate($data,$id){
        $data = Province::updateOrCreate(['id'=>$id],$data);
        if($data)
        {
            return true;
        }
        return false;
    }
    public function destroy($id)
    {
        $data = Province::find($id);
        $data->delete();
        if($data->trashed())
        {
            return true;
        }
        return false;
    }
    public function trash()
    {
        $data = Province::onlyTrashed()->get();
        return $data;
    }
    public function restore($id)
    {
        $data = Province::onlyTrashed()->updateOrCreate(['id'=>$id],['deleted_at'=>null]);
        if($data)
        {
            return true;
        }
        return false;
    }
    public function countDelete()
    {
        $data = Province::onlyTrashed()->count();
        return $data;
    }
}
