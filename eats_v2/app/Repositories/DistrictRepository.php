<?php
namespace App\Repositories;
use App\Models\District;
class DistrictRepository
{
    public function getDataIndex()
    {
        $data  = District::whereNull('deleted_at')->get();
        return $data;
    }
    public function getDataById($id)
    {
        $data = District::find($id);
        return $data;
    }
    public function updateOrCreate($data,$id){
        $data = District::updateOrCreate(['id'=>$id],$data);
        if($data)
        {
            return true;
        }
        return false;
    }
    public function destroy($id)
    {
        $data = District::find($id);
        $data->delete();
        if($data->trashed())
        {
            return true;
        }
        return false;
    }
    public function trash()
    {
        $data = District::onlyTrashed()->get();
        return $data;
    }
    public function restore($id)
    {
        $data = District::onlyTrashed()->updateOrCreate(['id'=>$id],['deleted_at'=>null]);
        if($data)
        {
            return true;
        }
        return false;
    }
    public function countDelete()
    {
        $data = District::onlyTrashed()->count();
        return $data;
    }
}
