<?php
namespace App\Repositories;
use App\Models\Category;
class CateRepository
{
    public function getDataIndex()
    {
        $data = Category::whereNull('deleted_at')->get();
        return $data;
    }
    public function getDataById($id)
    {
        $data = Category::find($id);
        return $data;
    }
    public function countDelete()
    {
        $data = Category::onlyTrashed()->count();
        return $data;
    }
    public function trash()
    {
        $data = Category::onlyTrashed()->get();
        return $data;
    }
    public function updateOrCreate($data,$id){
        $data = Category::updateOrCreate(['id'=>$id],$data);
        if($data)
        {
            return true;
        }
        return false;
    }
    public function destroy($id)
    {
        $delete = Category::find($id);
        $delete->delete();
        if($delete->trashed())
        {
            return true;
        }
        return false;
    }
    public function restore($id)
    {
        $data = Category::onlyTrashed()->updateOrCreate(['id'=>$id],['deleted_at'=>null]);
        if($data)
        {
            return true;
        }
        return false;
    }
}
