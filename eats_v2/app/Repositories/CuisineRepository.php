<?php
namespace App\Repositories;
use App\Models\Cuisine;

class CuisineRepository
{
    public function getDataIndex()
    {
        $data = Cuisine::whereNull('deleted_at')->get();
        return $data;
    }
    public function countDelete()
    {
        $data = Cuisine::onlyTrashed()->count();
        return $data;
    }
    public function updateOrCreate($data,$id){
        $data = Cuisine::updateOrCreate(['id'=>$id],$data);
        if($data)
        {
            return true;
        }
        return false;
    }
    public function getDataById($id)
    {
        $data = Cuisine::find($id);
        return $data;
    }
    public function destroy($id)
    {
        $delete = Cuisine::find($id);
        $delete->delete();
        if($delete->trashed())
        {
            return true;
        }
        return false;
    }
    public function trash()
    {
        $data = Cuisine::onlyTrashed()->get();
        return $data;
    }
    public function restore($id)
    {
        $data = Cuisine::onlyTrashed()->updateOrCreate(['id'=>$id],['deleted_at'=>null]);
        if($data)
        {
            return true;
        }
        return false;
    }
}
