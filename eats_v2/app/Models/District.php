<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class District extends Model
{
    use SoftDeletes;
    protected $table ='districts';
    protected $guarded =[];
    protected $dates =['deleted_at'];
    public function province()
    {
        return $this->belongsTo('App\Models\Province','province_id');
    }
}
