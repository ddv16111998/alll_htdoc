<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Restaurant extends Model
{
    use SoftDeletes;
    protected $guarded = [];

    function province() {
        return $this->belongsTo('App\Models\Province');
    }
    function category() {
        return $this->belongsToMany('App\Models\Category','restaurat_categories');
    }
    function cuisine() {
        return $this->belongsToMany('App\Models\Cuisine','restaurat_cuisines');
    }
}
