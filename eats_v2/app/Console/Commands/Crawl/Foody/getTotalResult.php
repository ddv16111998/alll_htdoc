<?php

namespace App\Console\Commands\Crawl\Foody;

use Illuminate\Console\Command;
use App\Services\Foody\Lib;

class getTotalResult extends Command
{
    /* The name and signature of the console command. */
    protected $signature = 'crawl-foody:getTotalResult  {InputCate} {InputProvince}';

    /* The console command description. */
    protected $description = 'Get information total restaurant results';

    public function __construct()
    {
        parent::__construct();
    }

    /* Execute the console command. */
    public function handle()
    {
        $InputCate = $this->input->getArgument('InputCate'); // lấy category
        $InputProvince = $this->input->getArgument('InputProvince'); // lấy khu vực
        $urlString = trim(Lib::createUrl($InputCate,$InputProvince)); // tạo url
        $responseJson = Lib::getJson($urlString);
        $total_result = $responseJson->totalResult;
        $total_page   = ceil($responseJson->totalResult/12);
        $this->info($total_result."-".$total_page);
    }
}
