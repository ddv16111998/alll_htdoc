<?php

namespace App\Console\Commands\Crawl\Foody;

use Illuminate\Console\Command;
use App\Services\Foody\Lib;
use App\Services\Foody\Handling\Html;
use App\Services\Foody\Handling\Json;

class getRestaurant extends Command
{
    /**
     * The name and signature of the console command.
     * type = json/html
     * @var string
     */
    protected $signature = 'crawl-foody:getInfoRestaurant {pageEnd} {type} {url}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get basic information of a restaurant';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        /* lấy tham số */
        $url  = $this->input->getArgument('url');
        $type = $this->input->getArgument('type');
        $pageEnd = $this->input->getArgument('pageEnd');
        #
        $urlNoPage = Lib::renderUrl($url);
        $oldPage = \DB::table('md5_url')->where('md5',md5($urlNoPage))->pluck('page')->first();
        if ($oldPage < $pageEnd) {
            $page = ($oldPage) ? $oldPage+1 : 1;
            #
            $newUrl = $urlNoPage.'&page='.(int)$page;
            if ($type == 'html') {
                $res = Html::restaurant($newUrl,$page);
            } else if ($type == 'json') {
                $res = Json::restaurant($newUrl,$page);
            }
            if($res) {
                foreach ($res as $key => $value) {
                    $arrNew[] = $value['name'];
                }
                $arrNew['page'] = $page;
                dump($arrNew);
            } 
            else $this->info(1);
        }
        else {
            $this->info(1);
        }
    }
}
