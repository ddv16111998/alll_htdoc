<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Repositories\DistrictRepository;
use App\Repositories\ProvinceRepository;
class DistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $repository;
    function __construct(DistrictRepository $district)
    {
        $this->repository = $district;
    }

    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = $this->repository->getDataIndex();
            return Datatables::of($data)
                ->editColumn('province_id',function ($data){
                    if($data->province())
                    {
                        return $data->province->name;
                    }
                    return '---';
                })
                ->addColumn('action',function ($data){
                    return '
                    <a href="'.route('admin.district.edit',$data->id).'" class="btn btn-xs btn-warning"><i class="fas fa-edit"></i>Sửa</a>
                    <a href="'.route('admin.district.destroy',$data->id).'" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i> Xóa</a>';
                })
                ->rawColumns(['action'])
                ->toJson();
        }
        $count = $this->repository->countDelete();
        return view('admin.district.index',['count'=>$count]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(ProvinceRepository $province)
    {
        $provinces = $province->getDataIndex();
        $data['provinces'] = $provinces;
        return view('admin.district.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save($id,Request $request)
    {
        $district = $request->district;
        $slug = \Str::slug($district);
        $province_id = $request->province_id;
        $data = [
            'name' => $district,
            'slug'     => $slug,
            'province_id' => $province_id
        ];
        return $this->repository->updateOrCreate($data,$id);
    }
    public function store($id=null,Request $request)
    {
        $data = $this->save($id,$request);
        if($data)
        {
            return redirect()->route('admin.district.index')
                ->with(['message' => 'Thêm mới thành công', 'type' => 'success']);
        }
        else {
            return redirect()->route('admin.district.index')
                ->with(['message' => 'Thêm mới thất bại', 'type' => 'error']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id,ProvinceRepository $province)
    {
        $provinceOne = $this->repository->getDataById($id);
        $provinces = $province->getDataIndex();
        $data['provinces'] = $provinces;
        $data['provinceOne'] = $provinceOne;
        return view('admin.district.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $this->save($id,$request);
        if($data)
        {
            return redirect()->route('admin.district.index')
                ->with(['message' => 'Sửa thành công', 'type' => 'success']);
        }
        else {
            return redirect()->route('admin.district.index')
                ->with(['message' => 'Sửa thất bại', 'type' => 'error']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->repository->destroy($id);
        if($data)
        {
            return redirect()->route('admin.district.index')
                ->with(['message' => 'Xóa thành công', 'type' => 'success']);
        }
        else {
            return redirect()->route('admin.district.index')
                ->with(['message' => 'Xóa thất bại', 'type' => 'error']);
        }
    }
    public function trash(Request $request)
    {
        if($request->ajax())
        {
            $data = $this->repository->trash();
            return Datatables::of($data)
                ->addColumn('action',function ($data){
                    return '
                    <a href="'.route('admin.district.restore',$data->id).'" class="btn btn-xs btn-success"><i class="fa fa-undo"></i> Khôi phục</a>';
                })
                ->rawColumns(['action'])
                ->toJson();
        }
        return view('admin.district.trash');
    }
    public function restore($id)
    {
        $data = $this->repository->restore($id);
        if($data)
        {
            return redirect()->route('admin.district.index')
                ->with(['message' => 'Khôi phục thành công', 'type' => 'success']);
        }
        else {
            return redirect()->route('admin.district.index')
                ->with(['message' => 'Khôi phục thất bại', 'type' => 'error']);
        }
    }
}
