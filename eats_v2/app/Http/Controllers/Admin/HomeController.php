<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Foody\Handling\Html;

class HomeController extends Controller
{
	public function test()
	{
		// echo get_html('https://www.foody.vn/ha-noi/nha-hang?ds=Restaurant&vt=row&st=1&c=1&page=52&provinceId=218&categoryId=1&append=true');
        echo get_html('https://www.foody.vn/ho-chi-minh/dia-diem?ds=Restaurant&vt=row&st=1&provinceId=217&categoryId=&append=true&page=85');
        // $html = Html::restaurant('https://www.foody.vn/ho-chi-minh/dia-diem?ds=Restaurant&vt=row&st=1&provinceId=217&categoryId=&append=true&page=86',86);
        // dd($html);
	}
    public function index()
    {
    	$data['COUNT_NEW_DATA'] = \App\Models\Restaurant::where('status',config('modules.crawl.STATUS_ID.NEW_DATA'))->count();
    	$data['COUNT_DATA_GET_INFO'] = \App\Models\Restaurant::where('status','<>',config('modules.crawl.STATUS_ID.NEW_DATA'))->count();
    	return view('admin.dashboard.index',$data);
    }
}
