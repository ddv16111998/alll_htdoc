<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Restaurant;
use App\Models\Province;
use App\Models\Category;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class RestaurantController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if ($request->trash AND $request->trash==1) {
                $query = Restaurant::onlyTrashed();
            }
            else {
                $query = Restaurant::query();
            }
            $data = $query;
            return DataTables::of($data)
                ->editColumn('avatar', function ($data) {
                    return '<img src="/images/restaurants/'.$data->avatar.'" style="width:100px">';
                })
                ->editColumn('status', function ($data) {
                    return '<b>'.config('modules.crawl.STATUS_TITLE.'.$data->status).'</b>';
                })
                ->editColumn('created_at', function ($data) {
                    return date('H:i - d/m/Y',strtotime($data->created_at));
                })
                ->addColumn('action', function ($data) {
                    $str = '<a href="#" class="btn btn-xs btn-warning">Crawl Info</a>&nbsp;';
                    $str .= '<a href="#" class="btn btn-xs btn-info">Chi tiết</a>&nbsp;';
                    if ($data->deleted_at) {
                        $str .= '<a href="' . route('admin.data.restore', $data->id) . '" class="btn btn-xs btn-info">Phục hồi</a>';
                    } else {
                        $str .= '<a href="javascript:;" data-href="' . route('admin.data.destroy', $data->id) . '" class="btn btn-xs btn-danger btn-delete">Delete</a>';
                    }
                    return $str;
                })
                ->rawColumns(['action','picture','status'])
                ->make(true);
        }
        return view('admin.restaurant.index');
    }
    function destroy($id) {
        $data = Restaurant::find($id);
        $data->delete();
        if ($data->trashed()) {
            return redirect()->route('admin.restaurant.index')->with(['type'=>'success','mesage'=>'Xóa thành công']);
        }
        else {
            return redirect()->route('admin.restaurant.index')->with(['type'=>'error','mesage'=>'Xóa thất bại']);
        }
    }
    function restore($id) {
        $data = Restaurant::withTrashed()->find($id);
        $data->restore();
        return redirect()->route('admin.restaurant.index');
    }
}
