<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Repositories\CateRepository;

class CateController extends Controller
{
    private $repository;
    function __construct(CateRepository $cate)
    {
        $this->repository = $cate;
    }
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = $this->repository->getDataIndex();
            return Datatables::of($data)
                ->addColumn('action',function ($data){
                    return '
                        <a href="'.route('admin.cate.edit',$data->id).'" class="btn btn-xs btn-warning"><i class="fas fa-edit"></i>Sửa</a>
                        <a href="'.route('admin.cate.destroy',$data->id).'" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i> Xóa</a>';
                })
                ->rawColumns(['action'])
                ->toJson();
        }
        $count = $this->repository->countDelete();
        return view('admin.category.index',['count'=>$count]);
    }
    public function create()
    {
        return view('admin.category.create');
    }
    public function save($id,Request $request)
    {
        $cate = $request->cate;
        $data =[
          'name'=> $cate,
          'slug'=> \Str::slug($cate)
        ];
        return $this->repository->updateOrCreate($data,$id);
    }
    public function store($id=null,Request $request)
    {
        $data = $this->save($id,$request);
        if($data)
        {
            return redirect()->route('admin.cate.index')
                ->with(['message' => 'Thêm mới thành công', 'type' => 'success']);
        }
        else {
            return redirect()->route('admin.cate.index')
                ->with(['message' => 'Thêm mới thất bại', 'type' => 'error']);
        }
    }
    public function edit($id)
    {
        $data = $this->repository->getDataById($id);
        return view('admin.category.edit',['data'=>$data]);
    }
    public function update($id,Request $request)
    {
        $data = $this->save($id,$request);
        if($data)
        {
            return redirect()->route('admin.cate.index')
                ->with(['message' => 'Cập nhật thành công', 'type' => 'success']);
        }
        else {
            return redirect()->route('admin.cate.index')
                ->with(['message' => 'Cập nhật thất bại', 'type' => 'error']);
        }
    }
    public function destroy($id)
    {
        $data = $this->repository->destroy($id);
        if($data)
        {
            return redirect()->route('admin.cate.index')
                ->with(['message' => 'Xóa thành công', 'type' => 'success']);
        }
        else {
            return redirect()->route('admin.cate.index')
                ->with(['message' => 'Xóa thất bại', 'type' => 'error']);
        }
    }
    public function trash(Request $request)
    {
        if($request->ajax())
        {
            $data = $this->repository->trash();
            return Datatables::of($data)
                ->addColumn('action',function ($data){
                    return '
                        <a href="'.route('admin.cate.restore',$data->id).'" class="btn btn-xs btn-success"><i class="fas fa-undo"></i>Khôi phục</a>';
                })
                ->rawColumns(['action'])
                ->toJson();
        }
        return view('admin.category.trash');
    }
    public function restore($id)
    {
        $data = $this->repository->restore($id);
        if($data)
        {
            return redirect()->route('admin.cate.index')
                ->with(['message' => 'Khôi phục thành công', 'type' => 'success']);
        }
        else {
            return redirect()->route('admin.cate.index')
                ->with(['message' => 'Khôi phục thất bại', 'type' => 'error']);
        }
    }
}
