<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Repositories\ProvinceRepository;
class ProvinceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $repository;
    function __construct(ProvinceRepository $province)
    {
        $this->repository = $province;
    }

    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = $this->repository->getDataIndex();
            return Datatables::of($data)
                ->addColumn('action',function ($data){
                    return '
                <a href="'.route('admin.province.edit',$data->id).'" class="btn btn-xs btn-warning"><i class="fas fa-edit"></i>Sửa</a>
                <a href="'.route('admin.province.destroy',$data->id).'" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i> Xóa</a>';
                })
                ->rawColumns(['action'])
                ->toJson();
        }
        $count = $this->repository->countDelete();
        return view('admin.province.index',['count'=>$count]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.province.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save($id,Request $request)
    {
        $province = $request->province;
        $slug = \Str::slug($province);
        $data = [
            'name' => $province,
            'slug'     => $slug,
        ];
        return $this->repository->updateOrCreate($data,$id);
    }
    public function store($id=null,Request $request)
    {
            $data = $this->save($id,$request);
            if($data)
            {
                return redirect()->route('admin.province.index')
                    ->with(['message' => 'Thêm mới thành công', 'type' => 'success']);
            }
            else {
                return redirect()->route('admin.province.index')
                    ->with(['message' => 'Thêm mới thất bại', 'type' => 'error']);
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = $this->repository->getDataById($id);
        return view('admin.province.edit',['data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $this->save($id,$request);
        if($data)
        {
            return redirect()->route('admin.province.index')
                ->with(['message' => 'Sửa thành công', 'type' => 'success']);
        }
        else {
            return redirect()->route('admin.province.index')
                ->with(['message' => 'Sửa thất bại', 'type' => 'error']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = $this->repository->destroy($id);
        if($data)
        {
            return redirect()->route('admin.province.index')
                ->with(['message' => 'Xóa thành công', 'type' => 'success']);
        }
        else {
            return redirect()->route('admin.province.index')
                ->with(['message' => 'Xóa thất bại', 'type' => 'error']);
        }
    }
    public function trash(Request $request)
    {
       if($request->ajax())
       {
           $data = $this->repository->trash();
           return Datatables::of($data)
               ->addColumn('action',function ($data){
                   return '
                    <a href="'.route('admin.province.restore',$data->id).'" class="btn btn-xs btn-success"><i class="fa fa-undo"></i> Khôi phục</a>';
               })
               ->rawColumns(['action'])
               ->toJson();
       }
        return view('admin.province.trash');
    }
    public function restore($id)
    {
        $data = $this->repository->restore($id);
        if($data)
        {
            return redirect()->route('admin.province.index')
                ->with(['message' => 'Khôi phục thành công', 'type' => 'success']);
        }
        else {
            return redirect()->route('admin.province.index')
                ->with(['message' => 'Khôi phục thất bại', 'type' => 'error']);
        }
    }
}
