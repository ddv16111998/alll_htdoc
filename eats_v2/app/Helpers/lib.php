<?php

if (!function_exists('get_html')) {
    function get_html($url)
    {
    	$type = config('modules.crawl.TYPE');
    	if ($type == 'json') {
    		$headers = array(
    			"Host: www.foody.vn",
				"Connection: keep-alive",
				"Accept: application/json, text/javascript, */*; q=0.01",
				"X-Requested-With: XMLHttpRequest",
				"X-Foody-User-Token: kCNb0NARh4TSaduQ9V7Mi9IYLS90laREHKhAKkZXRUTnl4tzwRHL6sgGSEtq",
				"User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36",
				"Sec-Fetch-Mode: cors",
				"Sec-Fetch-Site: same-origin",
				"Accept-Encoding: gzip, deflate, br",
				"Accept-Language: en-US,en;q=0.9",
				"Cookie: _fbp=fb.1.1569598799791.1555045689; flg=vn; FOODY.AUTH.UDID=5e0c68fe-be22-4d5e-af89-59fcf8a4aefa; FOODY.AUTH=C99334CE3A03A4716E254260DE759C0537D62226370E1A548DD4DD99BD243C8BE0767170AE4417D31AFFCC2EFDD54F711A9DDFEBD1B10D30159FC7650B8793C05CBBAA2DF039D5A98500CDCD501A414A2F0BCEDB256498AB865E173DDC2A4B3444A02B7E8A19316829EF40068BECF6D9A618227439108B49104A495E7D3C417AD88EAF63740BFE7C5B531832403D1859F70A6AC8498925F2C9535FBC77161963CE7E1ADA2E8EBFD305B57FDD26E33931C06008B61DDB74FD1397CD863A461074E8EF528CFE9E2F65DFA6E4D0E9799037740A3290179A1A08FABEFBE907DB25F257B91AF5BA38574CA156536629FAF1EA; fd.keys=; _ga=GA1.2.1105526034.1569598806; _gid=GA1.2.131592050.1569598806; __utmz=257500956.1569598806.1.1.utmcsr=id.foody.vn|utmccn=(referral)|utmcmd=referral|utmcct=/dang-nhap-thanh-cong; xfci=VOP8YKFLKZ369SY; __ondemand_sessionid=4cw3wzftfiybdrg41xsekxp5; gcat=food; __utmc=257500956; fd.res.view.218=958152,909677,641068,142863,7657; floc=217; _gat=1; _gat_ads=1; __utma=257500956.1105526034.1569598806.1569668929.1569687078.4; __utmt_UA-33292184-1=1; __utmb=257500956.3.10.1569687078",
    		);
    	} else {
    		$headers = array(
	    		"Accept: */*",
			    "Accept-Encoding: gzip, deflate",
			    "Cache-Control: no-cache",
			    "Connection: keep-alive",
			    "Cookie: flg=vn; floc=218; __ondemand_sessionid=vnh3vgtkrub5qtk2fvw4kwmb; fd.keys=",
			    "Host: www.foody.vn",
			    "Postman-Token: 3a401400-0810-4a1e-85c1-97638d3315f8,fbe3e2a4-66ab-4cb8-ac15-d9e399e3caa0",
			    "User-Agent: PostmanRuntime/7.16.3",
			    "cache-control: no-cache"
			);
    	}
        $curl = curl_init();
		curl_setopt_array($curl, array(
			  	CURLOPT_URL => $url,
			  	CURLOPT_RETURNTRANSFER => true,
			  	CURLOPT_ENCODING => "",
			  	CURLOPT_MAXREDIRS => 10,
			  	CURLOPT_TIMEOUT => 30,
			  	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			  	CURLOPT_CUSTOMREQUEST => "GET",
			  	CURLOPT_HTTPHEADER => $headers,
		));
		$html = curl_exec($curl);
		$dom  = new \simple_html_dom(null, true, true, DEFAULT_TARGET_CHARSET, true, DEFAULT_BR_TEXT, DEFAULT_SPAN_TEXT);
        $html = $dom->load($html, true, true);
        return $html;
    }
}

