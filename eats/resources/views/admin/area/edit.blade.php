@extends('admin.theme.master')

@section('css')
@endsection

@section('js')
@endsection

@section('content')
    <div id="list-crud">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">
                        Sửa khu vực
                    </h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Khu vực</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.area.index')}}">Danh sách khu vực</a></li>
                        <li class="breadcrumb-item"><a href="{{route('admin.area.create')}}">Thêm mới khu vực</a></li>
                        <li class="breadcrumb-item active"><a href="">Sửa khu vực</a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <form class="form form-submit" method="POST" action="{{route('admin.area.update',$area->id)}}">
                    <div id="main-content">
                        <div class="left-form">
                            <div class="card">
                                <div class="card-body">
                                    @csrf
                                    <div class="form-group row justify-content-md-center">
                                        <label for="area-input" class="col-md-2 text-center col-form-label">Khu vực <span class="text-danger">*</span></label>
                                        <div class="col-md-5">
                                            <input class="form-control " type="text" id="area-input" name="area" value="{{$area->name}}">
                                            @error('area')
                                            <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <section class="text-right">
                                        <div class="row justify-content-around">
                                            <div class="col-md-7 col-sm-8">
                                                <a href="{{route('admin.area.index')}}" class="btn btn-secondary mr-1">Hủy bỏ</a>
                                                <input type="submit" class="btn btn-info" value="Lưu">
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
