<style>
    .skin-default-dark .sidebar-nav > ul > li.active > a {border-color:#fff}
</style>
<aside class="left-sidebar">
    <div class="scroll-sidebar">
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- QUẢN LÝ -->
                <li class="nav-small-cap">--- QUẢN LÝ</li>
                <li>
                    <a class="waves-effect waves-dark" href="{{route('admin.dashboard')}}" aria-expanded="false">
                        <i class="icon-speedometer"></i>
                        <span class="hide-menu">Dashboard</span>
                    </a>
                </li>
                {{-- <li>
                    <a class="waves-effect waves-dark" href="{{route('admin.data.index')}}" aria-expanded="false">
                        <i class="ti-help-alt"></i>
                        <span class="hide-menu">Datas</span>
                    </a>
                </li> --}}

                <!-- KHU VỰC -->
                <li class="nav-small-cap">--- KHU VỰC</li>
                <li>
                    <a class="waves-effect waves-dark" href="{{route('admin.area.index')}}" aria-expanded="false">
                        <i class="icon-location-pin"></i>
                        <span class="hide-menu">Khu vực</span>
                    </a>
                </li>
                <!-- DANH MỤC -->
                <li class="nav-small-cap">--- DANH MỤC</li>
                <li>
                    <a class="waves-effect waves-dark" href="{{route('admin.cate.index')}}" aria-expanded="false">
                        <i class="icon-list"></i>
                        <span class="hide-menu">Danh mục</span>
                    </a>
                </li>

                <!-- Tỉnh - Huyện -->
                <li class="nav-small-cap">--- TỈNH - HUYỆN</li>
                <li>
                    <a class="waves-effect waves-dark" href="{{route('admin.province.index')}}" aria-expanded="false">
                        <i class=""></i>
                        <span class="hide-menu">Tỉnh</span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="" aria-expanded="false">
                        <i class=""></i>
                        <span class="hide-menu">Huyện</span>
                    </a>
                </li>

                <!-- HỆ THỐNG -->
                <li class="nav-small-cap">--- HỆ THỐNG</li>
                <li>
                    <a class="waves-effect waves-dark" href="{{route('admin.crawl.index')}}">
                        <i class="ti-search"></i>
                        <span class="hide-menu">Crawl</span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="pages-faq.html" aria-expanded="false">
                        <i class="ti-help-alt"></i>
                        <span class="hide-menu">FAQs</span>
                    </a>
                </li>
                <li>
                    <a class="waves-effect waves-dark" href="#">
                        <i class="ti-settings"></i>
                        <span class="hide-menu">System</span>
                    </a>
                </li>

                <li>
                    <a class="waves-effect waves-dark" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" aria-expanded="false">
                        <i class="icon-power text-danger"></i>
                        <span class="hide-menu">Log Out</span>
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
        </nav>
    </div>
</aside>
