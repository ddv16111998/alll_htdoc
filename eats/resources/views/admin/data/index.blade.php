@extends('theme.master')
@section('title', 'Datas')
@section('js')
    <script>
        $(function() {
            var url = '{{route('admin.data.index')}}?ds=true';

            @if(isset($_GET['trash']))
                url += "&trash={{$_GET['trash']}}";
            @endif
            @if(isset($_GET['province']))
                url += "&province={{$_GET['province']}}";
            @endif
            @if(isset($_GET['category']))
                url += "&category={{$_GET['category']}}";
            @endif
            @if(isset($_GET['source']))
                url += "&source={{$_GET['source']}}";
            @endif
            @if(isset($_GET['status']))
                url += "&status={{$_GET['status']}}";
            @endif

            $('#listData').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'picture', name: 'picture' },
                    // { data: 'url_detail', name: 'url_detail',orderable: false },
                    { data: 'location', name: 'location' },
                    { data: 'category_id', name: 'category_id',orderable: false },
                    { data: 'cuisine_id', name: 'cuisine_id',orderable: false },
                    { data: 'source', name: 'source' },
                    { data: 'status', name: 'status' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'action', name : 'action',orderable: false },
                ],
                "aoColumnDefs": [
                    { "bSearchable": false, "aTargets": [ 5 ] }
                ]
            });
        });
    </script>
@endsection
@section('content')
    <div id="list-crud">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">
                        Datas
                        @if(isset($_GET['trash']))
                            @if($_GET['trash']==1)
                                <a style="margin-left:10px" href="{{route('admin.data.index')}}" class="btn btn-sm btn-info">Danh sách</a>
                            @else
                                <a style="margin-left:10px" href="{{route('admin.data.index')}}?trash=1" class="btn btn-sm btn-warning">Thùng rác</a>
                            @endif
                        @else
                            <a style="margin-left:10px" href="{{route('admin.data.index')}}?trash=1" class="btn btn-sm btn-warning">Thùng rác</a>
                        @endif
                    </h4>
                    <form method="GET">
                    <ul style="list-style:none">
                        <input type="hidden" name="trash" value="{{ isset($_GET['trash']) ? $_GET['trash'] : 0 }}">
                        <li style="display:inline-block;">
                            <select class="form-control form-control-sm" name="province">
                                <option value="">-- Tỉnh/TP --</option>
                                @foreach ($province as $elm_pr)
                                    <option @if (isset($_GET['province']))
                                        @if($_GET['province']==$elm_pr['slug']) selected @endif
                                    @endif value="{{$elm_pr['slug']}}">{{$elm_pr['name']}}</option>
                                @endforeach
                            </select>
                        </li>
                        <li style="display:inline-block;">
                            <select class="form-control form-control-sm" name="category">
                                <option value="">-- Hình thức --</option>
                                @foreach ($category as $elm_ct)
                                    <option @if (isset($_GET['category']))
                                        @if($_GET['category']==$elm_ct['id']) selected @endif
                                    @endif value="{{$elm_ct['id']}}">{{$elm_ct['name']}}</option>
                                @endforeach
                            </select>
                        </li>
                        <li style="display:inline-block;">
                            <select class="form-control form-control-sm" name="source">
                                <option value="">-- Nguồn --</option>
                                @foreach ($source as $k_s => $elm_s)
                                    <option @if (isset($_GET['source']))
                                        @if($_GET['source']==$elm_s) selected @endif
                                    @endif value="{{$elm_s}}">{{$k_s}}</option>
                                @endforeach
                            </select>
                        </li>
                        <li style="display:inline-block;">
                            <select class="form-control form-control-sm" name="status">
                                <option value="">-- Trạng thái --</option>
                                @foreach ($status as $k_st => $v_st)
                                    <option @if (isset($_GET['status']))
                                        @if($_GET['status']==$k_st) selected @endif
                                    @endif value="{{$k_st}}">{{$v_st}}</option>
                                @endforeach
                            </select>
                        </li>
                        <li style="display:inline-block;">
                            <button class="btn btn-primary"> Lọc </button>
                        </li>
                    </ul>
                    </form>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="">Datas </a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div>
                                    <table class="table table-hover mt-4" id="listData">
                                        <thead>
                                            <th>#</th>
                                            <th style="width:225px">Nhà hàng,Quán ăn</th>
                                            <th>Ảnh đại diện</th>
                                            {{-- <th style="width:50px">Url</th> --}}
                                            <th>Khu vực</th>
                                            <th>Danh mục</th>
                                            <th>Ẩm thực</th>
                                            <th>Nguồn</th>
                                            <th>Trạng thái</th>
                                            <th>Ngày tạo</th>
                                            <th>Action</th>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
