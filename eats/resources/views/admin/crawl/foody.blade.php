@extends('admin.theme.master')
@section('title', 'Crawl Foody')
@section('js')
    <script>
        $.ajaxSetup({headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
        function Filter() {
            $('.iconloadren').show();
            $('.showtotal').hide();
            InputCate=$('.CateId').val();InputProvince=$('.ProvinceId').val();
            if (InputProvince!=0) {
                var formData = new FormData();
                formData.append('InputCate', InputCate);
                formData.append('InputProvince', InputProvince);
                $.ajax({
                    type: "POST",
                    url: "{{ route('admin.crawl.foody.get-total-result') }}",
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    success: function(data){
                        $('.total-result').text(data.total_result);
                        $('.total_page').text(data.total_page);
<<<<<<< HEAD
                        $('.url').val(UrlString);
                        $('.over-loading').removeClass('avtive');
                        console.log(data);
=======
                        $('.urlcate').val(InputCate);
                        $('.urlpro').val(InputProvince);
                        $('.iconloadren').hide();
                        $('.showtotal').show();
                        $('.btn-quet').show();
>>>>>>> develop
                    }
                });
            }
        }
        $('.btn-quet').click(function(){
            let time = 5000+Math.floor(Math.random()*200);
            $('.iconload').show();
            let urlcate = $('.urlcate').val(),urlpro = $('.urlpro').val();
            let formData = new FormData();
            formData.append('urlcate',urlcate);
            formData.append('urlpro',urlpro);
            $.ajax({
                type: "POST",
                url: "{{ route('admin.crawl.foody.action') }}",
                data: formData,
                dataType: 'html',
                contentType: false,
                processData: false,
                success: function(data){
                    console.log(data);
                    $('.results').html(data);
                    $('.iconload').hide();
                    if (data != 1) {
                        setTimeout(
                            function() 
                            {
                                $('.btn-quet').click();
                            }, time);
                       console.log("Crawler");
                    } else {
                        alert('Done');
                    }
                }
            });
        });
    </script>
@endsection
@section('content')
    <div id="list-crud">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">
                        Crawl Foody
                    </h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="">Crawl Foody</a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="option">
                                    <select onchange="Filter()" class="CateId form-control col-1">
                                        <option value="0" selected="selected">- Danh mục -</option>
                                        <option value="12={{ config('modules.foody.category.12') }}">Sang trọng</option>
                                        <option value="39={{config('modules.foody.category.39')}}">Buffet</option>
                                        <option value="1={{config('modules.foody.category.1')}}">Nhà hàng</option>
                                        <option value="11={{config('modules.foody.category.11')}}">Ăn vặt/vỉa hè</option>
                                        <option value="56={{config('modules.foody.category.56')}}">Ăn chay</option>
                                        <option value="2={{config('modules.foody.category.2')}}">Café/Dessert</option>
                                        <option value="3={{config('modules.foody.category.3')}}">Quán ăn</option>
                                        <option value="4={{config('modules.foody.category.4')}}">Bar/Pub</option>
                                        <option value="54={{config('modules.foody.category.54')}}">Quán nhậu</option>
                                        <option value="43={{config('modules.foody.category.43')}}">Beer club</option>
                                        <option value="6={{config('modules.foody.category.6')}}">Tiệm bánh</option>
                                        <option value="44={{config('modules.foody.category.44')}}">Tiệc tận nơi</option>
                                        <option value="27={{config('modules.foody.category.27')}}">Shop Online</option>
                                        <option value="28={{config('modules.foody.category.28')}}">Giao cơm văn phòng</option>
                                        <option value="79={{config('modules.foody.category.79')}}">Khu Ẩm Thực</option>
                                    </select>
                                    <select onchange="Filter()" class="ProvinceId form-control col-1">
                                        <option value="0">- Khu vực -</option>
                                        @include('admin.crawl.include-province-foody')
                                    </select>
                                    <div class="showtotal form-control col-1" style="padding:10px">
                                        Có <b class="total_page" style="color:#f33">0</b> trang - <b class="total-result" style="color:#f33">0</b> kết quả.
                                    </div>
                                    <i class="iconloadren fa fa-spinner fa-pulse" style="margin:0 20px;display:none"></i>
                                    <div class="col-1 form-control" style="border:none">
                                        <button class="btn btn-quet btn-primary" style="display:none;padding:8px 20px;height:auto!important;">
                                            Quét &nbsp;<i class="iconload fa fa-spinner fa-pulse" style="display:none"></i>
                                        </button>
                                    </div>
                                </div>
                                <hr>
                                <div class="form" style="margin-top:20px">
                                    <input class="urlcate" type="hidden">
                                    <input class="urlpro" type="hidden">
                                </div>
                                <div class="results" style="min-height:200px;border:1px solid #eee;margin-top:20px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
