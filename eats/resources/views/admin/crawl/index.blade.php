@extends('admin.theme.master')
@section('title', 'Site Crawl')
@section('content')
    <div id="list-crud">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">
                        Site Crawl
                    </h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active"><a href="">Site Crawl</a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <style>
                                    .list-option .col-1 a {
                                        width: 100%;
                                        background: #f33;
                                        display: flex;
                                        height: 38px;
                                        color: #fff;
                                        align-items: center;
                                    }
                                    .list-option .col-1 a span {
                                        margin: 0 auto;
                                        font-size: 18px;
                                    }
                                </style>
                                <div class="use-link">
                                    <div class="row list-option">
                                        <div class="col-1">
                                            <a href="{{ route('admin.crawl.foody') }}"><span>Foody</span></a>
                                        </div>
                                        <div class="col-1">
                                            <a href="pasgo/url"><span>PasGo</span></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
