$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('.img_change').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
$(".inp__img").change(function() {
    readURL(this);
});
jQuery.fn.center = function () {
    this.css("position","relative");
    this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
    this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
    return this;
}
$("#form-actions").css("width", $('#main-content').width() + 25).css('margin','0 auto');
$("#form-footer").css("width", $('.page-wrapper').width() + 25);

function notification( type, message ) {
    $.toast({
        text: message,
        position: 'top-right',
        loaderBg:'#ff6849',
        icon: type,
        stack: 6,
        hideAfter: 10000
    });
}

$(document).on('click', '.btn-delete', function() {
    var result = confirm("Bạn có muốn xóa ?");
    if (result) {
        location.href = $(this).attr('data-href');
    }
});
