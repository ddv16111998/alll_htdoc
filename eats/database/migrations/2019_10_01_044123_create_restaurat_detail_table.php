<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRestauratDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurat_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('restaurat_id')->unsigned();
            $table->foreign('restaurat_id')->references('id')->on('restaurats')->onDelete('cascade');
            $table->string('phone')->nullable();
            $table->string('email')->nullable();
            $table->string('website')->nullable();
            $table->string('fanpage')->nullable();
            $table->string('promotions')->nullable()->comment('khuyến mãi');
            $table->integer('is_special_desc')->nullable();
            $table->integer('total_review')->nullable();
            $table->integer('total_view')->nullable();
            $table->integer('total_favourite')->nullable();
            $table->integer('total_checkins')->nullable();
            $table->text('guide')->nullable()->comment('chỉ đường');
            $table->text('capacity')->nullable()->comment('sức chứa');
            $table->text('convenient')->nullable()->comment('tiện nghi');
            $table->text('intent')->nullable()->comment('mục đích');
            $table->text('speciality')->nullable()->comment('món đặc trưng');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurat_detail');
    }
}
