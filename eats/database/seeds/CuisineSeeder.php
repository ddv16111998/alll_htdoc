<?php

use Illuminate\Database\Seeder;

class CuisineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            ["Món Việt", "mon-viet" , 1],
            ["Món Miền Nam", "mon-mien-nam" , 12],
            ["Món Bắc", "mon-bac" , 2],
            ["Món Miền Trung", "mon-mien-trung" , 7],
            ["Tây Nguyên", "tay-nguyen" , 29],
            ["Món Á", "mon-a" , 36],
            ["Món Hàn", "mon-han" , 22],
            ["Món Nhật", "mon-nhat" , 21],
            ["Món Trung Hoa", "mon-trung-hoa" , 4],
            ["Món Thái", "mon-thai" , 17],
            ["Singapore", "singapore" , 30],
            ["Malaysia", "malaysia" , 31],
            ["Campuchia", "campuchia" , 38],
            ["Philippines", "philippines" , 42],
            ["Indonesia", "indonesia" , 37],
            ["Món Ấn Độ", "mon-an-do" , 16],
            ["Đài Loan", "dai-loan" , 51],
            ["Iran", "iran" , 53],
            ["Món Âu", "mon-au" , 34],
            ["Pháp", "phap" , 19],
            ["Ý", "mon-y" , 18],
            ["Đức", "duc" , 20],
            ["Tây Ban Nha", "tay-ban-nha" , 35],
            ["Thụy sĩ", "thuy-si" , 25],
            ["Bánh Pizza", "banh-pizza" , 52],
            ["Châu Mỹ", "chau-my" , 24],
            ["Mỹ", "my" , 33],
            ["Brazil", "brazil" , 32],
            ["Mexico", "mexico" , 39],
            ["Canada", "canada" , 40],
            ["Argentina", "argentina" , 46],
            ["Quốc tế", "quoc-te" , 50],
            ["Đông Âu", "dong-au" , 28],
            ["Thổ Nhĩ Kỳ", "tho-nhi-ky" , 43],
            ["Tiệp (Séc)", "tiep-sec" , 44],
            ["Úc", "uc" , 47],
            ["Bắc Âu", "bac-au" , 26],
            ["Trung Đông", "trung-dong" , 27],
            ["Ả Rập", "a-rap" , 41],
            ["Châu Phi", "chau-phi" , 23],
        ];
    	\DB::insert('insert into cuisines (name,slug,cs_fd_id) values (?,?,?,?)',$arr);
    }
}
