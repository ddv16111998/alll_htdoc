<?php

use Illuminate\Database\Seeder;

class ProvinceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('provinces')->insert([
        	[ 'prv_fd_id' => 1, 'name' => 'Hồ Chí Minh', 'slug' => 'ho-chi-minh'],
			[ 'prv_fd_id' => 2, 'name' => 'Hà Nội', 'slug' => 'ha-noi'],
			[ 'prv_fd_id' => 3, 'name' => 'Đà Nẵng', 'slug' => 'da-nang'],
			[ 'prv_fd_id' => 4, 'name' => 'Cần Thơ', 'slug' => 'can-tho'],
			[ 'prv_fd_id' => 5, 'name' => 'Khánh Hòa', 'slug' => 'khanh-hoa'],
			[ 'prv_fd_id' => 6, 'name' => 'Vũng Tàu', 'slug' => 'vung-tau'],
			[ 'prv_fd_id' => 7, 'name' => 'Hải Phòng', 'slug' => 'hai-phong'],
			[ 'prv_fd_id' => 8, 'name' => 'Bình Thuận', 'slug' => 'binh-thuan'],
			[ 'prv_fd_id' => 9, 'name' => 'Lâm Đồng', 'slug' => 'lam-dong'],
			[ 'prv_fd_id' => 10, 'name' => 'Đồng Nai', 'slug' => 'dong-nai'],
			[ 'prv_fd_id' => 11, 'name' => 'Quảng Ninh', 'slug' => 'quang-ninh'],
			[ 'prv_fd_id' => 12, 'name' => 'Huế', 'slug' => 'hue'],
			[ 'prv_fd_id' => 13, 'name' => 'Bình Dương', 'slug' => 'binh-duong'],
			[ 'prv_fd_id' => 14, 'name' => 'Hải Dương', 'slug' => 'hai-duong'],
			[ 'prv_fd_id' => 15, 'name' => 'Ninh Thuận', 'slug' => 'ninh-thuan'],
			[ 'prv_fd_id' => 16, 'name' => 'Nam Định', 'slug' => 'nam-dinh'],
			[ 'prv_fd_id' => 17, 'name' => 'Tiền Giang', 'slug' => 'tien-giang'],
			[ 'prv_fd_id' => 18, 'name' => 'Phú Quốc', 'slug' => 'phu-quoc'],
			[ 'prv_fd_id' => 19, 'name' => 'Kon Tum', 'slug' => 'kon-tum'],
			[ 'prv_fd_id' => 20, 'name' => 'Quảng Nam', 'slug' => 'quang-nam'],
			[ 'prv_fd_id' => 21, 'name' => 'Lào Cai', 'slug' => 'lao-cai'],
			[ 'prv_fd_id' => 22, 'name' => 'Nghệ An', 'slug' => 'nghe-an'],
			[ 'prv_fd_id' => 23, 'name' => 'Long An', 'slug' => 'long-an'],
			[ 'prv_fd_id' => 24, 'name' => 'Bình Định', 'slug' => 'binh-dinh'],
			[ 'prv_fd_id' => 25, 'name' => 'Phú Yên', 'slug' => 'phu-yen'],
			[ 'prv_fd_id' => 26, 'name' => 'Đắk Lắk', 'slug' => 'dak-lak'],
			[ 'prv_fd_id' => 27, 'name' => 'An Giang', 'slug' => 'an-giang'],
			[ 'prv_fd_id' => 28, 'name' => 'Thanh Hóa', 'slug' => 'thanh-hoa'],
			[ 'prv_fd_id' => 29, 'name' => 'Kiên Giang', 'slug' => 'kien-giang'],
			[ 'prv_fd_id' => 30, 'name' => 'Quảng Ngãi', 'slug' => 'quang-ngai'],
			[ 'prv_fd_id' => 31, 'name' => 'Tây Ninh', 'slug' => 'tay-ninh'],
			[ 'prv_fd_id' => 32, 'name' => 'Gia Lai', 'slug' => 'gia-lai'],
			[ 'prv_fd_id' => 33, 'name' => 'Vĩnh Long', 'slug' => 'vinh-long'],
			[ 'prv_fd_id' => 34, 'name' => 'Cà Mau', 'slug' => 'ca-mau'],
			[ 'prv_fd_id' => 35, 'name' => 'Đồng Tháp', 'slug' => 'dong-thap'],
			[ 'prv_fd_id' => 36, 'name' => 'Quảng Bình', 'slug' => 'quang-binh'],
			[ 'prv_fd_id' => 37, 'name' => 'Quảng Trị', 'slug' => 'quang-tri'],
			[ 'prv_fd_id' => 38, 'name' => 'Bến Tre', 'slug' => 'ben-tre'],
			[ 'prv_fd_id' => 39, 'name' => 'Bình Phước', 'slug' => 'binh-phuoc'],
			[ 'prv_fd_id' => 40, 'name' => 'Bắc Ninh', 'slug' => 'bac-ninh'],
			[ 'prv_fd_id' => 41, 'name' => 'Sóc Trăng', 'slug' => 'soc-trang'],
			[ 'prv_fd_id' => 42, 'name' => 'Vĩnh Phúc', 'slug' => 'vinh-phuc'],
			[ 'prv_fd_id' => 43, 'name' => 'Trà Vinh', 'slug' => 'tra-vinh'],
			[ 'prv_fd_id' => 44, 'name' => 'Thái Nguyên', 'slug' => 'thai-nguyen'],
			[ 'prv_fd_id' => 45, 'name' => 'Ninh Bình', 'slug' => 'ninh-binh'],
			[ 'prv_fd_id' => 46, 'name' => 'Bạc Liêu', 'slug' => 'bac-lieu'],
			[ 'prv_fd_id' => 47, 'name' => 'Hà Tĩnh', 'slug' => 'ha-tinh'],
			[ 'prv_fd_id' => 48, 'name' => 'Bắc Giang', 'slug' => 'bac-giang'],
			[ 'prv_fd_id' => 49, 'name' => 'Phú Thọ', 'slug' => 'phu-tho'],
			[ 'prv_fd_id' => 50, 'name' => 'Hậu Giang', 'slug' => 'hau-giang'],
			[ 'prv_fd_id' => 51, 'name' => 'Thái Bình', 'slug' => 'thai-binh'],
			[ 'prv_fd_id' => 52, 'name' => 'Sơn La', 'slug' => 'son-la'],
			[ 'prv_fd_id' => 53, 'name' => 'Lạng Sơn', 'slug' => 'lang-son'],
			[ 'prv_fd_id' => 54, 'name' => 'Hòa Bình', 'slug' => 'hoa-binh'],
			[ 'prv_fd_id' => 55, 'name' => 'Hưng Yên', 'slug' => 'hung-yen'],
			[ 'prv_fd_id' => 56, 'name' => 'Hà Giang', 'slug' => 'ha-giang'],
			[ 'prv_fd_id' => 57, 'name' => 'Đắk Nông', 'slug' => 'dak-nong'],
			[ 'prv_fd_id' => 58, 'name' => 'Tuyên Quang', 'slug' => 'tuyen-quang'],
			[ 'prv_fd_id' => 59, 'name' => 'Yên Bái', 'slug' => 'yen-bai'],
			[ 'prv_fd_id' => 60, 'name' => 'Hà Nam', 'slug' => 'ha-nam'],
			[ 'prv_fd_id' => 61, 'name' => 'Điện Biên', 'slug' => 'dien-bien'],
			[ 'prv_fd_id' => 62, 'name' => 'Cao Bằng', 'slug' => 'cao-bang'],
			[ 'prv_fd_id' => 63, 'name' => 'Lai Châu', 'slug' => 'lai-chau'],
			[ 'prv_fd_id' => 64, 'name' => 'Bắc Kạn', 'slug' => 'bac-kan'],
        ]);
    }
}
