<?php

namespace App\Services;

class District
{
	public static function checkAndSave($data)
	{
		$districtId = \DB::table('districts')->where('slug',$data['slug'])->pluck('id')->first();
		if ($districtId) {
			return $districtId;
		} else {
			$data['created_at'] = date('Y-m-d H:i:s');
            $insertGetId = \DB::table('districts')->insertGetId($data);
            return $insertGetId;
		}
	}
}