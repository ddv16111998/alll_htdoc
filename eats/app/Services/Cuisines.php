<?php

namespace App\Services;

class Cuisines
{
	public static function checkAndSave($data)
	{
		$cuisineId = \DB::table('cuisines')->where('slug',$data['slug'])->pluck('id')->first();
		if ($cuisineId) {
			return $cuisineId;
		} else {
			$data['created_at'] = date('Y-m-d H:i:s');
            $insertGetId = \DB::table('cuisines')->insertGetId($data);
            return $insertGetId;
		}
	}
}