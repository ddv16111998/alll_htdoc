<?php

namespace App\Services;

class CrawlFoody
{
    public static function createUrl($InputCate, $InputProvince)
    {
        $UrlString='https://www.foody.vn/';
        if ($InputProvince!=0)
        {
            $ArrProvince    = explode('=',$InputProvince);
            $ProvinceId     = $ArrProvince[0];
            $ProvinceSlug   = $ArrProvince[1];
            $UrlString .= $ProvinceSlug;
            if ($InputCate!=0) {
                $ArrCate    = explode('=',$InputCate);
                $CateId     = $ArrCate[0];
                $CateSlug   = $ArrCate[1];
                $UrlString .='/'.$CateSlug.'?ds=Restaurant&vt=row&st=1&c='.$CateId.'&provinceId='.$ProvinceId.'&categoryId='.$CateId.'&append=true&page=1';
            } else {
                $UrlString .='/dia-diem?ds=Restaurant&vt=row&st=1&provinceId='.$ProvinceId.'&categoryId=&append=true&page=1';
            }
        }
        return $UrlString;
    }
	public static function getJson($url)
    {
        #============================ Get Json Response ============================
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Host: www.foody.vn",
                "Connection: keep-alive",
                "Accept: application/json, text/javascript, */*; q=0.01",
                "X-Requested-With: XMLHttpRequest",
                "X-Foody-User-Token: kCNb0NARh4TSaduQ9V7Mi9IYLS90laREHKhAKkZXRUTnl4tzwRHL6sgGSEtq",
                "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36",
                "Sec-Fetch-Mode: cors",
                "Sec-Fetch-Site: same-origin",
                "Accept-Encoding: gzip, deflate, br",
                "Accept-Language: en-US,en;q=0.9",
                "Cookie: flg=vn; gcat=food; _ga=GA1.2.1312918708.1569081844; __utmz=257500956.1569081845.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none); _fbp=fb.1.1569081844897.1142373852; xfci=MELKDUFQ4EJMLIC; fd.keys=; floc=218; _gid=GA1.2.1201171068.1569501082; __ondemand_sessionid=dabtr5fx4f0k4gjw2s0lqd4s; __utmc=257500956; __utma=257500956.1312918708.1569081844.1569587562.1569595413.10; FOODY.AUTH.UDID=ab8861f0-ee2b-44aa-b5f2-b271aa49252d; FOODY.AUTH=FE2F9DF06E4FA8E5A0B772CDCBC8A4683299C86CA6DF48B0BE64808795908BC6182C81EF9DB15D9A52106E68088E207E92947F78840BB03570B397BBC4C470791DB22CD035A5031B8D259D697B8884EA7C65DF8FFD5DD8D487D95D3E7D81D6687F4DA4A4CCBD0465605B1689CB2DC1915D0D6065ED04AC2F8B9F5E7CD47271617253CD3C53A28FFEF7EC07CDD187ACA5F6A3AAD62D6A0D687A25A0BBC25E657E2AAE2B69D390C1FD60CBA5C5F40BEE8B009F2B2621C62D6E696A412C2162C657A129A5644CF90E64BB65A8E1D0B47F25BC6AC28CC35E7081FFF8AA454D216B959AF458A5209A9BECFBF84A0B1BCF2E28; __utmt_UA-33292184-1=1; fbm_395614663835338=base_domain=.foody.vn; fd.verify.password.17487925=27/09/2019; __utmb=257500956.8.10.1569595413; fbsr_395614663835338=R6GLsOcLn0_-u8GV6glwBI5bzv8iNng-yQsRwhfrAYA.eyJ1c2VyX2lkIjoiMTI2MDE4NjcyMTE1MzU3IiwiY29kZSI6IkFRRHY3VThCNWxlN3Q1b2hRRUw4X3hlbkJaNEx6eWdVSlBTYkZKT21nRVBFNXhESTJlRElNdlFpT09tUWVzQzFrQ0I5V0dEUktOSVkxR3lmQ0tLR0tWUFNzMDNtQnpOWUZuWXktM2l2cm5rZklzWENRNEZrR2NqSHp4WDJIODNKc3ZxbVNnQzZkUXdFc1RnMmVBZUZTOThodjVEV2NER2FoajgyakhxU0dsN19RZlE1M3FHa2wzYXFsam9WSXVfcEFoaUd2VUVrN1ZxTzhGUHFXaTVTMl8tcWxIOEVFQW9VUk9TNFNfT3VwSzV4bG43SUF0UFZCaS1kQVN1N1lBbVhrUTBtYllxUGhPTHpzNGlDaG9wSmdLZmJCdlpuSGZyYk84ZEVncWwtZlhCTUZ3cHMtcU8tNGpSSHJJOFhMVmxTNGp0RVNXWlpxXzZxVXRKcnZ4RVA4V0puYVo5a19FVjc1ZjZJMDBxZEJtblBLYXhETGlLbnpYQmRsTHpFM19pRDkwRSIsIm9hdXRoX3Rva2VuIjoiRUFBRm56emVCZnNvQkFFSkJLVUhxT3NkRmJ1Rk83b1hXWkFuV3I0aXdlbE1jWTNIMWNFdnB2Y2JidlJnaXMxMjhaQVNPN3JVYlpCWkN3Zk13bXFVOFJDN25wV01Eak5INlFHVndTem5LUWhKNFZMdlpDeEl1b3cwMnE4NUxjZlhINjhtSllIM0J5ZlgyMkZla2ZQZjNaQnJVTFNVdjFwZzdZdEZUbWZJTGhRNlVURm1MY21OMDA5bHNNcmlFODlpY1ZtZ0tmcnJIM21Td1pEWkQiLCJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImlzc3VlZF9hdCI6MTU2OTU5NjIzN30"
            ),
        ));
        $json = curl_exec($curl);
        return json_decode($json);

        #=========================== /Get Json Response ============================
    }
    public static function renderUrl($url) {
        #============================ Tạo mới url ============================
        $parsed = parse_url($url); // phan tich chuoi url thanh 1 mang cac thanh phan co key va value
        $scheme   = isset($parsed['scheme']) ? $parsed['scheme'] . '://' : ''; // cac thanh phan cua 1 chuoi url => phần giao thức : http:
        $host     = isset($parsed['host']) ? $parsed['host'] : ''; // phần cổng kết nối
        $path     = isset($parsed['path']) ? $parsed['path'] : ''; // thành phàn đường dẫn trên url
        $query    = $parsed['query'];  // phân truy vấn thông thường bắt đầu bằng ?
        parse_str($query, $params);// phan tich chuoi thanh 1 mang có key va value
        unset($params['page']); // loại bỏ phần tử trong mảng
        $string  = http_build_query($params); // chuyển mảng thành chuỗi url tương ứng
        $new_url = $scheme.$host.$path.'?'.$string;
        return $new_url;
        #=========================== /Tạo mới url ============================
    }
    public static function saveOneRestaurant($url, $page)
    {
    	$getJson  = self::getJson($url);// dump($getJson);
//        dd($getJson);
    	$response = [];
        $ok = 0;
        if (!empty($getJson)) { $dataItems = $getJson->searchItems;
            foreach ($dataItems as $item)
            {
                $RestaurantPicture = null;
                # Quận/Huyện
                $district = [
                	'name' => $item->District,
                	'slug' => \Str::slug($item->District),
                	'provinces_slug' => $item->Location,
                ];
                $districtID = \App\Services\District::checkAndSave($district);
                # Phong cách ẩm thực
                $restaurant_cuisines = [];
                $cuisines = $item->Cuisines;
                foreach ($cuisines as $cuisine) {
                    $cuisineItem = [
                        'name' => $cuisine->Name,
                        'slug' => \Str::slug($cuisine->Name),
                    ];
                    $cuisineID = \App\Services\Cuisines::checkAndSave($cuisineItem);
                    $restaurant_cuisines[] = $cuisineID;
                }
                # loại nhà hàng
                $restaurant_categories = [];
                $categories = $item->Categories;
                foreach ($categories as $category) {
                    $categoryItem = [
                        'name' => $category->Name,
                        'slug' => \Str::slug(trim(str_replace('/', ' ', $category->Name))),
                    ];
                    $categoryID = \App\Services\Category::checkAndSave($categoryItem);
                    $restaurant_categories[] = $categoryID;
                }
                # Lưu ảnh đại diện
                if ($item->PicturePathLarge) {
                	$pictureUrl = $item->PicturePathLarge;
                	$contents 	= file_get_contents($pictureUrl);
                	$currentName = substr($pictureUrl, strrpos($pictureUrl, '/') + 1);
                	if (!file_exists(public_path('images/restaurants'))) {
	                    mkdir(public_path('images/restaurants'), 0777, true);
	                }
                	file_put_contents(public_path('images/restaurants/'.$currentName), $contents);
	                # đổi tên file image
	                $newName = \Str::slug($item->Name).'-'.$item->Id.substr($pictureUrl, strrpos($pictureUrl, '.') + 1);
	                rename(public_path('images/restaurants/'.$currentName), public_path('images/restaurants/'.$newName));
	                $RestaurantPicture = $newName;
                }
                # Thông tin nhà hàng
                $response[$page][] = $item->Name;
                $infoRestaurant = [
                	'name' => $item->Name,
                	'slug' => \Str::slug($item->Name),
                	'location' => $item->Location,
                	'district_id' => $districtID,
                	'address' => $item->Address,
                	'picture' => $RestaurantPicture,
                	'url_detail' => $item->DetailUrl,
                	'source' => config('modules.crawl.SITE.FOODY'),
                	'status' => config('modules.crawl.STATUS_ID.NEW_DATA'),
                	'created_at' => date('Y-m-d H:i:s'),
                ];
                $restaurantID = \App\Services\Restaurant::insertOne($infoRestaurant);
                if ($restaurantID) {
                	# ================== Liên kết bảng ================== #
                	self::insertRestaurantCuisine($restaurant_cuisines,$restaurantID);
                    self::insertRestaurantCategory($restaurant_categories,$restaurantID);
                    $ok = 1;
                } else {
                	unlink(public_path('images/restaurants/'.$RestaurantPicture));
                    $ok = 0;
                }
            }
        }
        if ($ok == 1) {
            \DB::table('md5_url')->updateOrInsert(
                ['md5'=>md5(self::renderUrl($url))],
                ['md5'=>md5(self::renderUrl($url)),'page'=>$page,'created_at'=>date('Y-m-d H:i:s'), 'cate' => $url]
            );
            return $response;
        } else {
            return false;
        }
    }
    public static function insertRestaurantCuisine($cuisineId,$restaurantId)
    {
        foreach ($cuisineId as $cs_Id) {
            $checkExist = \DB::table('restaurant_cuisine')
                        ->where('cuisine_id',$cs_Id)
                        ->where('restaurant_id',$restaurantId)
                        ->count();
            if ($checkExist < 1) {
                \DB::table('restaurant_cuisine')->insert([
                    'cuisine_id' => $cs_Id,
                    'restaurant_id' => $restaurantId
                ]);
            }
        }
    }
    public static function insertRestaurantCategory($categoryId,$restaurantId)
    {
        foreach ($categoryId as $cs_Id) {
            $checkExist = \DB::table('restaurant_category')
                        ->where('category_id',$cs_Id)
                        ->where('restaurant_id',$restaurantId)
                        ->count();
            if ($checkExist < 1) {
                \DB::table('restaurant_category')->insert([
                    'category_id' => $cs_Id,
                    'restaurant_id' => $restaurantId
                ]);
            }
        }
    }
}
