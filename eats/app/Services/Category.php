<?php

namespace App\Services;

class Category
{
	public static function checkAndSave($data)
	{
		$categoryId = \DB::table('categories')->where('slug',$data['slug'])->pluck('id')->first();
		if ($categoryId) {
			return $categoryId;
		} else {
			$data['created_at'] = date('Y-m-d H:i:s');
            $insertGetId = \DB::table('categories')->insertGetId($data);
            return $insertGetId;
		}
	}
}