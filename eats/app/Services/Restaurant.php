<?php

namespace App\Services;

class Restaurant
{
	public static function insertOne($data)
	{
		$restaurantId = \DB::table('restaurants')->where('slug',$data['slug'])->pluck('id')->first();
		if ($restaurantId) {
			return $restaurantId;
		} else {
			$data['created_at'] = date('Y-m-d H:i:s');
            $insertGetId = \DB::table('restaurants')->insertGetId($data);
            return $insertGetId;
		}
	}
}