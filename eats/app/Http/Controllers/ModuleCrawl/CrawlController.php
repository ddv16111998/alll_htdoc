<?php

namespace App\Http\Controllers\ModuleCrawl;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CrawlController extends Controller
{
    public function index() {
        return view('admin.crawl.index');
    }
    public function foody()
    {
    	return view('admin.crawl.foody');
    }
}
