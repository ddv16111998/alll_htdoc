<?php

namespace App\Http\Controllers;

use App\Models\Restaurant;
use App\Models\Province;
use App\Models\Category;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class DataController extends Controller
{
    public function index(Request $request)
    {
        $res['province'] = Province::all('slug','name')->toArray();
        $res['category'] = Category::all('id','name')->toArray();
        $res['status']   = config('crawl.status');
        $res['source']   = config('crawl.source');
        if ($request->ajax()) {
            if ($request->trash AND $request->trash==1) {
                $query = Restaurant::onlyTrashed(); // deer lay cai da xoa ròi
            }
            else {
                $query = Restaurant::whereNotNull('name'); /// lấy cai chưa xoa
            }
            if ($request->province) {
                $pr = trim(str_replace('/', ' ', $request->province));
                $query = $query->where('location', $pr);
            }
            if ($ct = $request->category) {
                $query = $query->whereHas('category', function ($query) use ($ct) {
                    $query->where('category_id', $ct);
                });
            }
            if ($sc = $request->source) {
                $query = $query->where('source',$sc);
            }
            if ($st = $request->status) {
                $query = $query->where('status',$st);
            }
            $data = $query;
            return DataTables::of($data)
                ->editColumn('location', function ($data) {
                    return '<b style="color:#00c292">'.$data->location.'</b>';
                })
                ->editColumn('category_id', function ($data) {
                    $category = $data->category->pluck('name')->toArray();
                    return '<b style="color:#ff3333">'.implode(', ', $category).'</b>';
                })
                ->editColumn('cuisine_id', function ($data) {
                    $cuisine = $data->cuisine->pluck('name')->toArray();
                    return '<b style="color:#ff9600">'.implode(', ', $cuisine).'</b>';
                })
                ->editColumn('source', function ($data) {
                    return '<label class="label label-info">'.config('crawl.get_source.'.$data->source).'</label>';
                })
                ->editColumn('picture', function ($data) {
                    return '<img src="/images/restaurants/'.$data->picture.'" style="width:100px">';
                })
                ->editColumn('status', function ($data) {
                    return '<b>'.config('crawl.status.'.$data->status).'</b>';
                })
                ->editColumn('created_at', function ($data) {
                    return date('H:i - d/m/Y',strtotime($data->created_at));
                })
                ->addColumn('action', function ($data) {
                    $str = '<a href="#" class="btn btn-xs btn-info">Chi tiết</a>&nbsp;';
                    if ($data->deleted_at) {
                        $str .= '<a href="' . route('admin.data.restore', $data->id) . '" class="btn btn-xs btn-info">Phục hồi</a>';
                    } else {
                        $str .= '<a href="javascript:;" data-href="' . route('admin.data.destroy', $data->id) . '" class="btn btn-xs btn-danger btn-delete">Delete</a>';
                    }
                    return $str;
                })
                ->rawColumns(['action','picture','location','status','url_detail','category_id','source','cuisine_id'])
                ->make(true);
        }
        return view('admin.data.index',$res);
    }
    function destroy($id) {
        $data = Source::find($id);
        $data->delete();
        if ($data->trashed()) {
            return redirect()->route('admin.data.index')->with(['type'=>'success','mesage'=>'Xóa thành công']);
        }
        else {
            return redirect()->route('admin.data.index')->with(['type'=>'error','mesage'=>'Xóa thất bại']);
        }
    }
    function restore($id) {
        $data = Source::withTrashed()->find($id);
        $data->restore();
        return redirect()->route('admin.data.index');
    }
}
