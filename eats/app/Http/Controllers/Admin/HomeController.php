<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
    	$data['COUNT_NEW_DATA'] = \App\Models\Restaurant::where('status',config('modules.crawl.STATUS_ID.NEW_DATA'))->count();
    	$data['COUNT_DATA_GET_INFO'] = \App\Models\Restaurant::where('status','<>',config('modules.crawl.STATUS_ID.NEW_DATA'))->count();
    	return view('admin.dashboard.index',$data);
    }
}
