<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Repository\CateRepository;
use Yajra\Datatables\Datatables;
use Exception;

class CateController extends Controller
{
    protected $repository;
    protected $stt = 1;
    public function __construct(CateRepository $cate)
    {
        $this->repository = $cate;
    }
    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = $this->repository->getDataIndex();
            return Datatables::of($data)
                ->editColumn('slug',function ($data){
                    return '<a href="'.url('/admin/caterogy').'/'.$data->slug.'">'.url('/admin/caterogy').'/'.$data->slug.'</a>';
                })
                ->editColumn('created_at',function ($data){
                        $created_at = explode('-','2019-09-02');
                        return $created_at[2].'/'.$created_at[1].'/'.$created_at[0];

                })
                ->addColumn('action', function ($data) {
                    return '<a href="'.route('admin.cate.edit',$data->id).'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> Sửa</a>
                        <a href="javascript:;" data-href="'.route('admin.cate.destroy',$data->id).'" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i> Xóa</a>';
                })
                ->rawColumns(['action','created_at','slug'])
                ->toJson();

        }
        $count = $this->repository->countDelete();
        return view('admin.cate.index',['count'=>$count]);
    }
    public function create()
    {
        $data = $this->repository->getDataIndex();
        return view('admin.cate.create',['data'=>$data]);
    }
    public function save(Request $request,$id)
    {
        $name = $request->title;
        $slug = \Str::slug($name);
        $parent_id = $request->parent_id;
        $data = [
            'name'=>$name,
            'slug'=>$slug,
            'parent_id'=>$parent_id
        ];
        $param=[];

        return $this->repository->updateOrCreate($data,$param,$id);
    }
    public function store(Request $request,$id=null)
    {
        $data = $this->save($request,$id);
        if($data)
        {
            return redirect()->route('admin.cate.index')
                ->with(['message' => 'Thêm mới thành công', 'type' => 'success']);
        }
        else {
            return redirect()->route('admin.cate.index')
                ->with(['message' => 'Thêm mới thất bại', 'type' => 'error']);
        }
    }
    public function edit($id)
    {
        $cate = $this->repository->getDataEdit($id);
        $cates = $this->repository->getDataIndex();
        $data['cate']=$cate;
        $data['data']=$cates;
        return view('admin.cate.edit',$data);
    }
    public function update(Request $request,$id)
    {
        $data = $this->save($request,$id);
        if($data)
        {
            return redirect()->route('admin.cate.index')
                ->with(['message' => 'Cập nhật thành công', 'type' => 'success']);
        }
        else {
            return redirect()->route('admin.cate.index')
                ->with(['message' => 'Cập nhật thất bại', 'type' => 'error']);
        }
    }
    public function destroy($id)
    {
        $data = $this->repository->destroy($id);
        if($data)
        {
            return redirect()->route('admin.cate.index')
                ->with(['message' => 'Xóa thành công', 'type' => 'success']);
        }
        else {
            return redirect()->route('admin.cate.index')
                ->with(['message' => 'Xóa thất bại', 'type' => 'error']);
        }

    }
    public function trashCan(Request $request)
    {
        if($request->ajax())
        {
            $data = $this->repository->trash_can();
            return Datatables::of($data)
                ->editColumn('slug',function ($data){
                    return '<a href="'.url('/admin/caterogy').'/'.$data->slug.'">'.url('/admin/caterogy').'/'.$data->slug.'</a>';
                })
                ->editColumn('created_at',function ($data){
                    $created_at = explode('-','2019-09-02');
                    return $created_at[2].'/'.$created_at[1].'/'.$created_at[0];

                })
                ->addColumn('action', function ($data) {
                    return '<a href="'.route('admin.cate.restore',$data->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Phục hồi</a>';
                })
                ->rawColumns(['action','created_at','slug'])
                ->toJson();
        }
        return view('admin.cate.trash');
    }
    public function restore($id)
    {
        $data = $this->repository->restore($id);
        if($data)
        {
            return redirect()->route('admin.cate.index')
                ->with(['message' => 'Phục hồi thành công', 'type' => 'success']);
        }
        else {
            return redirect()->route('admin.cate.index')
                ->with(['message' => 'Phục hồi thất bại', 'type' => 'error']);
        }
    }


}
