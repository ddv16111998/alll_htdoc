<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repository\AreaRepository;
use Yajra\Datatables\Datatables;

class AreaController extends Controller
{
    protected $repository;
    protected $stt = 1;
    public function __construct(AreaRepository $area)
    {
        $this->repository = $area;
    }

    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = $this->repository->getDataIndex();
            return Datatables::of($data)
                ->editColumn('slug',function ($data){
                    return '<a href="'.url('/admin/area').'/'.$data->slug.'">'.url('/admin/area').'/'.$data->slug.'</a>';
                })
                ->editColumn('created_at',function ($data){
                    $created_at = explode('-','2019-09-02');
                    return $created_at[2].'/'.$created_at[1].'/'.$created_at[0];

                })
                ->addColumn('action', function ($data) {
                    return '<a href="'.route('admin.area.edit',$data['id']).'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> Sửa</a>
                        <a href="javascript:;" data-href="'.route('admin.area.destroy',$data->id).'" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i> Xóa</a>';
                })
                ->rawColumns(['action','created_at','slug'])
                ->toJson();
        }
        $count = $this->repository->countDelete();
        return view('admin.area.index',['count'=>$count]);
    }

    public function save(Request $request,$id)
    {
        $area = $request->area;
        $slug = \Str::slug($area);
        $data = [
            'name' => $area,
            'slug' => $slug
        ];
        $param = [];
        return $this->repository->updateOrCreate($data,$param,$id);
    }

    public function create()
    {
        return view('admin.area.create');
    }

    public function store(Request $request,$id=null)
    {
        $data = $this->save($request,$id);
        if($data)
        {
            return redirect()->route('admin.area.index')
                ->with(['message' => 'Thêm mới thành công', 'type' => 'success']);
        }
        else {
            return redirect()->route('admin.area.index')
                ->with(['message' => 'Thêm mới thất bại', 'type' => 'error']);
        }
    }

    public function edit($id)
    {
        $area = $this->repository->getDataEdit($id);
        $data['area'] = $area;
        return view('admin.area.edit',$data);
    }

    public function update(Request $request,$id)
    {
        $data = $this->save($request,$id);
        if($data)
        {
            return redirect()->route('admin.area.index')
                ->with(['message' => 'Cập nhật thành công', 'type' => 'success']);
        }
        else {
            return redirect()->route('admin.area.index')
                ->with(['message' => 'Cập nhật thất bại', 'type' => 'error']);
        }
    }
    public function destroy($id)
    {
       $data = $this->repository->destroy($id);
        if($data)
        {
            return redirect()->route('admin.area.index')
                ->with(['message' => 'Xóa thành công', 'type' => 'success']);
        }
        else {
            return redirect()->route('admin.area.index')
                ->with(['message' => 'Xóa thất bại', 'type' => 'error']);
        }
    }
    public function trashCan(Request $request)
    {
        if($request->ajax())
        {
            $data = $this->repository->trash_can();
            return Datatables::of($data)
                ->editColumn('slug',function ($data){
                    return '<a href="'.url('/admin/caterogy').'/'.$data->slug.'">'.url('/admin/caterogy').'/'.$data->slug.'</a>';
                })
                ->editColumn('created_at',function ($data){
                    $created_at = explode('-','2019-09-02');
                    return $created_at[2].'/'.$created_at[1].'/'.$created_at[0];

                })
                ->addColumn('action', function ($data) {
                    return '<a href="'.route('admin.area.restore',$data->id).'" class="btn btn-xs btn-primary"><i class="fa fa-edit"></i> Phục hồi</a>';
                })
                ->rawColumns(['action','created_at','slug'])
                ->toJson();
        }
       return view('admin.area.trash');
    }
    public function restore($id)
    {
        $data = $this->repository->restore($id);
        if($data)
        {
            return redirect()->route('admin.area.index')
                ->with(['message' => 'Khôi phục thành công', 'type' => 'success']);
        }
        else {
            return redirect()->route('admin.area.index')
                ->with(['message' => 'Khôi phục thất bại', 'type' => 'error']);
        }
    }
}
