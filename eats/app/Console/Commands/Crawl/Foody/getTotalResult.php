<?php

namespace App\Console\Commands\Crawl\Foody;

use Illuminate\Console\Command;
use App\Services\CrawlFoody;

class getTotalResult extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:foody-get-total-result {InputCate} {InputProvince}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get information total restaurant results';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $InputCate = $this->input->getArgument('InputCate');
        $InputProvince = $this->input->getArgument('InputProvince');
        $urlString = trim(CrawlFoody::createUrl($InputCate,$InputProvince));
        $responseJson = CrawlFoody::getJson($urlString);
        $total_result = $responseJson->totalResult;
        $total_page   = ceil($responseJson->totalResult/12);
        $this->info($total_result."-".$total_page);
    }
}
