<?php

namespace App\Console\Commands\Crawl\Foody;

use Illuminate\Console\Command;
use App\Services\CrawlFoody;

class getRestaurant extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:foody-get-info-restaurant {url}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get basic information of a restaurant';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $url  = $this->input->getArgument('url');
        $urlNoPage   = CrawlFoody::renderUrl($url);
        $currentPage = \DB::table('md5_url')->where('md5',md5($urlNoPage))->pluck('page')->first();
        /** trang bắt đầu **/
        if($currentPage) $page = $currentPage+1;
        else $page = 1;
        $page = (int)$page;

        $newUrl = $urlNoPage.'&page='.$page;
        $res = CrawlFoody::saveOneRestaurant($newUrl, $page);
        if($res) {
            $arrNew = $res[$page];
            $arrNew['page'] = $page;
            dump($arrNew);
        } 
        else $this->info(1);
    }
}
