<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Cate extends Model
{
    use SoftDeletes;
    protected $table="categories";
    protected $dates = ['deleted_at'];
    protected $guarded=[];
}
