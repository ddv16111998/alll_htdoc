<?php

namespace App\Repository;

use AnhAiT\LaravelRepository\Repository;
use Illuminate\Database\Eloquent\Model;

class ProvinceRepository extends Repository
{
    public function model()
    {
        return Model::class;
    }
}
