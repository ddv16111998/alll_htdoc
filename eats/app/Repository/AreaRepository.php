<?php
namespace App\Repository;
use App\Models\Area;

class AreaRepository
{
    public function getDataIndex(){
        $data = Area::whereNull('deleted_at')->get();
        return $data;
    }
    public function getDataEdit($id)
    {
        $data = Area::find($id);
        return $data;
    }
    public function updateOrCreate($data,$param,$id)
    {
        $data = Area::updateOrCreate(['id'=>$id],$data);
        if($data)
        {
            return true;
        }
        return false;
    }
    public function destroy($id)
    {
        $data = Area::find($id);
        $data->delete();
        if($data->trashed())
        {
            return true;
        }
        return false;
    }
    public function trash_can()
    {
        $data = Area::onlyTrashed()->get();
        return $data;
    }
    public function restore($id)
    {
        $data = Area::onlyTrashed()->updateOrCreate(['id'=>$id],['deleted_at'=>null]);
        if($data)
        {
            return true;
        }
        return false;
    }
    public function countDelete()
    {
        $data = Area::onlyTrashed()->count();
        return $data;
    }
}
?>
