<?php

namespace App\Repository;
use App\Models\Cate;

class CateRepository
{
    public function getDataIndex()
    {
        $data = Cate::whereNull('deleted_at')->get();
        return $data;
    }
    public function updateOrCreate($data,$parm,$id)
    {
        $data = Cate::updateOrCreate(['id'=>$id],$data);
        if($data)
        {
            return true;
        }
        return false;
    }
    public function getDataEdit($id)
    {
        $data = Cate::find($id);
        return $data;
    }
    public function destroy($id)
    {
        $data = Cate::find($id);
        $data->delete();
        if($data->trashed())
        {
            return true;
        }
        return false;
    }
    public function trash_can()
    {
        $data = Cate::onlyTrashed()->get();
        return $data;
    }
    public function restore($id)
    {
        $data = Cate::onlyTrashed()->updateOrCreate(['id'=>$id],['deleted_at'=>null]);
        if($data)
        {
            return true;
        }
        return false;
    }
    public function countDelete()
    {
        $data = Cate::onlyTrashed()->count();
        return $data;
    }
}
