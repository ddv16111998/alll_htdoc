<?php

return [
    'SITE' => [
        'FOODY' => 1,
        'PASGO' => 2,
    ],
    'SITE_ID' => [
        1 => 'FOODY',
        2 => 'PASGO',
    ],
    'STATUS_TITLE' => [
        1 => 'Dữ liệu mới',
        2 => 'Thiếu thông tin',
        3 => 'Đã lấy được thông tin',
    ],
    'STATUS_ID' => [
        'NEW_DATA' => 1, // Dữ liệu mới
        'LACK_OF_INFORMATION' => 2, // Thiếu thông tin
        'FULL_INFORMATION' => 3, // Đầy đủ thông tin
    ],
];
