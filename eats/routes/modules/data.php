<?php

Route::get('data/list', [
	'as' => 'admin.data.index', 'uses' => 'DataController@index'
]);
Route::get('data/destroy/{id}', [
	'as' => 'admin.data.destroy', 'uses' => 'DataController@destroy'
]);
Route::get('data/restore/{id}', [
	'as' => 'admin.data.restore','uses' => 'DataController@restore'
]);
