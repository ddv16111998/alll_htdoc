<?php
    Route::group(['prefix'=>'caterogy'],function (){
        Route::get('/','Admin\CateController@index')->name('admin.cate.index');
        Route::get('/create','Admin\CateController@create')->name('admin.cate.create');
        Route::post('/store','Admin\CateController@store')->name('admin.cate.store');
        Route::get('/edit/{id}','Admin\CateController@edit')->name('admin.cate.edit');
        Route::post('/update/{id}','Admin\CateController@update')->name('admin.cate.update');
        Route::get('/destroy/{id}','Admin\CateController@destroy')->name('admin.cate.destroy');
        Route::get('/trash_can','Admin\CateController@trashCan')->name('admin.cate.trash_can');
        Route::get('/restore/{id}','Admin\CateController@restore')->name('admin.cate.restore');
    })
?>
