<?php
    Route::group(['prefix'=>'area'],function (){
        Route::get('/','Admin\AreaController@index')->name('admin.area.index');
        Route::get('/create','Admin\AreaController@create')->name('admin.area.create');
        Route::post('/store','Admin\AreaController@store')->name('admin.area.store');
        Route::get('/edit/{id}','Admin\AreaController@edit')->name('admin.area.edit');
        Route::post('/update/{id}','Admin\AreaController@update')->name('admin.area.update');
        Route::get('/destroy/{id}','Admin\AreaController@destroy')->name('admin.area.destroy');
        Route::get('/trash-can','Admin\AreaController@trashCan')->name('admin.area.trash_can');
        Route::get('/restore/{id}','Admin\AreaController@restore')->name('admin.area.restore');
    })
?>
