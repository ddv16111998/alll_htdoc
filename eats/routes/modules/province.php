<?php
Route::group(['prefix' => 'province','namespace'=>'Admin'], function () {
        Route::get('/','ProvinceController@index')->name('admin.province.index');
        Route::get('create','ProvinceController@create')->name('admin.province.create');
    });
?>
