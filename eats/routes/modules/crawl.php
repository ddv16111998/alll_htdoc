<?php

Route::group(['prefix' => 'crawl', 'namespace' => 'ModuleCrawl'], function(){
    Route::get('/', 'CrawlController@index')->name('admin.crawl.index');
    Route::get('/foody', 'CrawlController@foody')->name('admin.crawl.foody');

    Route::post('/foody/get-total-result', 'CrawlFoodyController@getTotalResult')->name('admin.crawl.foody.get-total-result');
    
    Route::post('/foody/action', 'CrawlFoodyController@action')->name('admin.crawl.foody.action');
});
