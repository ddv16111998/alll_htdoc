<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('index','CrawlController@index')->name('admin.crawl.index');
Route::get('foody','CrawlController@foody')->name('admin.crawl.foody');
Route::post('getDataCurl','CrawlController@getDataCurl')->name('admin.crawl.getDataCurl');
