<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service\CrawlFoody;

class CrawlController extends Controller
{
    public function index()
    {
    	return view('crawl.index');
    }
    public function foody()
    {
    	return view('crawl.foody');
    }
    public function getDataCurl(Request $request)
    {
    	$url = trim($request->link);
    	$responseJson = CrawlFoody::getJson($url);
    	return json_encode(['responseJson'=>$responseJson]);
    }
}
