<?php
namespace App\Service;

class CrawlFoody
{
	public static function getJson()
	{
		#============================ Get Json Response ============================
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Accept: application/json, text/javascript, */*; q=0.01",
                "Accept-Encoding: gzip, deflate",
                "Cache-Control: no-cache",
                "Connection: keep-alive",
                "Cookie: flg=vn; floc=218; gcat=food; _ga=GA1.2.2049825386.1567244279; __utma=257500956.2049825386.1567244279.1567397057.1567422222.6; __utmz=257500956.1567253991.3.2.utmcsr=id.foody.vn|utmccn=(referral)|utmcmd=referral|utmcct=/dang-nhap-thanh-cong; _fbp=fb.1.1567244282205.489063649; fd.keys=; xfci=8UVACUW5XU6VB8I; FOODY.AUTH.UDID=d0903667-f27d-4300-824b-a54c31b29cba; FOODY.AUTH=41AC348022609A47A7F14414A417CEF94139CBA003F9DA9FCAFA5EE8A9F48F6B0A1482B733CE13429C157BC05A3E2D4A836E3B6EE32F2429C96BECF5683115230264D89DC9FE879D7B8A79AD72D94347FEA862A454236C6AE9305FC4D4A3E757077F6073981A17518D68DA436B08CAC6BDC3271F50C47419EB61EFF661D1E418FB23420C708418509505BE727B1CFE785922F2BBB3AD87AB35E5418B8808A81CF5C5DF330DDD0BD6FD7357FEF2AC6371E88279E620253445622269C73D69535560A47842ED5D7EEE92E2952EDB519223BFC3545E6770D07A5F282A1280718ED997CEC76E1CF0E7E38F618E071EA109BF; fd.res.view.218=49221; _gid=GA1.2.473186061.1567397057; __ondemand_sessionid=niasknhvycmst4mxjvwirehx; __utmb=257500956.4.10.1567422222; __utmc=257500956; __utmt_UA-33292184-1=1; _gat=1; _gat_ads=1",
                "Host: www.foody.vn",
                "Sec-Fetch-Mode: cors",
                "X-Requested-With: XMLHttpRequest",
                "X-Foody-User-Token: OrfFYu0poqorTlJ0phwds12lR4BmIyFwwcxMcUsnsJBsq4bqi71oBpVkCfHS",
                "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36",
                "Sec-Fetch-Site: same-origin"
            ),
        ));
        $json = curl_exec($curl);
        return json_decode($json);

        #=========================== /Get Json Response ============================
	}
}

 ?>