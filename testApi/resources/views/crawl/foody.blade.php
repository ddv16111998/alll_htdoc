<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
      <script type="text/javascript">
        $.ajaxSetup({headers:{'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
         function Filter () {
            var InputCate = $('.CateId').val();
            var InputProvince = $('.ProvinceId').val();
            var ArrCate = InputCate.split('=');
            var CateId = ArrCate[0];
            var CateSlug = ArrCate[1];
            var ArrProvince = InputProvince.split('=');
            var ProvinceId = ArrProvince[0];
            var ProvinceSlug = ArrProvince[1];
            UrlString='https://www.foody.vn/';
            if(InputProvince !=0)
            {
                //code
                UrlString +=ProvinceSlug;
                if(InputCate !=0)
                {
                    UrlString += '/'+CateSlug+'?ds=Restaurant&vt=row&st=1&c='+CateId+'&provinceId='+ProvinceId+'&categoryId='+CateId+'&append=true&page=1';
                    // alert(UrlString);
                }
                else {
                    UrlString += '/'+CateSlug+'?ds=Restaurant&vt=row&st=1&c='+'&provinceId='+ProvinceId+'&append=true&page=1';
                     // alert(UrlString);
                }
                var formData = new FormData();
                formData.append('link',UrlString);
                $.ajax({
                    type:"POST",
                    url:"{{ route('admin.crawl.getDataCurl') }}",
                    data:formData,
                    dataType:'json',
                    contentType:false,
                    processData:false,
                    success:function(data) {
                       console.log(data.responseJson);   
                    }
                })
            }
            else{
                $('.url').val('');
            }
         }
     </script>
</head>
<body>
       
     <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="over-loading"></div>
                            <div class="card-body">
                                <div class="option">
                                    <select onchange="Filter()" class="CateId form-control col-1">
                                        <option value="0" selected="selected">- Danh mục -</option>
                                        <option value="12={{ config('modules.foody.category.12') }}">Sang trọng</option>
                                        <option value="39={{config('modules.foody.category.39')}}">Buffet</option>
                                        <option value="1={{config('modules.foody.category.1')}}">Nhà hàng</option>
                                        <option value="11={{config('modules.foody.category.11')}}">Ăn vặt/vỉa hè</option>
                                        <option value="56={{config('modules.foody.category.56')}}">Ăn chay</option>
                                        <option value="2={{config('modules.foody.category.2')}}">Café/Dessert</option>
                                        <option value="3={{config('modules.foody.category.3')}}">Quán ăn</option>
                                        <option value="4={{config('modules.foody.category.4')}}">Bar/Pub</option>
                                        <option value="54={{config('modules.foody.category.54')}}">Quán nhậu</option>
                                        <option value="43={{config('modules.foody.category.43')}}">Beer club</option>
                                        <option value="6={{config('modules.foody.category.6')}}">Tiệm bánh</option>
                                        <option value="44={{config('modules.foody.category.44')}}">Tiệc tận nơi</option>
                                        <option value="27={{config('modules.foody.category.27')}}">Shop Online</option>
                                        <option value="28={{config('modules.foody.category.28')}}">Giao cơm văn phòng</option>
                                        <option value="79={{config('modules.foody.category.79')}}">Khu Ẩm Thực</option>
                                    </select>
                                    <select onchange="Filter()" class="ProvinceId form-control col-1">
                                        <option value="0">- Khu vực -</option>
                                        @include('crawl.include-province-foody')
                                    </select>
                                    <div class="form-control col-1" style="padding:10px">
                                        Có <b class="total_page" style="color:#f33">0</b> trang - <b class="total-result" style="color:#f33">0</b> kết quả.
                                    </div>
                                </div>
                                <hr>
                                <div class="form" style="margin-top:20px">
                                    <input class="page_start form-control col-1" value="1">
                                    <input class="page_end form-control col-1" placeholder="Nhập page ket thuc">
                                    <input class="url form-control col-7" placeholder="Nhập url foody">
                                    <button class="btn btn-sm btn-quet btn-primary">Quét &nbsp;<i class="iconload fa fa-spinner fa-pulse" style="display:none"></i></button>
                                </div>
                                <div class="results" style="min-height:200px;border:1px solid #eee;margin-top:20px;">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>
</html>