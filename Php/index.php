<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<script src="jquery-3.4.1.min.js"></script>
</head>
<body onload="pageload()">
	<div class="container">
		
		<p></p>
		<button class="btn btn-primary" data-toggle="modal" data-target="#addData" onclick="return confirm('Bạn có muốn thêm không?') "> <i class="fa fa-plus"></i> Insert Data </button>
		<button class="btn btn-info" onclick="AutoRefresh()"> <i class="fa fa-refresh"></i> Refesh</button>
		<button class="btn btn-secondary" onclick="logout()">Đăng xuất</button>
		<div class="modal fade" id="addData" tabindex="-1" role="dialog" aria-labelledby="addLabel" aria-hidden="true">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="addLabel">Insert Data</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
				
					<div id="error" class="text-center" style="color: red">
					  
					</div>
				
		        <form>
				  <div class="form-group">
				    <label for="nm">FullName</label>
				    <input type="text" class="form-control" id="nm" aria-describedby="" placeholder="Enter FullName">
		
				  </div>
				  <div class="form-group">
				    <label for="em">Email</label>
				    <input type="email" class="form-control" id="em" placeholder="Enter Email">
				  </div>
				 <div class="form-group">
				    <label for="hp">Phone</label>
				    <input type="number" class="form-control" id="hp" placeholder="Enter Phone">
				  </div>
				  <div class="form-group">
				    <label for="al">Address</label>
				    <input type="text" class="form-control" id="al" placeholder="Enter Address">
				  </div>
				</form>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        <button type="submit" onclick="saveData()" class="btn btn-primary">Save</button>
		      </div>
		    </div>
		  </div>
		</div>
		<p></p>
		
		<table class="table table-bordered table-striped" id="list-view">
			<label for=""><b>Show </b></label>
			<select name="show-col" id="show-col" style="width: 60px;margin: 0 10px 0 5px" onchange="ChangeShowData(this.value)">
				<option value="">---</option>	
				<option value="10">10</option>	
				<option value="9">9</option>	
				<option value="8">8</option>	
				<option value="7">7</option>	
				<option value="6">6</option>	
				<option value="5">5</option>	
				<option value="4">4</option>	
				<option value="3">3</option>	
				<option value="2">2</option>	
				<option value="1">1</option>	
			</select>
			<button type="button" class="btn btn-danger" onclick="downData()"><i class="fa fa-arrow-down"></i></button>
			<button type="button" class="btn btn-info" onclick="upData()"><i class="fa fa-arrow-up"></i></button>
			<thead>
				<tr>
					<th>ID</th>
					<th>Name</th>
					<th>Email</th>
					<th>Phone</th>
					<th>Address</th>
					<th>Change</th>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>
	<script>
		
		function logout() {
			window.location.href='form.php?logout';

		}
		function upData() {
			var number=$('#show-col').val();
			$.ajax({
				type:"POST",
				url:"handle.php?p=upData",
				data:"number="+number,
				success:function(data) {
					$('tbody').html(data);
				}
			});
		}
		function downData() {
			var number=$('#show-col').val();


			$.ajax({
				type:"POST",
				url:"handle.php?p=downData",
				data:"number="+number,
				success:function(data) {
					$('tbody').html(data);
				}
			});

			
		}
		function ChangeShowData(number)
		{
			pageload(number);
		}
		function saveData() {
			
			var name = $('#nm').val();
			var email = $('#em').val();
			var phone = $('#hp').val();
			var address = $('#al').val();

			if(name==''||email==''||phone==''||address=='')
			{
				$('#error').html('Thông tin không được để trống');
				$('#nm').css('border' ,'1px solid red');
				$('#em').css('border' ,'1px solid red');
				$('#hp').css('border' ,'1px solid red');
				$('#al').css('border' ,'1px solid red');
			}
			else
			{
				$.ajax({
					type:"POST",
					url:"http://localhost/php/handle.php?p=add",
					data:"nm="+name+"&em="+email+"&hp="+phone+"&al="+address,
					success: function(data)
					{
						alert("Insert Data Success");
						pageload();	
					}
				});
			}
		}

		function AutoRefresh() {
               setTimeout("location.reload(true);");
            }

		function pageload(number='null',sort='null') {
			$.ajax({
				type :"GET",
				url:"handle.php",
				data:"number="+number,
				success: function(data)
				{
					$('tbody').html(data);			
				}
			});

		}

		function del(id){
			var id = id;
			var result = confirm('Bạn có muốn xóa không?');
			if (result) {
				$.ajax({
					type :"GET",
					url:"http://localhost/php/handle.php?p=del",
					data : "id="+id,
					success:function(data) {
						alert("Delete success");
						pageload();
					}
				});
			}
		}
		function edit(id) {
			var id = id;
			var name = $('#nm-'+id).val();
			var email = $('#em-'+id).val();
			var phone = $('#hp-'+id).val();
			var address = $('#al-'+id).val();
			$.ajax({
				type:"GET",
				url:"http://localhost/php/handle.php?p=edit",
				data:"nm="+name+"&em="+email+"&hp="+phone+"&al="+address+"&id="+id,
				success:function(data) {
					alert("Edit Success");
					pageload();
					
				}
			});
		}
	</script>
</body>
</html>