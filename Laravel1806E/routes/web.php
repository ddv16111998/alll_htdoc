<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// ********************************************** Front-end
// ***********************************
route::get('demoConstant','DemoConstantController@index');
route::group([
	'prefix' => 'frontend',
	'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ],
	'namespace'=>'Frontend',// thu muc bao ve /chua
	'as'=>'frontend.'
],function(){
	route::get('/index','ProfileController@index')->name('profile');
	route::get('/resume','ResumeController@index')->name('resume');
	route::get('/work','WorkController@index')->name('work');
	route::get('/skill','SkillController@index')->name('skill');
	
	route::get('/lang','ProfileController@lang')->name('lang');

});

route::get('/switch-lang/{lang}',function($lang){
	// set langugua cho toan bo ung dung
	App::setLocale($lang);
	//luu vao session
	Session::put('locale',$lang);
	// su dung thu vien de xet lai ngon ngu cho ung dung
	LaravelLocalization::setLocale($lang);

	// quay ve duoc dung trang ma ng dung dang o do ma chuyen ngon ngu
	$url=LaravelLocalization::getLocalizedURL($lang,\URL::previous());
	return Redirect::to($url);
})->name('switchLang');
Route::get('/', function () {
    return view('welcome');
})->name('trang_chu');
// *******************************************************************
route::get('viet-nam',function(){
	return 'vo địch aff';
});

route::post('aff-2018',function(){
	return 'viet-nam';
});

route::put('malaysia',function(){
	return 'A quan';
});

route::delete('thai-lan',function(){
	return 'thua cuoc';
});

//1 request vua nhan method get or post
route::match(['post','get'],'sea-game',function(){
	return 'get or post';
});


//1 request nhan tat ca cac methoa
route::any('demo-any',function(){
	return 'demo-any';
});

//route dieu huong

route::get('bongda',function(){
	return redirect('viet-nam');
});

route::get('laptop',function(){
	return 'dell 3568 i7';
})->name('dell');

route::get('may-tinh',function(){
	return redirect()->route('dell');
});

//route chua tham so

route::get('dell/{name}/{price}/{id}',function($n,$p,$id){
	return "Name:{$n}+Price:{$p}+Id:{$id}";
})->where('id','\d+')->name('laptop');

route::get('view-laptop',function(){
	return redirect()->route('laptop',['name'=>'asus','price'=>200000,'id'=>5]);
});

//route group
route::group(['prefix'=>'MyGroup'],function(){
	route::get('route1',function(){
		return 'route1';
	});
	route::get('route2',function(){
		return 'route2';
	});
});



// cac route lam viec voi middleware
route::get('/film/watch/{age}',function($age){
	return 'oke';
})->middleware('my.check.age:admin');
route::get('not-found',function(){
	return "do not watch";
})->name('notFound');

//tham so truyen vao la 1 ham goi la callback function
route::get('check-number/{number}',function(){
	return 'yes';
})->middleware('my.check.number');

route::get('hello/{age}/{name}/{id}','TestController@index');
route::get('test/{age}','TestController@checkAgeWatchFilm')->name('test-age');
route::get('test-request/{name}/{age}','TestController@testRequest')->name('test-request');
route::get('demo','DemoController@index')->name('demo');
route::get('myName/{name}','TestController@myName');
route::get('contact','DemoController@contact')->name('contact');
route::get('about','DemoController@about')->name('about');
route::get('home','DemoController@home')->name('home');


route::group([
	'prefix'=>'db-query'
],function(){
	route::get('select','QueryDbController@index')->name('select');
	route::get('orm','QueryDbController@orm')->name('orm');
});


// *************************************

// ADMIN
route::group([
	'prefix'=>'admin', // di cung tren url get
	'namespace'=>'Admin',// mang/ thu muc bao ve cho file controller
	'as'=>'admin.'  // di cung name cua route   {{ route('admin.handle-login') }}
],function(){
	route::get('login','LoginController@index')->name('login');
	route::post('handle-login','LoginController@handleLogin')->name('handle-login');
	route::get('logout','LoginController@logout')->name('logout');
});


// rosurce controller

route::group([
	'prefix'=>'service',
	'namespace'=>'API'
	],function(){
		route::resource ('/photos','ServiceController');

});
//end rosusre controller
//route profile/ home
route::group([
	'prefix'=>LaravelLocalization::setLocale().'/admin', // di cung tren url get
	'namespace'=>'Admin',
	'as'=>'admin.', //name // di cung name cua route   {{ route('admin.handle-login') }}
	'middleware' => ['check.admin.login','web','localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
],function(){
	route::get('dashboard','DashboardController@index')->name('dashboard');
	route::get('profile','ProfileController@index')->name('profile');
	route::get('add-profile','ProfileController@addView')->name('formAddProfile');
	route::post('handle-add-profile','ProfileController@handleAdd')->name('handleAddProfile');
	route::post('delete-profile','ProfileController@deleteProfile')->name('deleteProfile');
	route::get('edit-profile/{id}','ProfileController@editProfile')->name('editProfile');
	route::post('handle-edit-profile/{id}','ProfileController@handleEdit')->name('handleEdit');
	route::post('searchData','ProfileController@searchData')->name('search');


	route::get('indexWork','WorkController@viewWork')->name('indexWork');
	route::get('formAddWork','WorkController@formAddWork')->name('formAddWork');
	route::post('workDataHanle','WorkController@workDataHanle')->name('workDataHanle');


	route::get('indexSkill','SkillController@indexSkill')->name('indexSkill');
	route::get('formAddSkill','SkillController@formAddSkill')->name('formAddSkill');
	route::post('handleAddSkill','SkillController@handleAddSkill')->name('handleAddSkill');
	route::get('deleteData/{id}','SkillController@deleteData')->name('deleteData');
	route::get('viewEditData/{id}','SkillController@viewEditData')->name('viewEditData');
	route::post('searchData','SkillController@searchData')->name('searchData');
	route::post('updateData/{id}','SkillController@updateData')->name('updateData');

	route::get('/products','ProductsController@index')->name('products');
	route::get('/addCart/{id}','CartController@add')->name('addCart');

	route::get('/cart','CartController@index')->name('listCart');
	route::get('update-cart','CartController@update')->name('updateCart');
	route::get('delelte-cart/{id}','CartController@delete')->name('deleteCart');
	route::get('remove-cart','CartController@remove')->name('deleteAll');
});
route::get('/switch-lang/{lang}',function($lang){
	// set langugua cho toan bo ung dung
	App::setLocale($lang);
	//luu vao session
	Session::put('locale',$lang);
	// su dung thu vien de xet lai ngon ngu cho ung dung
	LaravelLocalization::setLocale($lang);

	// quay ve duoc dung trang ma ng dung dang o do ma chuyen ngon ngu
	$url=LaravelLocalization::getLocalizedURL($lang,\URL::previous());
	return Redirect::to($url);
})->name('switchLangAdmin');

// // route works
// route::group([
// 	'prefix'=>'admin', // di cung tren url get
// 	'namespace'=>'Admin',
// 	'as'=>'admin.', //name // di cung name cua route   {{ route('admin.handle-login') }}'middleware' => 
// ],function(){
// 	route::get('indexWork','WorkController@viewWork')->name('indexWork');
// 	route::get('formAddWork','WorkController@formAddWork')->name('formAddWork');
// 	route::post('workDataHanle','WorkController@workDataHanle')->name('workDataHanle');
// });


// //route skills
// route::group([
// 	'prefix'=>'admin',
// 	'namespace'=>'Admin',
// 	'as'=>'admin.',
// ],function(){
// 	route::get('indexSkill','SkillController@indexSkill')->name('indexSkill');
// 	route::get('formAddSkill','SkillController@formAddSkill')->name('formAddSkill');
// 	route::post('handleAddSkill','SkillController@handleAddSkill')->name('handleAddSkill');
// 	route::get('deleteData/{id}','SkillController@deleteData')->name('deleteData');
// 	route::get('viewEditData/{id}','SkillController@viewEditData')->name('viewEditData');
// 	route::post('searchData','SkillController@searchData')->name('searchData');
// 	route::post('updateData/{id}','SkillController@updateData')->name('updateData');
// });


