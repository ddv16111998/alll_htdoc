<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
class StoreWorkPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company'=>'required',
            'start_date'=>'required',
            'ranks'=>'required|',
            'status'=>'required',
            'description'=>'required'
        ];
    }
    public function messages()
    {
        return [
            'company.required'=>':attribute khong duoc trong',
            'start_date.required'=>':attribute khong duoc trong',
            'ranks.required'=>':attribute khong duoc trong',
            'status.required'=>':attribute khong duoc trong',
            'description.required'=>':attribute khong duoc trong',
        ];
    }
}
