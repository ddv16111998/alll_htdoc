<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLoginPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;

        // cho phép validate data =>true
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // tao ra cac luat rang buoc giu lieu

            // key:gia tri cua thuoc tinh name cua the input
            // value: la cac rules/ moi rules cach nhau boi dau |
            'user'=>'required|min:3|max:60',
            'pass'=>'required|min:3|max:60'
        ];
    }


    // tao ra 1 ham thong bao loi ra ngoai trinh duyet
    public function messages()
    {
        return [
            'user.required'=>'Xin moi nhap username',
            'user.min'=>':attribute khong duoc nho hon :min ki tu',
            // atribute la label cua input
            'user.max'=>':attribute khong duoc lon hon :max ki tu',
            'pass.min'=>':attribute khong duoc nho hon :min ki tu',
            'pass.max'=>':attribute khong duoc lon hon :max ki tu',
            'pass.required'=>'Xin moi nhap password'
        ];
    }
}
