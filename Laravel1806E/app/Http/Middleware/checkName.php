<?php

namespace App\Http\Middleware;

use Closure;

class checkName
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $name=$request->name;
        if($name=='ddv')
        {
            return redirect()->route('trang_chu');
        }
        return $next($request);
    }
}
