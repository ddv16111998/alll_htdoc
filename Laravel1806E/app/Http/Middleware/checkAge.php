<?php

namespace App\Http\Middleware;

use Closure;

class checkAge
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next,$param=null)
    {
        //$param duoc goi la tham so truyen vao middleware
        //kiem tra tham so tu request trong route gui len
        $age=$request->age;
        if($age<18 && $param!=='admin')
        {
            return redirect()->route('notFound');
        }
        //cho phep thuc thi cac request
        return $next($request);
    }
}
