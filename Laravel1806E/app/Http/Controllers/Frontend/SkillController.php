<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Profile;
class SkillController extends Controller
{
	private $status=1;
    public function index(Profile $dbProfile)
    {
    	$data=[];
    	$result=$dbProfile->getInfoProfileByActive($this->status);
    	$data['info']=$result;
    	return view('frontend.skill.index',$data);
    }
}
