<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Model\Profile;
class ResumeController extends BaseController
{
    public function index(Profile $dbProfile)
    {
    	$infoPfile=$this->getDataInfoHeader($dbProfile,1);
    	$data=[];
    	$data['info']=$infoPfile;
    	return view('frontend.resume.index',$data);
    	
    }
}
