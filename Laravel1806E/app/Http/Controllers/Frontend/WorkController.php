<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Profile;
class WorkController extends Controller
{
	private $status=1;

    public function index(Profile $dbProfile)
    {
    	$data=[];
    	$infoPfile=$dbProfile->getInfoProfileByActive($this->status);
    	$data['info']=$infoPfile;
    	return view('frontend.work.index',$data);

    }
}
