<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use App\Model\Profile;
class ProfileController extends BaseController
{
	
	private $statusProfile = 1;

    public function index(Profile $dbProfile,Request $request)
    {
    	$infoPfile=$this->getDataInfoHeader($dbProfile,$this->statusProfile);
    	return view('frontend.profile.index',['info'=>$infoPfile]);
    }
    public function lang()
    {
    	return view('frontend.profile.lang');
    }
}
