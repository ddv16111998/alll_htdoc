<?php

namespace App\Http\Controllers;

//su dung thu vien db ket noi hay truy van du lieu
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB; // su dung thu vien db trong laravel
use App\Model\Post;

class QueryDbController extends Controller
{
    private $dbPost;
    public function __construct(Post $post)
    {
        // Post $post = new Post
        $this->dbPost=$post;
    }
    public function orm()
    {
       $data=$this->dbPost->getAllData();
       $data2=$this->dbPost->getAllDataPost();

       $data3=Post::getDataById(1); //static khong ton tai $this . thi goi luon class model
       dd($data3->id);

       foreach ($data2 as $key=>$item) {
           echo $item->id;
           echo '<br>';
       }
    }
    public function getDataById($id)
    {
        $data=post::find($id);
        $data=($data) ? $data->toArray(): [];
        return $data;
    }


    public function index()
    {
    	// return 'test query database';
    	$admins=DB::table('admins')->get();
    	//chuyen object laravel ve mang
    	$admins=json_decode($admins,true);
    	// dd($admins);
    	foreach ($admins as $key=>$value) {
    		echo $value['id']; //vi n su dung kieu object
    		echo '<br>';
    	}

    	$name=DB::table('admins')->select('username')->where('id',10)->first();
    	// dd($name);

    	// get() = lay tat du lieu hay tra ve 1 mang da chieu
    	// firts()= lay 1 dong du lieu tra ve mang 1 chieu == limit 1

        $count=DB::table('admins')->count();
        $min=DB::table('admins')->min('id');
        $max=DB::table('admins')->max('id');
        $avg=DB::table('admins')->avg('id');
        // dd($count,$min,$max,$avg);

        $exits=DB::table('admins')->where('id',1)->doesntExist(); // lam voi form dang nhap
        

        $select=DB::table('admins')->select('id','username')->get();
        // dd($select);
        $raw=DB::table('admins')->select(DB::raw('count(*) as total,id'))->groupBy('id')->get(); //DB::raw: truyen cau lenh sql
        // dd($raw);   

        // where in hoac not in la dieu kien nam trong

        // $raw=DB::table('admins')->select('')

        // $raw2=DB::table('admins')->whereRaw('id NOT IN (1,2,3)')->get();
        // $join =DB::table('post')->select('post.*','post.created_at as created_at_post','comment.*')
        //                         ->join('comment','comment.id_post','=','post.id')->get();
        // $like=DB::table('admin')->select('*')->where('admins.email','like','%T%');->get();
        // $between=DB::table('admins')->whereBetween('id',[1,3])->get();

        // $insert=DB::table('comment')->insert([
        //     'questions'=>'thi is question',
        //     'id_post'=>1,
        //     'created_at'=>date('Y-m-d'),
        //     'updated_at'=null
        // ]);
        // if($insert)
        // {
        //     dd('ok');
        // }
        // else {
        //     dd('that bai');
        // }
    }
}
