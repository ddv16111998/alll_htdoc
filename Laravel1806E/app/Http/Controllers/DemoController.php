<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DemoController extends Controller
{
    public function index()
    {
    	//load 1 file view
    	//truyen du lieu tu controller ra view
    	//truyen nhieu du lieu cugn 1 luc

    	$data=[
    		'name'=>'abc',
    		'age' =>20,
    		'email'=>'ddv@gmail.com',
    		'phone'=>'0962737289'
    	];
        $money=12345679;
    	// return view('demo',[
    	// 	'info'=>$data
    	// ]); // truyen tham so cho view kieu nhieu bien hoac mang
        // return view('demo')->with('m',$money); // truyen tham so cho 1 1 bien

        $infoSt=[
            ['masv'=>123,
              'name'=>'abc',
              'age' =>20,
              'email'=>'ddv@gmail.com',
              'phone'=>'0962737289',
              'money'=>10
            ],
              
            [
                'masv'=>124,
              'name'=>'ddv',
              'age' =>12,
              'email'=>'ac@gmail.com',
              'phone'=>'09627372',
              'money'=>20
            ],
            [
                'masv'=>125,
              'name'=>'nva',
              'age' =>15,
              'email'=>'nva@gmail.com',
              'phone'=>'0123456789',
              'money'=>30
            ]
        ];

        // return view('demo',['info'=>$infoSt]);
        return view('demo.index');
    }
    public function contact()
    {
      return view('demo.contact');
    }
    public function about()
    {
      return view('demo.about');
    }
     public function home()
    {
      return view('demo.home');
    }
}
