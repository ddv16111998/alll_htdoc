<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSkillPost;
use App\Model\Skill;
class SkillController extends Controller
{
    public function indexSkill(Skill $skill,Request $request)
    {
    	$result=$skill->getAllData();
        
    	return view('admin.skill.index',['info'=>$result]);
    }
    public function formAddSkill()
    {
    	return view('admin.skill.formAddSkill');
    }
    public function handleAddSkill(StoreSkillPost $request,Skill $skill)
    {
    	$language=$request->language;
    	$level=$request->level;
    	$status=$request->status;
    	$data=[
    		'language'=>$language,
    		'level'=>$level,
    		'status'=>$status,
    		'created_at'=>date('Y-m-d H:i:s'),
    		'updated_at'=>null
    	];
    	$result=$skill->insertData($data);
    	if($result)
    	{
    		$result=$skill->getAllData();
    		$request->session()->flash('addData','Add Success');
    		return view('admin.skill.index',['info'=>$result]);
    	}
    	else {
    		$result=$skill->getAllData();
    		$request->session()->flash('addData','Fail Success');
    		return view('admin.skill.index',['info'=>$result]);
    	}
    }
    public function deleteData($id,Skill $skill,Request $request)
    {
    	$delete=$skill->deleteData($id);
    	if($delete)
    	{
    		$result=$skill->getAllData();
    		$request->session()->flash('addData','Delete Success');
    		return view('admin.skill.index',['info'=>$result]);
    	}
    	else {
    		$result=$skill->getAllData();
    		$request->session()->flash('addData','Delete Fail');
    		return view('admin.skill.index',['info'=>$result]);
    	}
    }
    public function viewEditData($id,Request $request,Skill $skill)
    {
    	$result=$skill->editData($id);
    	return view('admin.skill.viewEditData',['oldData'=>$result]);
    }
    public function searchData(Request $request,Skill $skill)
    {
    	$keyword=$request->search;
    	$data=$skill->searchData($keyword);
        return view('admin.skill.index',['info'=>$data]);
    }
    public function updateData($id,StoreSkillPost $request,Skill $skill)
    {
        $language=$request->language;
        $level=$request->level;
        $status=$request->status;
        $data=['language'=>$language,
                'level'=>$level,
                'status'=>$status,
                'created_at'=>date('Y-m-d H:i:s'),
                'updated_at'=>null
        ];
        $result=$skill->updateData($id,$data);
        if($result)
        {
            $dulieu=$skill->getAllData();
            $request->session()->flash('addData','Update Success');
            return view('admin.skill.index',['info'=>$dulieu]);
        }
        else
        {
             $dulieu=$skill->getAllData();
            $request->session()->flash('addData','Not Found Id');
            return view('admin.skill.index',['info'=>$dulieu]);
        }
    }
}
