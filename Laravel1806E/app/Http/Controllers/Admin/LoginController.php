<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAddProductPost;

use App\Model\Login;

class LoginController extends Controller
{
    public function index()
    {
    	return view('login.index');
    }
    public function handleLogin(StoreLoginPost $request,Login $login)
    {
    	$username=$request->input('user',null); // dung cach nay khi khong biet co gia tri nhap vao hay khong
    	$password=$request->input('pass',null);
    	// dd($username,$password);

    	//validate form

    	// kiem tra du lieu co ton tai khong database hay khong
    	$infoAdmin=$login->checkAdminLogin($username,$password);
    	if($infoAdmin)
    	{
            // tao ra cac du lieu session cua tung input
            // put: tao ra session và lưu lại // thường dùng trong lưu lại tài khoản đăng nhập title
    		$request->session()->put('username',$infoAdmin['username']);
    		$request->session()->put('id',$infoAdmin['id']);
    		$request->session()->put('email',$infoAdmin['email']);
    		$request->session()->put('role',$infoAdmin['role']);

    		// di vao trang dasboad

    		return redirect()->route('admin.dashboard');
    		

    		// $_SESSION['username']=$infoAdmin['username']
    	}
    	else {
    		return redirect()->route('admin.login',['state'=>'fail']);
    	}
    }

    public function logout(Request $request)
    {
    	// xoa session
    	$request->session()->forget('username');
    	$request->session()->forget('id');
    	$request->session()->forget('email');
    	$request->session()->forget('role');
    	return redirect()->route('admin.login');
    }

}
