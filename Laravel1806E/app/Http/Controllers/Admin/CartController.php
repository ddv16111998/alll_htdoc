<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Gloudemans\Shoppingcart\Facades\Cart;
class CartController extends Controller
{
    public function add($id,Request $request)
    {
    	$id=is_numeric($id) ? $id :0;
    	$info=DB::table('products')->where('id',$id)->first();
    	if($info){
    		// add vao gio hang
            Cart::add(['id'=>$info->id,
                        'name'=>$info->name,
                        'qty'=>1,
                        'price'=>$info->money,
                        'options'=>[
                            'image'=>$info->image
                    ]
            ]);
            return redirect()->route('admin.listCart');
    		
    	}
    	else {
    		return redirect()->route('admin.dashboard');
    	}
    }
    public function index()
    {
        $cartData=Cart::content();
        $totalItem=Cart::content()->count();
        $totalBuy=Cart::count();
        $totalPrice=Cart::total();
        $data=[];

        $data['data']= $cartData;

        $data['total']=$totalItem;
        $data['totalBuy']=$totalBuy;
        $data['totalPrice']=$totalPrice;
        //view san pham trong gio hang
        return view('admin.cart.index',$data);
    }
    public function update(Request $request)
    {
        $id=$request->id;
        $qty=$request->qty;
        $checkQty=(is_numeric($qty)) && $qty>0 &&$qty<11 ? true :false;
        if($id&&$checkQty){
            Cart::update($id,$qty);
        }
        return redirect()->route('admin.listCart');
    }
    public function delete($id)
    {
        if($id)
        {
            Cart::remove($id);
        }
        return redirect()->route('admin.listCart');
    }
    public function remove()
    {
        if(Cart::content())
        {
            Cart::destroy();
        }
        return redirect()->route('admin.listCart');
    }
}
