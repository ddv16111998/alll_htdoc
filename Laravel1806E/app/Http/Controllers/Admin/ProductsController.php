<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
class ProductsController extends Controller
{
   public function index()
   {
   		$data =DB::table('products')->get();

   		
   		return view('admin.product.index')->with('data',$data);
   }
}
