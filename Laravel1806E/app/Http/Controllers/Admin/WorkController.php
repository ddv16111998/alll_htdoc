<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreWorkPost;
use App\Model\Work;

class WorkController extends Controller
{
    public function viewWork(Request $request,Work $workModel)
    {
        $result=$workModel->getAllData();
    	return view('admin.work.index',['info'=>$result]);
    }

    public function formAddWork()
    {
    	return view('admin.work.add');
    }
    public function workDataHanle(StoreWorkPost $request,Work $workModel)
    {
    	$company=$request->company;
    	$start_date=$request->start_date;
    	$end_date=$request->end_date;
    	$ranks=$request->ranks;
    	$status=$request->status;
    	$description=$request->description;
    	$address=$request->address;
    	$data=['company'=>$company,
    			'start_date'=>$start_date,
    			'end_date'=>$end_date,
    			'ranks'=>$ranks,
    			'status'=>$status,
    			'description'=>$description,
    			'address'=>$address,
    			'created_at'=>date('Y-m-d H:i:s'),
    			'updated_at'=>null
    			];
    	$result=$workModel->insertDataWork($data);
    	if($result)
    	{
            $dulieu=$workModel->getAllData();
    		$request->session()->flash('addData','Success');
    		return view('admin.work.index',['info'=>$dulieu]);
    	}

    }
}
