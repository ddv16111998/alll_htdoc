<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProfilePost;
use  App\Model\Profile;// su dung model
class ProfileController extends Controller
{
    public function index(Request $request,Profile $profileModel)
    {
        $data =[];

        //get keyword
        $keyword=$request->keyword;
        $keyword=strip_tags($keyword);


    	$message=$request->session()->get('addProfile');
        $dataProfile=$profileModel->getAllDataProfile($keyword);
        $data['mess']=$message;
        $data['info']=$dataProfile;
    	return view('admin.profile.index',$data); // truyen bien mess khong phai $message

        // in du lieu ra tu info


    }
    public function addView()
    {
    	return view('admin.profile.add');
    }
    public function handleAdd(StoreProfilePost $request,Profile $profileModel)
    {
    	$user=$request->input('fullname');
    	$nickname=$request->input('nickname');
    	$email=$request->input('email');
    	$phone=$request->input('phone');
    	$address=$request->input('address');
    	$date=$request->input('date');
    	$gender=$request->input('gender');
    	$single=$request->input('single');
    	$description=$request->input('description');
    	$avatarProfile=null;


    	// upload file
    	// $request :: du lieu nguoi dung gui len sever
    	if ($request->hasFile('avatar')) {
    		// lay ten file luu tren may cua nguoi dung
    		$photoFile=$request->file('avatar');
    		// lay ra ten duong dan cua file
    		$nameFile= $photoFile->getClientOriginalName();
    		if($nameFile)
    		{
    			// tao bien de luu ten file vao db
    			$avatarProfile=$nameFile;
    			$photoFile->move('upload/images',$nameFile);
    		}
    	}
    	//save data
    	$saveData= [
    		'fullname'=>$user,
    		'nickname'=>$nickname,
    		'email'=>$email,
    		'avatar'=>$avatarProfile,
    		'phone'=>$phone,
    		'address'=>$address,
    		'birthday'=>$date,
    		'gender'=>$gender,
    		'single'=>$single,
    		'status'=>1,
    		'description'=>$description,
    		'created_at'=>date('Y-m-d H:i:s'),
    		'updated_at'=>null
    	];
    	$save=$profileModel->saveProfile($saveData);
    	if ($save) {
    		$request->session()->flash('addProfile','add success'); // refesh se mat session flash
    		return redirect()->route('admin.profile');
    	}
    	else {
    		$request->session()->flash('addProfile','add fail');
    		return redirect()->route('admin.formAddProfile');
    	}

    }
    public function deleteProfile(Request $request,Profile $profileModel)
    {
        if($request->ajax())
        {
            $id=$request->idProfile;
            $id= is_numeric($id) ? $id : 0;
            $del= $profileModel->deleteProfileById($id);
            $data=[];
            $data['mess']=null; // them gia tri vao truong mess trong mang data
            // $data['mess'=>null]
            if($del)
            {
                $data['mess']='ok';
            }
            return response()->json($data);

        }
    }
    public function editProfile($id,Request $request,Profile $profileModel)
    {
        $id=is_numeric($id) ? $id : 0;
        $infoProfile=$profileModel->getDataInfoProfileById($id);
        if($infoProfile)
        {
            return view('admin.profile.edit',['info'=>$infoProfile]);
        }
        else {
            return abort(404);
        }
    }
    public function handleEdit($id,StoreProfilePost $request,Profile $profileModel)
    {
        // validate from data
        // lay thong tin tu db
        $infoProfile=$profileModel->getDataInfoProfileById($id);
        // lay cac thong tu form
        if($infoProfile)
        {
            $user=$request->input('fullname');
            $nickname=$request->input('nickname');
            $email=$request->input('email');
            $phone=$request->input('phone');
            $address=$request->input('address');
            $date=$request->input('date');
            $gender=$request->input('gender');
            $single=$request->input('single');
            $description=$request->input('description');
            $status=$request->status;
            $status=in_array($status,['0','1']) ? $status :0;
            $avatarProfile=$infoProfile['avatar'];
            // xu li upload file
            if ($request->hasFile('avatar')) {
            // lay ten file luu tren may cua nguoi dung
            $photoFile=$request->file('avatar');
            // lay ra ten duong dan cua file
            $nameFile= $photoFile->getClientOriginalName();
            if($nameFile)
            {
                // tao bien de luu ten file vao db
                $avatarProfile=$nameFile;
                $photoFile->move('upload/images',$nameFile);
            }
            }
            //update data
            $dataUpdate=[
                'fullname'=>$user,
                'nickname'=>$nickname,
                'email'=>$email,
                'avatar'=>$avatarProfile,
                'phone'=>$phone,
                'address'=>$address,
                'birthday'=>$date,
                'gender'=>$gender,
                'single'=>$single,
                'status'=>$status,
                'description'=>$description,
                'updated_at'=>date('Y-m-d H:i:s')

            ];
            $up= $profileModel->updateProfileById($dataUpdate,$id);
            if($up)
            {
                $request->session()->flash('addProfile','Edit success'); // refesh se mat session flash
                return redirect()->route('admin.profile');
            }
            else {
                return redirect()->route('admin.editProfile',['id'=>$id,'state'=>'fail']);
            }
        }
        else {
            return abort(404);
        }
            
        
    }

}
