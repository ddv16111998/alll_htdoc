<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
	public function __construct()
	{
		// // nap middleware
		// $this->middleware('my.check.age:user')->only('checkAgeWatchFilm'); // chi tac dong vao function nao do  // except:ngoai tru fn do
		// //only(['fn1','fn2',....])
        $this->middleware('check.name')->only('myName');
	}
    public function index($myName,$id,Request $request)
    {
    	$name=$request->name;
    	$id=$request->id;
    	return "hello-{$name}-{$id}";
    	// return "hello-{$myName}-{$id}";
    }
    public function checkAgeWatchFilm($age)
    {
    	//su dung middleware trong controller
    	return "my age-{$age}";

    }

    //request
    public function testRequest(Request $request)
    {
    	$id=$request->id;
    	$name=$request->name;
    	// dd($id,$name);//var_dump+die;
    	// $url=$request->fullUrl();
    	// dd($url);

    	//kiem tra phuong thuc gui len la phuong thuc nao
    	$request->method();

    	if($request->isMethod('post'))
    	{
    		echo 'AAAA';
    	}
    	// else {
    	// 	echo 'BBBB';
    	// }

    	// dd($request->all());
    	// $request->money; // khac pp duoi
    	// dd($request->input('money','100000'));// neu khong ton tai gia tri money thi gan n la 100000 hoac lay query string

    	dd($request->name);


    }
    public function myName($name)
    {
        return "Name:{$name}";
    }
}
