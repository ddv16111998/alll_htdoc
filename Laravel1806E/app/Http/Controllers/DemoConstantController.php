<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DemoConstantController extends Controller
{
    public function index()
    {
    	// su dung gia tri trong file constant
    	$name=\Config::get('constants.name_class');
    	dd($name);
    }
}
