<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;// su dung thu vien db trong laravel
class Post extends Model
{
    // chi dinh file model se lam viec voi bang nao trong database

    // 1 file model chi lam viec voi 1 bang database hay la 1 migration
    protected $table='post';


    // tao phuong thuc rang buoc voi bang comment
    public function comment()
    {
    	return $this->hasMany('App\Model\Comment');
    }
    public function getAllData()
    {
    	$data=DB::table('post')->get(); // query buiding
        // $obj=new Post();
        // $obj->get();                 // ORM laravel  // dung cach nao cung dk
    }
    public function getAllDataPost()
    {
    	return Post::all();   // ORM laravel
    	// <==> DB::table('post')->get();

    	$data =($data) ? $data->toArray() :[]; // nen dung cach nay// han che dung cach khac vi cach nay kiem tra luon du lieu co rong hay khong??
    	return $data;
    }
    public static function getDataById($id)
    {
    	return Post::find($id);
    	// <==> DB::table('admins')->where('id',$id)->first();
    }

}

// neu dung query buider thi muon thanh mang dung json_decode
