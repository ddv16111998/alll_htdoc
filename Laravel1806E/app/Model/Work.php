<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Work extends Model
{
	protected $table='works';
	
	public function changeData($data)
	{
		return $data ? $data->toArray() : [];
	}
	public function getAllData()
	{
		$data=Work::paginate(2);// paginate cung lay duoc du lieu ra và phân trang luôn
		return $data;
	}
	
   public function insertDataWork($data)
   {
   		$insert=Work::insert($data);
   		if($insert)
   		{
   			return true;
   		}
   		else {
   			return false;
   		}
   }
   
}
