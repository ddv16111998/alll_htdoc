<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Profile extends Model
{
    protected $table ='profiles';
    private $limit=2; // phan bao nhieu dong du lieu tren 1 trang

    // ham danh cho ORM
    private function changeDataToArray($data) 
    {
    	return ($data) ? $data->toArray() : [];
        // toArray() chi danh cho ORM
    }
    public function deleteProfileById($id)
    {
        $profile=Profile::find($id);
        if($profile->delete())
        {
            return true;
        }
        return false;
    }
    public function saveProfile($data)
    {
    	$insert=DB::table('profiles')->insert($data);
    	if ($insert) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }
    public function getAllDataProfile($key = '')
    {
    	$data= Profile::where('fullname','like','%'.$key.'%')->orWhere('email','like','%'.$key.'%')->orWhere('phone','like','%'.$key.'%')->paginate($this->limit);
    	// $data=$this->changeDataToArray($data); // to_array() chi danh cho ORM
        return $data;
    }
    public function getDataInfoProfileById($id)
    {
        $info=Profile::find($id);
        return $this->changeDataToArray($info);
    }
    public function updateProfileById($data,$id)
    {
        $up=DB::table('profiles')->where('id',$id)->update($data);
        if($up)
        {
            return true;
        }
        else {
            return false;
        }

    }
    public function getInfoProfileByActive($status)
    {
        $info=Profile::where('status',$status)->first();
        return $info;
    }
    
}
