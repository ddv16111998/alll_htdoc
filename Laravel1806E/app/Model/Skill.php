<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
    protected $table='skills';
    
    protected function changeData($data)
    {
    	return $data ? $data->toArray() :[];
    }
    public function getAllData()
    {
    	$data=Skill::paginate(5);
    	return $data;
    }
    public function insertData($data)
    {
    	$insert=Skill::insert($data);
    	if($insert)
    	{
    		return true;
    	}
    	else {
    		return false;
    	}
    }
    public function deleteData($id)
    {
        $find=Skill::find($id); 
        if($find)
        {
            $find->delete();
            return true;
        }
        else {
            return false;
        }
    }
    public function editData($id)
    {
        $data=Skill::find($id);
        return $this->changeData($data);
    }
    public function searchData($keyword='')
    {
        $data=Skill::where('language','like','%'.$keyword.'%')->paginate(3);
        return $data;
    }
    public function updateData($id,$data)
    {
        $update=Skill::find($id);
        if($update)
        {
            $update->update($data);
            return true;
        }
        return false;
    }
}
