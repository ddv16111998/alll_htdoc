<?php
	return [
		'fullname'=>'Fullname',
		'nickname'=>'Nickname',
		'email'=>'Email',
		'avatar'=>'Avatar',
		'phone'=>'Phone',
		'address'=>'Address',
		'birthday'=>'Birthday',
		'gender'=>'Gender',
		'single'=>'Single',
		'change'=>'Change'
	]; 
 ?>