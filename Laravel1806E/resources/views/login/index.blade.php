<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login page</title>
	<link rel="stylesheet" href="{{ asset('css/demo/bootstrap.min.css') }}">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 offset-lg-3 offset-md-3">
				<div class="row mt-4">
					<div class="col-lg-12 col-md-12">
						<div class="error">
							@if ($errors->any())
							    <div class="alert alert-danger">
							        <ul>
							            @foreach ($errors->all() as $error)
							                <li>{{ $error }}</li>
							            @endforeach
							        </ul>
							    </div>
							@endif
						</div>
					</div>
				</div>
				<form action="{{ route('admin.handle-login') }}" method="post" class="mt-4">
					<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
					<h1 class="text-center">Page Login</h1>
					<div class="form-group">
						<label for="user">Username</label>
						<input type="text" name="user" id="user" class="form-control">
							
					</div>
					<div class="form-group">
						<label for="pass">Password</label>
						<input type="password" name="pass" id="pass" class="form-control">
					</div>
					<button type="submit" name="btnLogin" class="btn btn-primary">Login</button>
				</form>
			</div>
		</div>
	</div>
</body>
</html>