@extends('admin.base')
@section('content')
        <div class="container-fluid">
         <div class="row">
         	<div class="col-md-12">
         		<h3>This is Cart</h3>
         	</div>
         </div>
          <div class="row">
            <div class="col-md-12">
            <table class="table">
            	<thead>
            		<th>#</th>
            		<th>Name</th>
            		<th>Image</th>
            		<th>Qty</th>
            		<th>Price</th>
            		<th>Money</th>
            		<th colspan="2" width="3%">Action</th>
            	</thead>
            	@php
            		$i=1;
            	@endphp
            	<tbody>
            		@foreach($data as $key =>$item)
            		<tr>
            			<td>{{ $i++ }}</td>
            			<td>{{ $item->name }}</td>
            			<td><img src="{{ $item->options->image}}" alt="" width="120" height="120"></td>
            			<td><input type="text" value="{{ $item->qty }}" class="qty_{{ $item->id }}"></td>
            			<td>{{ $item->price }}</td>
            			<td>{{ ($item->qty *$item->price) }}</td>
            			<td>
            				<button href="" class="btn btn-primary" onclick="updateCart('{{ $key }}',{{ $item->id }},this)">Update</button>
            			</td>
            			<td>
            				<a href="{{ route('admin.deleteCart',['id'=>$key]) }}" class="btn btn-danger">Delete</a>
            			</td>
            		</tr>
            		@endforeach
            		
            	</tbody>
            	<tfoot>
            		<tr>
            			<td>
            				Total Item:{{ $total}}
            			</td>
            			<td>Total Buy :{{ $totalBuy }}</td>
            			<td>Total Price :{{ $totalPrice }} </td>
            			<td><a href="{{ route('admin.deleteAll') }}" class="btn btn-danger">Delete All</a></td>

            		</tr>
            	</tfoot>
            </table>
            </div>
          </div>
        </div>
@endsection
@push('javascript')
	<script type="text/javascript">
		function updateCart(idCart,idRow) {
			let qty=$('.qty_'+idRow).val().trim();
			let url="{{ route('admin.updateCart') }}"+"?id="+idCart+"&qty="+qty; // gia tri request
			window.location.href=url;
		}
	</script>
@endpush