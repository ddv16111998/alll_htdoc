@extends('admin.base')
@section('content')
        <div class="container-fluid">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="{{ route('admin.profile') }}">Profile</a>
            </li>
            <li class="breadcrumb-item active">Overview</li>
          </ol>
          <div class="row">
            <div class="col-md-12">
              <h3 class="text-center">Edit Profiles</h3>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-12">
                @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
               @endif
              </div>
                <form action="{{ route('admin.handleEdit',['id'=>$info['id']]) }}" method="post" enctype="multipart/form-data">
                  @csrf
                  <div class="form-group">
                    <label for="fullname">(*) FullName</label>
                    <input type="text" class="form-control" id="fullname" name="fullname" value="{{ $info['fullname'] }}">
                    <input type="hidden" id="" name="idProfile" value="{{ $info['id'] }}">
                  </div>
                   <div class="form-group">
                    <label for="nickname">NickName</label>
                    <input type="text" class="form-control" id="nickname" name="nickname" value="{{ $info['nickname'] }}"  >
                  </div>
                   <div class="form-group">
                    <label for="email">(*) Email</label>
                    <input type="text" class="form-control" id="email" name="email" value="{{ $info['email'] }}">
                  </div>
                   <div class="form-group">
                    <label for="avatar">Avatar</label>
                    <input type="file" id="avatar" name="avatar">
                    <br>
                    <img src="{{ URL::to('/upload/images') }}/{{ $info['avatar'] }}" alt="avatar" width="120" height="120">
                  </div>
                   <div class="form-group">
                    <label for="phone">(*) Phone</label>
                    <input type="text" class="form-control" id="phone" name="phone" value="{{ $info['phone'] }}">
                  </div>
                   <div class="form-group">
                    <label for="address">(*) Address</label>
                    <input type="text" class="form-control" id="address" name="address" value="{{ $info['address'] }}">
                  </div>
                   <div class="form-group">
                    <label for="date">(*)Date</label>
                    <input type="date" class="form-control" id="date" name="date" value="{{ $info['birthday'] }}">
                  </div>

                   <div class="form-group">
                    <label for="gender">(*)Gender</label>
                    <select name="gender" id="gender" class="form-control">
                      <option value="1" {{ $info['gender'] ==1 ? 'selected=selected ' : ''}}>Nam</option>
                      <option value="0" {{ $info['gender'] ==0 ? 'selected=selected ' : ''}}>Nu</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="single">(*)Single</label>
                    <select name="single" id="single" class="form-control">
                      <option value="1" {{ $info['gender'] ==1 ? 'selected=selected ' : ''}} >Doc Than</option>
                      <option value="0" {{ $info['gender'] ==0 ? 'selected=selected ' : ''}}>Co gia dinh</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <label for="Status">(*)Status</label>
                    <select name="status" id="status" class="form-control">
                      <option value="1" {{ $info['status'] ==1 ? 'selected=selected ' : ''}} >Active</option>
                      <option value="0" {{ $info['status'] ==0 ? 'selected=selected ' : ''}}>Deative</option>
                    </select>
                  </div>
                  
                  <div class="form-group">
                    <label for="description">(*) Description</label>
                    <textarea name="description" id="description" rows="8" class="form-control">
                      {!! $info['description'] !!}
                    </textarea>
                  </div>
                  <button type="submit" class="btn btn-primary btn-block" name="editProfile">Save + </button>
                </form>
            </div>
          </div>
        </div>
@endsection
        
