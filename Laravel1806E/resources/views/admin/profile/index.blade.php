@extends('admin.base')
@section('content')
        <div class="container-fluid">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="">Profile</a>
            </li>
            <li class="breadcrumb-item active">Overview</li>
          </ol>
          <div class="row">
            <div class="col-md-12">
              <h3 class="text-center">This is Profiles</h3>
              @if($mess)
                <div class="alert alert-info text-center">
                   <h3> {{ $mess }}</h3>
                </div>
              @endif
              
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <a href="{{ route('admin.formAddProfile') }}" class="btn btn-primary">Add ++</a>
            </div>
            <div class="col-md-8">
                <div class="row">
                  <div class="col-md-9">
                    <input type="text" class="form-control" id="txtSearch" name="search">
                  </div>
                  <div class="col-md-3">
                    <button class="btn btn-primary" id="btnSearch" type="submit">Search</button>
                  </div>
                </div>

            </div>
          </div>

          <div class="row mt-4">
            <div class="col-md-12">
              <table class="table table-hover">
                <tr class="text-center">
                 
                  <td>@lang('profile_lang.fullname')</td>
                  <td>Nickname</td>
                  <td>Email</td>
                  <td>Avatar</td>
                  <td>Phone</td>
                  <td>Address</td>
                  <td>Birthday</td>
                  <td>Gender</td>
                  <td>Single</td>
                  <td>Change</td>
                </tr>
                @foreach($info as $value)
                  <tr>
                    <td>{{ $value->fullname }}</td>
                    <td>{{ $value->nickname }}</td>
                    <td>{{ $value->email }}</td>
                    <td><img src="{{ URL::to('/upload/images') }}/{{ $value->avatar }}" alt="" width="120" height="120"></td>
                    {{-- URL::to(/) =>trỏ đến thư mục public --}}
                    <td>{{ $value->phone }}</td>
                    <td>{{ $value->address }}</td>
                    <td>{{ $value->birthday }}</td>
                    <td>{{ $value->gender ==1 ? 'nam' : 'nữ' }}</td>
                    <td>{{ $value->single ==1 ? 'doc than' : 'có gia đình' }}</td>
                    <td>
                      <a href="{{ route('admin.editProfile',['id'=>$value->id]) }}" class="btn btn-info">Edit</a>
                    </td>
                    <td>
                      <a href="" data-id="{{ $value->id }}" class="btn btn-danger btn-delete-profile" id="profile-{{ $value->id }}">Delete</a>
                    </td>
                  </tr>
                @endforeach
              </table>
            </div>
            <div class="col-md-12">
              {{ $info->appends(request()->query())->links() }}
            </div>
          </div>
        </div>
        @endsection
        {{-- mo dau viet js --}}
        @push('javascript')
          <script type="text/javascript">
            $(function(){
              $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                  }
              });

              // bat su kien tim kiem
              $('#btnSearch').click(function(){
                // lau keyword nguoi dung nhap
                let keyword = $('#txtSearch').val().trim();
                if(keyword.length>0)
                {
                  window.location.href="{{ route('admin.profile') }}" + "?keyword=" + keyword;
                }
              });

              // viet ajax o day
              $('.btn-delete-profile').click(function(){
                // can lay duoc id tuong cua profiles
                let idProfile=$(this).attr('data-id');
                idProfile=Number.parseInt(idProfile);
                if(Number.isInteger(idProfile) && idProfile>0)
                {
                  $.ajax({
                    url:"{{ route('admin.deleteProfile') }}",
                    type:"POST",
                    data:{idProfile:idProfile},
                    beforeSend:function(){
                      $('#profile-'+idProfile).text('Loading...');
                    },
                    success:function(data){
                      $('#profile-'+idProfile).text('Delete');
                      if(data.mess==='ok')
                      {
                        alert('Successfull');
                        window.location.reload(true);
                      }
                      else {
                        alert('fail');
                        return false;
                      }
                    }
                  });
                }
              });
            });
          </script>
        @endpush

        
