@extends('admin.base')
@section('content')
        <div class="container-fluid">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="{{ route('admin.profile') }}">Profile</a>
            </li>
            <li class="breadcrumb-item active">Overview</li>
          </ol>
          <div class="row">
            <div class="col-md-12">
              <h3 class="text-center">Add Profiles</h3>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-12">
                @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
               @endif
              </div>
                <form action="{{ route('admin.handleAddProfile') }}" method="post" enctype="multipart/form-data">
                  @csrf
                  <div class="form-group">
                    <label for="fullname">(*) FullName</label>
                    <input type="text" class="form-control" id="fullname" name="fullname">
                  </div>
                   <div class="form-group">
                    <label for="nickname">NickName</label>
                    <input type="text" class="form-control" id="nickname" name="nickname">
                  </div>
                   <div class="form-group">
                    <label for="email">(*) Email</label>
                    <input type="text" class="form-control" id="email" name="email">
                  </div>
                   <div class="form-group">
                    <label for="avatar">Avatar</label>
                    <input type="file" id="avatar" name="avatar">
                  </div>
                   <div class="form-group">
                    <label for="phone">(*) Phone</label>
                    <input type="text" class="form-control" id="phone" name="phone">
                  </div>
                   <div class="form-group">
                    <label for="address">(*) Address</label>
                    <input type="text" class="form-control" id="address" name="address">
                  </div>
                   <div class="form-group">
                    <label for="date">(*)Date</label>
                    <input type="date" class="form-control" id="date" name="date">
                  </div>

                   <div class="form-group">
                    <label for="gender">(*)Gender</label>
                    <select name="gender" id="gender" class="form-control">
                      <option value="1">Nam</option>
                      <option value="0">Nu</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="single">(*)Single</label>
                    <select name="single" id="single" class="form-control">
                      <option value="1">Doc Than</option>
                      <option value="0">Co gia dinh</option>
                    </select>
                  </div>
                  
                  <div class="form-group">
                    <label for="description">(*) Description</label>
                    <textarea name="description" id="description" rows="8" class="form-control"></textarea>
                  </div>
                  <button type="submit" class="btn btn-primary btn-block" name="addProfile">Save + </button>
                </form>
            </div>
          </div>
        </div>
@endsection
        
