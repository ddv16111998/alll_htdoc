@extends('admin.base')
@section('content')
	 <div class="container-fluid">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="">Skills</a>
            </li>
            <li class="breadcrumb-item active">Overview</li>
          </ol>
          <div class="row">
            <div class="col-md-12">
              <h3 class="text-center">Result Search Skills</h3>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <a href="{{ route('admin.formAddSkill') }}" class="btn btn-info">Add ++ </a>
            </div>
            <div class="col-md-8 mb-4">
              <form action="{{ route('admin.searchData') }}" method="post">
                @csrf
                <div class="row">
                  <div class="col-md-9">
                    <input type="text" class="form-control" name="search" id="search" placeholder="Tim kiem">
                  </div>
                  <div class="col-md-3">
                    <button type="submit" name="btnSearch" id="btnSearch" class="btn btn-info">Search</button>
                  </div>
                </div>
              </form>
            </div>
          </div> 
          <div class="row">
           <div class="col-md-12">
             <table class="table table-hover">
               <tr>
                 <td>Language</td>
                 <td>Level</td>
                 <td>Status</td>
                 <td>Change</td>
               </tr>
               @foreach($info as $value)
               <tr>
                 <td>{{ $value->language }}</td>
                 <td>{{ $value->level }}</td>
                 <td>{{ $value->status }}</td>
                 <td>
                   <a href="{{ route('admin.viewEditData',['id'=>$value->id]) }}" class="btn btn-info">Edit</a>
                   <a href="{{ route('admin.deleteData',['id'=>$value->id]) }}" class="btn btn-danger">Delete</a>
                 </td>
               </tr>
               @endforeach
             </table>
           </div>
          <div class="col-md-12">
            {{ $info->appends(request()->query())->links() }}
          </div>
         </div>


          
     </div>
@endsection