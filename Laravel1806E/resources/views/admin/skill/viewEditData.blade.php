@extends('admin.base')
@section('content')
	 <div class="container-fluid">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="">Skills</a>
            </li>
            <li class="breadcrumb-item active">Overview</li>
          </ol>
          <div class="row">
            <div class="col-md-12">
              <h3 class="text-center">Edit Skills</h3>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
               @endif
              </div>
          </div>
          <form action="{{ route('admin.updateData',['id'=>$oldData['id']]) }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label for="language">(*) Language</label>
              <input type="text" class="form-control" id="language" name="language" value="{{ $oldData['language'] }}">
            </div>
            <div class="form-group">
              <label for="level">(*)Level</label>
              <input type="number" class="form-control" id="level" name="level" value="{{ $oldData['level'] }}">
            </div>

            <div class="form-group">
              <label for="status">(*)Status</label>
              <select name="status" id="status" class="form-control">
                <option value="1" {{ $oldData['status']==1 ? 'selected=selected' : '' }} >Active</option>
                <option value="0" {{ $oldData['status'] ==0 ? 'selected=selected': '' }}>DisAtive</option>
              </select>
            </div>

             <button type="submit" class="btn btn-primary btn-block" name="addSkill">Update + </button>
          </form>
         

          
     </div>
@endsection