@extends('admin.base')
@section('content')
	 <div class="container-fluid">
          <div class="row">
            <div class="col-md-12">
              <h3 class="text-center">Add Works</h3>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
                @if ($errors->any())
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
               @endif
              </div>
          </div>
          <form action="{{ route('admin.workDataHanle') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label for="company">(*) Company</label>
              <input type="text" class="form-control" id="company" name="company">
            </div>
            <div class="form-group">
              <label for="start_date">(*) Start_Date</label>
              <input type="date" class="form-control" id="start_date" name="start_date">
            </div>
            <div class="form-group">
              <label for="end_date">End_date</label>
              <input type="date" class="form-control" id="end_date" name="end_date">
            </div>
             <div class="form-group">
              <label for="rank">(*) Ranks</label>
              <input type="number" class="form-control" id="ranks" name="ranks">
            </div>
    
            <div class="form-group">
              <label for="status">(*)Status</label>
              <select name="status" id="status" class="form-control">
                <option value="1">Active</option>
                <option value="0">DisAtive</option>
              </select>
            </div>
            <div class="form-group">
              <label for="description">(*) Description</label>
              <textarea name="description" id="description" rows="8" class="form-control"></textarea>
            </div>
            <div class="form-group">
              <label for="address">Address</label>
              <input type="text" class="form-control" id="address" name="address">
            </div>

             <button type="submit" class="btn btn-primary btn-block" name="addProfile">Save + </button>
          </form>

         
          
     </div>
@endsection