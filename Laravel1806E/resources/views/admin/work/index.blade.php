@extends('admin.base')
@section('content')
	 <div class="container-fluid">
          <ol class="breadcrumb">
            <li class="breadcrumb-item">
              <a href="">Works</a>
            </li>
            <li class="breadcrumb-item active">Overview</li>
          </ol>
          <div class="row">
            <div class="col-md-12">
              <h3 class="text-center">Works</h3>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              @if(session('addData'))
                <div class="alert alert-info text-center">
                   <h3> {{session('addData') }}</h3>
                </div>
              @endif
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <a href="{{ route('admin.formAddWork') }}" class="btn btn-info">Add ++ </a>
            </div>
            <div class="col-md-8 mb-4">
              <form action="" method="post">
                @csrf
                <div class="row">
                  <div class="col-md-9">
                    <input type="text" class="form-control" name="search" id="search" placeholder="Tim kiem">
                  </div>
                  <div class="col-md-3">
                    <button type="submit" name="btnSearch" id="btnSearch" class="btn btn-info">Search</button>
                  </div>
                </div>
              </form>
            </div>
          </div> 
          <div class="row">
            <div class="col-md-12">
              <table class="table table-hover">
                <tr>
                  <td>Company</td>
                  <td>Start_date</td>
                  <td>End_date</td>
                  <td>Ranks</td>
                  <td>Status</td>
                  <td>Description</td>
                  <td>Address</td>
                  <td>Change</td>
                </tr>
                @foreach($info as $value)
                  <tr>
                    <td>{{ $value->company }}</td>
                    <td>{{ $value->start_date }}</td>
                    <td>{{ $value->end_date }}</td>
                    <td>{{ $value->ranks }}</td>
                    <td>{{ $value->status }}</td>
                    <td>{{ $value->description }}</td>
                    <td>{{ $value->address }}</td>
                    <td>
                     <a href="" class="btn btn-info">Edit</a>
                     <a href="" class="btn btn-danger">Delete</a>
                   </td>
                  </tr>
                @endforeach
              </table>
            </div>
          </div>
          <div class="row">
           <div class="col-md-12">
              
            {{ $info->appends(request()->query())->links() }}
        
           </div>
         </div>
         
     </div>
@endsection
