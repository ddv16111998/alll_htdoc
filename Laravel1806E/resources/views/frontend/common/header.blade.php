<header id="home">

      <nav id="nav-wrap">

         <a class="mobile-btn" href="#nav-wrap" title="Show navigation">Show navigation</a>
	      <a class="mobile-btn" href="#" title="Hide navigation">Hide navigation</a>

         <ul id="nav" class="nav">
            <li class="current"><a href="{{ route('frontend.profile') }}">@lang('mylang.home')</a></li>
	         <li><a  href="{{ route('frontend.resume') }}">@lang('mylang.resume')</a></li>
            <li><a  href="{{ route('frontend.work') }}">@lang('mylang.work')</a></li>
            <li><a  href="{{ route('frontend.skill') }}">@lang('mylang.skill')</a></li>
            <li><a  href="">@lang('mylang.project')</a></li>
            <li>
               <a href="{{ route('switchLang',['lang'=>'vi']) }}">Vietnamese</a>
            </li>
            <li>
               <a href="{{ route('switchLang',['lang'=>'en']) }}">English</a>
            </li>
         </ul> <!-- end #nav -->

      </nav> <!-- end #nav-wrap -->

      <div class="row banner">
         <div class="banner-text">
            <h1 class="responsive-headline">{{ $info->fullname }}</h1>
            <p>{{ $info->email }}</p>
            <hr />
            <ul class="social">
               <li><a href="#"><i class="fa fa-facebook"></i></a></li>
               <li><a href="#"><i class="fa fa-twitter"></i></a></li>
               <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
               <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
               <li><a href="#"><i class="fa fa-instagram"></i></a></li>
               <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
               <li><a href="#"><i class="fa fa-skype"></i></a></li>
            </ul>
         </div>
      </div>

      <p class="scrolldown">
         <a  href="#about"><i class="icon-down-circle"></i></a>
      </p>

   </header> <!-- Header End -->