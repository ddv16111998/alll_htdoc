<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Demo Lang</title>
</head>
<body>
	<h1>Thong tin</h1>
	<ul>
		<li>
			@lang('common.name')
		</li>
		<li>
			@lang('common.work')
		</li>
	</ul>
</body>
</html>