<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>demo view laravel</title>
	<link rel="stylesheet" href="{{ asset('css/demo/bootstrap.min.css') }}">
</head>
<body>
	<div class="container">
		@include('common.header')
		@include('common.menu')
		@yield('content')
		@include('common.footer')
	</div>
</body>
</html>