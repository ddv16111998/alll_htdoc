<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>demo</title>
	<style type="text/css">
		.chan{
			background: blue;
		}
		.le{
			background: red;
		}
	</style>
</head>
<body>
	<h1>demo view</h1>
	{{-- <p>Name:{{ $info['name'] }}</p>  $info dai dien cho mang $data
	<p>Name:{{ $info['age'] }}</p> --}}
	{{-- <p>Money:{{ $m }}</p> --}}
	<table width="100%" border="1" cellpadding="0" cellpadding="0">
		<tr>
			<td>masv</td>
			<td>name</td>
			<td>age</td>
			<td>email</td>
			<td>phone</td>
			<td>Money</td>
		</tr>
		@php
			$total=0;
		@endphp
		@foreach($info as $key=>$val)
			@php
				$total=$total+$val['money'];
				
			@endphp
			<tr class="{{ $key%2==0 ?"chan":"le" }}">   
				{{-- {{  }} == <?php echo ?>--}}
				<td>{{ $val['masv'] }}</td>
				<td>{{ $val['name'] }}</td>
				<td>{{ $val['age'] }}</td>
				<td>{{ $val['email'] }}</td>
				<td>{{ $val['phone'] }}</td>
				<td>{{ $val['money'] }}</td> {{-- {{  }} == <?php echo ?>--}}
			</tr>
		@endforeach
		<tfoot>
			<tr>
				<td colspan="5">ToTal Money</td>
				<td>{{ $total }}</td>
			</tr>
		</tfoot>

	</table>
</body>
</html>