<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1;$i<=5;$i++)
        {
        	DB::table('post')->insert([
        	'title'=>str_random(10),
        	'content'=>str_random(50),
        	'created_at'=>date('Y-m-d H:i:s'),
        	'updated_at'=>null
        	
        	]);
        }
    }
}
