<?php

use Illuminate\Database\Seeder;

class SkillsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1;$i<11;$i++)
        {
        	DB::table('skills')->insert([
        		'language'=>str_random(10),
        		'level'=>1,
        		'status'=>1,
        		'created_at'=>date('Y-m-d H:i:s'),
        		'updated_at'=>null
        	]);
        }
    }
}
