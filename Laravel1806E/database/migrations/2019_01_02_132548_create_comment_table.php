<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::enableForeignKeyConstraints();

        Schema::create('comment', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->text('questions');
            $table->integer('id_post')->unsigned();
            $table->timestamps();
            $table->foreign('id_post')->references('id')->on('post')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        //khi xoa bang
         Schema::create('comment', function (Blueprint $table) {
            $table->dropForeign(['id_post']);
         });
        Schema::dropIfExists('comment');
    }
}
