<?php

Route::group(['prefix' => 'system/diary','middleware' => 'permission'], function() {
	Route::get('/', 'DiarySystemController@index')->name('admin.diary.index');
});