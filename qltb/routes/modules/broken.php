<?php
Route::group(['prefix'=>'broken', 'middleware' => 'permission'], function () {
	Route::get('', 'BrokenController@index')->name('admin.broken.index');
	Route::get('/create', 'BrokenController@create')->name('admin.broken.create');
	Route::post('/store', 'BrokenController@store')->name('admin.broken.store');
	Route::get('/edit/{id?}', 'BrokenController@edit')->name('admin.broken.edit');
	Route::post('/update/{id?}', 'BrokenController@update')->name('admin.broken.update');
	Route::get('/destroy/{id?}', 'BrokenController@destroy')->name('admin.broken.destroy');
	Route::get('/exportExcel', 'BrokenController@exportExcel')->name('admin.broken.excel');
});
