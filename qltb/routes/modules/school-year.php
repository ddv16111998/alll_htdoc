<?php
Route::group(['prefix'=>'school-year'], function () {
    Route::get('/', 'SchoolYearController@index')->name('admin.school-year.index');
    Route::get('/create', 'SchoolYearController@create')->name('admin.school-year.create');
    Route::post('/store', 'SchoolYearController@store')->name('admin.school-year.store');
    Route::get('/edit/{id}', 'SchoolYearController@edit')->name('admin.school-year.edit');
    Route::post('/update/{id}', 'SchoolYearController@update')->name('admin.school-year.update');
    Route::get('/destroy/{id}', 'SchoolYearController@destroy')->name('admin.school-year.destroy');
});
