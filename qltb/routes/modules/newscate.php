<?php
Route::group(['prefix' => 'newscate','middleware' => 'permission'], function() {
    Route::get('/', 'NewsCateController@index')->name('admin.newscate');
    Route::get('/anydata', 'NewsCateController@anydata')->name('admin.anydata');
    Route::get('/create', 'NewsCateController@create')->name('admin.newscate.create');
});
?>
