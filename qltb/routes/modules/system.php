<?php

Route::group(['prefix' => 'system','middleware' => 'permission'], function() {
	Route::get('/show-log', 'SystemController@show_log')->name('admin.system.index');
});