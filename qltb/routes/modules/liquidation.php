<?php
    Route::group(['prefix'=>'liquidation'],function (){
        Route::get('/','LiquidationController@index')->name('admin.liquidation.index');
        Route::get('/create','LiquidationController@create')->name('admin.liquidation.create');
        Route::post('/store','LiquidationController@store')->name('admin.liquidation.store');
        Route::get('/edit/{id}','LiquidationController@edit')->name('admin.liquidation.edit');
        Route::post('/edit/{id}','LiquidationController@update')->name('admin.liquidation.update');
        Route::get('/destroy/{id}','LiquidationController@destroy')->name('admin.liquidation.destroy');
    });
