<?php 
Route::group(['prefix'=>'deviceup', 'middleware' => 'permission'], function () {
	Route::get('', 'DeviceupController@index')->name('admin.deviceup.index');
	Route::get('/create', 'DeviceupController@create')->name('admin.deviceup.create');
	Route::post('/store', 'DeviceupController@store')->name('admin.deviceup.store');
	Route::get('/edit/{id?}', 'DeviceupController@edit')->name('admin.deviceup.edit');
	Route::post('/update/{id?}', 'DeviceupController@update')->name('admin.deviceup.update');
	Route::get('/destroy/{id?}', 'DeviceupController@destroy')->name('admin.deviceup.destroy');

	Route::get('/exportExcel', 'DeviceupController@exportExcel')->name('admin.deviceup.excel');
});