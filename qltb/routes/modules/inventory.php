<?php
Route::group(['prefix'=>'inventory'], function () {
        Route::get('/','InventoryController@index')->name('admin.inventory.index');
        Route::get('create','InventoryController@create')->name('admin.inventory.create');
        Route::post('store','InventoryController@store')->name('admin.inventory.store');
        Route::get('edit/{id}','InventoryController@edit')->name('admin.inventory.edit');
        Route::post('update/{id}','InventoryController@update')->name('admin.inventory.update');
        Route::get('destroy/{id}','InventoryController@destroy')->name('admin.inventory.destroy');
        Route::get('new-broken/{id}','InventoryController@newBroken')->name('admin.inventory.newBroken');
    });
