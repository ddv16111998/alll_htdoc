<?php 
Route::group(['prefix'=>'classes'], function () {
	Route::get('', 'ClassesController@index')->name('admin.classes.index');
	Route::get('/create', 'ClassesController@create')->name('admin.classes.create');
	Route::post('/store', 'ClassesController@store')->name('admin.classes.store');
	Route::get('/edit/{id?}', 'ClassesController@edit')->name('admin.classes.edit');
	Route::post('/update/{id?}', 'ClassesController@update')->name('admin.classes.update');
	Route::get('/destroy/{id?}', 'ClassesController@destroy')->name('admin.classes.destroy');
});