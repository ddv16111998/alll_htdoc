# Một số chú ý khi code:

Xem giao diện page thêm mới ở file : resources\templates\views\create.stub

Xem giao diện page danh sách ở file : resources\templates\views\index.stub

Xem giao diện page sửa ở file : resources\templates\views\edit.stub


Xem Cấu trúc Controller: resources\templates\Controller.stub

Xem Cấu trúc Model: resources\templates\Model.stub

Xem Cấu trúc Route: resources\templates\routes.stub

Xem Cấu trúc Request: resources\templates\Request.stub

Xem Cấu trúc Repository: resources\templates\Repository.stub


Cấu trúc thư mục views: 


*  resources
   
*  __ views
   
*  _____themes 
  
*  ________ master.blade.php
 
*  _____layouts

*  _________ footer.blade.php
 
*  _________ top_bar.blade.php

*  _________ left_sidebar.blade.php

*  _____auth

*  _________passwords

*  _____________ ... ( các file view phục vụ việc reset pass )

*  _________ ... ( các file view phục vụ đăng kí , đăng nhập )

*  ______dashboard ( chứa các view phục vụ cho hiển thị dashboard )

*  ______user

*  _________index.blade.php

*  _________create.blade.php

*  _________edit.blade.php

*  ______ ....


Note: khi làm 1 issue thì tạo nhánh mới theo id của issue cùng với tên người làm và module chức năng

  VD: 
    
*  Khi làm Viện làm thêm mới user có id issue là 2 thì tạo nhánh như sau: user/vien-issue-2
    
*  Khi làm Tuấn làm sửa user có id issue là 3 thì tạo nhánh như sau: user/tuan-issue-3


Khi commit code lên thì ghi nội dung 'fixed #2' nếu issue có  id = 2

Khi commit code lên thì ghi nội dung 'fixed #3' nếu issue có  id = 3

Không tự ý merge code, khi commit xong thì lên trang gitlab.com vào project tạo merge request