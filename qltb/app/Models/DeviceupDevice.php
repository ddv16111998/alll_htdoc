<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Deviceup_device extends Model
{
  protected $table = 'deviceup_device';
  protected $guarded =[];
  protected $dates =['deleted_at'];

  public function deviceup()
  {
  	return $this->belongsTo('App\Models\Deviceup');
  }

  public function device() {
    return $this->belongsToMany('App\Models\Device');
  }
}
