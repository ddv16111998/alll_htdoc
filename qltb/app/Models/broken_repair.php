<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Broken_repair extends Model
{
    protected $table ='broken_repair';
    protected $guarded =[];

    public function device_repair()
    {
        return $this->belongsTo('App\Models\Device_Repair','repair_id');
    }

    public function device(){
        return $this->belongsToMany('App\Models\Device');
    }
}
