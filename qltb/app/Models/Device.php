<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Device extends Model
{

	use SoftDeletes;

    protected $table = 'devices';

    protected $dates =['deleted_at'];

    protected $guarded=[];
    //4 bảng là school,  block,class và classroom
    function school() {
        return $this->belongsToMany('App\Models\School','school_devices');
    }
    function class() {
        return $this->belongsToMany('App\Models\Classes','class_devices');
    }
    function block() {
        return $this->belongsToMany('App\Models\Block','block_devices');
    }
    function classroom() {
        return $this->belongsToMany('App\Models\Classroom','classroom_devices');
    }
    public function BrokenDevice() {
        return $this->belongsToMany('App\Models\BrokenDevice');
    }
    public function broken_repair() {
        return $this->belongsToMany('App\Models\Broken_repair');
    }
    public function DeviceupDevice() {
        return $this->belongsToMany('App\Models\DeviceupDevice');
    }
    public function school_year()
    {
        return $this->belongsTo('App\Models\SchoolYear','school_year_id');
    }
//    public function school_devices()
//    {
//        return $this->hasOne('App\Models\');
//    }
}
