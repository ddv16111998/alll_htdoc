<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Deviceup extends Model
{
  use SoftDeletes;

  protected $table = 'deviceups';
  protected $guarded =[];
  protected $dates =['deleted_at'];

  public function device()
  {
  	return $this->belongsToMany('App\Models\Device')->withPivot('amount', 'note');
  }
}
