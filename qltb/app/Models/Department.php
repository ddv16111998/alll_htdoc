<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'departments';
    protected $fillable = [
        'name'
    ];

    public function user()
    {
        return $this->belongsToMany('App\Models\User');
    }
}
