<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class School extends Model
{
	use SoftDeletes;

  protected $table = 'schools';
  protected $guarded =[];
  protected $dates =['deleted_at'];

  public function block()
  {
  	return $this->hasMany('App\Models\Block');
  }
  public function school_detail()
  {
  	return $this->hasOne('App\Models\school_detail');
  }
}
