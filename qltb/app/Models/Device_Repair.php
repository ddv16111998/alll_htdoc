<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Device_Repair extends Model
{
    use SoftDeletes;
    protected $table = 'device_repair';
    protected $guarded=[];
    protected $dates = ['deleted_at'];

    public function device()
    {
        return $this->belongsToMany('App\Models\Device','broken_repair','repair_id')->withPivot('amount','price','status');
    }

    public function broken()
    {
        return $this->belongsTo('App\Models\Broken','broken_id');
    }

    public function broken_repair()
    {
        return $this->hasMany('App\Models\Broken_repair');
    }

    public function school_year()
    {
        return $this->belongsTo('App\Models\SchoolYear','school_year_id');
    }


}
