<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Classes extends Model
{
    use SoftDeletes;

	protected $table = 'classes';
	protected $guarded =[];
	protected $dates =['deleted_at'];

	public function school()
	{
	    return $this->belongsTo('App\Models\School');
	}

	public function block()
	{
	    return $this->belongsTo('App\Models\Block');
	}
    public function deviceBorrow()
    {
        return $this->hasMany('App\Models\DeviceBorrow');
    }
}
