<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class BrokenDevice extends Model
{
  protected $table = 'broken_device';
  protected $guarded =[];
  protected $dates =['deleted_at'];

  public function broken()
  {
  	return $this->belongsTo('App\Models\Broken');
  }

  public function device() {
        return $this->belongsTo('App\Models\Device','device_id');
  }
}
