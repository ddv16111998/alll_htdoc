<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Liquidation extends Model
{
    use SoftDeletes;
    protected  $table ='liquidations';
    protected $guarded =[];
    protected $dates =['deleted_at'];

    public function device()
    {
        return $this->belongsToMany('App\Models\Device','liquidation_device','liquidation_id')->withPivot('amount','note');
    }
}
