<?php

namespace App\Exports;

use App\Models\Devicedown;
use App\Repositories\DevicedownRepository;
use Maatwebsite\Excel\Concerns\FromCollection;

use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;

class DevicedownExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
      $devicedowns = Devicedown::whereNull('deleted_at')->get();
	  	$devicedown[] = array();
	    foreach ($devicedowns as $row) {
	    	$devicedown[] = array(
	    		'0' => '#',
	    		'1' => 'Tên phiếu',
	    		'2' => 'Ngày tạo',
	    	);
	      $devicedown[] = array(
	        '0' => $row->id,
	        '1' => $row->name,
	        '2' => $row->create_date,
	      );
	      $devicedown[] = array('0'=>'Thiết bị', '1'=>'Số lượng');
	      foreach ($row->device()->get() as $device) {
	      	$devicedown[] = array(
	      		'0' => $device->name,
	      		'1' => $device->pivot->amount,
	      		'2' => $device->pivot->note,
	      	);
	      }
	      $devicedown[] = array('0'=>'');
	    }
	    return (collect($devicedown));
    }
}
