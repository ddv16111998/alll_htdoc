<?php

namespace App\Http\Middleware;

use Closure;
use App\Repositories\DiarySystemRepository;

class Diary
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $diary)
    {
        $data['user_id'] = \Auth::user()->id;
        $data['ip']      = $_SERVER['REMOTE_ADDR'];
        $data['action']  = $diary;
        DiarySystemRepository::updateOrCreate($data);
        return $next($request);
    }
}
