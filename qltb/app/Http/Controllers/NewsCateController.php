<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use DB;

class NewsCateController extends Controller
{
    public function index()
    {
        return view('news.category.index');
    }
    // public function anydata()
    // {
    //     $news = DB::table('tk_news')->select(['id', 'title', 'intro']);
    //     return Datatables::of($news)->make(true);
    // }

    public function create(Request $rq)
    {
        
        return view('news.category.create');
    }
}
