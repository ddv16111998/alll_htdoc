<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CategoryRepository;
use App\Repositories\DeviceRepository;
use App\Repositories\DevicedownRepository;
use Yajra\Datatables\Datatables;
use App\Http\Requests\StoreDevicedown;
use App\Repositories\SchoolYearRepository;
use Carbon\Carbon;

use App\Exports\DevicedownExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;

class DevicedownController extends Controller
{
    protected $repositories;

    function __construct(DevicedownRepository $devicedownRepository)
    {
            $this->repositories = $devicedownRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
        $data = $this->repositories->getDataIndex();
        return DataTables::of($data)
        ->addColumn('device', function ($data) {
            $dv = $data->device()->get();
            $ss = null;
            foreach ($dv as $v) {
                $ss .= $v->name . ' ('.$v->pivot->amount.' '.$v->unit.')<br>';
            }
            return $ss;
        })
        ->addColumn('action', function ($data) {
          $string = '
          <a href="'.route('admin.devicedown.edit', $data->id).'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i>Sửa</a>
          <a href="javascript:void(0)" data-href="'.route('admin.devicedown.destroy', $data->id).'" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i>Xóa</a>
          ';
          return $string;
        })
        ->rawColumns(['action', 'device'])
        ->toJson();
        }
        return view('devicedown.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, CategoryRepository $cate, DeviceRepository $device, SchoolYearRepository $school_year)
    {
        if($request->ajax()){
            $param = [
                ['name', 'like', '%'.$request->device_name.'%'],
            ];
            $devices = $this->repositories->getDataSearch($param);
            $string = '';
            foreach ($devices as $device) {
                $string .= '<tr>
                    <td>'.$device->so_hieu.'</td>
                    <td>'.$device->code.'</td>
                    <td>'.$device->name.'</td>
                    <td>'.$device->unit.'</td>
                    <td><button class="btn btn-info select_device" value="'.$device->id.'" data-device="'.$device->name.'" title="chọn">>>></button></td>
                </td>';
            }
            return $string;
        }

        if (isset($request->device)) {
            $from_device = $device->getDataDeviceById($request->device);
        }else{
            $from_device = null;
        }
        
        return view('devicedown.create', ['cates'=>$cate->getDataIndex(), 'devices'=>$device->getDataIndex(), 'school_years'=>$school_year->getDataIndex(), 'from_device' => $from_device]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function saveData($request, $id = null)
    {
        $create_date=  Carbon::createFromFormat('d/m/Y', $request->create_date);
        $data = [
            'name'      => $request->name,
            'create_date' => $create_date->toDateString(),
            'year_link' => $request->start.'-'.$request->end,
            'note' => $request->description,
        ];
        $rel_data = [
            'device_id' => $request->device_id,
            'amount'    => $request->amount,
            'note'    => $request->note,
        ];
        return $this->repositories->updateOrCreate($data, $rel_data, $id);
    }

    public function store(StoreDevicedown $request)
    {
        if ($this->saveData($request)) {
            return redirect()->route('admin.devicedown.index')->with(['message'=>'Thêm mới thành công', 'type'=>'success']);
        }else{
            return redirect()->route('admin.devicedown.index')->with(['message'=>'Thêm mới thất bại', 'type'=>'error']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, DeviceRepository $device, SchoolYearRepository $school_year)
    {
        $devicedown = $this->repositories->getById($id);
        $devices = $devicedown->device()->get();
        return view('devicedown.edit', ['school_years'=>$school_year->getDataIndex(), 'devices_sl' => $device->getDataIndex(), 'devicedown'=>$devicedown, 'devices'=>$devices]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreDevicedown $request, $id)
    {
        if ($this->saveData($request, $id)) {
            return redirect()->route('admin.devicedown.index')->with(['message'=>'Cập nhật thành công', 'type'=>'success']);
        }else{
            return redirect()->route('admin.devicedown.index')->with(['message'=>'Cập nhật thất bại', 'type'=>'error']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repositories->destroy($id);
        return redirect()->route('admin.devicedown.index')->with(['message'=>'Xóa thành công', 'type'=>'success']);
    }

    public function exportExcel()
    {
        return Excel::download(new DevicedownExport, 'Danh sách giảm thiết bị.xlsx');
    }
}
