<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ClassroomRepository;
use App\Repositories\SchoolRepository;
use App\Repositories\SubjectRepository;
use Yajra\DataTables\DataTables;

class ClassroomController extends Controller
{
    protected $repositories;
    function __construct(ClassroomRepository $classroomRepository)
    {
        $this->repositories = $classroomRepository;
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->repositories->getDataIndex();
            return DataTables::of($data)
                ->addColumn('school_id', function ($data) {
                    if($data->school) return $data->school->name;
                    else return '--';
                })
                ->addColumn('subject_id', function ($data) {
                    if($data->subject) return $data->subject->name;
                    else return '--';
                })
                ->addColumn('action', function ($data) {
                    return '<a href="'.route('admin.classroom.edit', $data->id).'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> Edit</a>
                        <a href="javascript:;" data-href="'.route('admin.classroom.destroy', $data->id).'" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i> Delete</a>';
                })
                ->rawColumns(['action','school_id','subject_id'])
                ->make(true);
        }
        return view('classroom.index');
    }


    public function create(SchoolRepository $schoolRepository,SubjectRepository $subjectRepository)
    {
        $data['school'] = $schoolRepository->getDataIndex();
        $data['subject'] = $subjectRepository->getDataIndex();
        return view('classroom.create', $data);
    }

    public function saveClassroom($request, $id = null)
    {
        $name    = $request->name;
        $school_id= $request->school_id;
        $subject_id= $request->subject_id;
        $type    = $request->type;
        $classification    = $request->classification;
        $officers    = $request->officers;
        $acreage    = $request->acreage;
        $is_functional_classroom    = $request->is_functional_classroom;

        $data = [
            'name'    => $name,
            'school_id'=> $school_id,
            'subject_id'    => $subject_id,
            'type'    => $type,
            'classification'    => $classification,
            'officers'    => $officers,
            'acreage'    => $acreage,
            'is_functional_classroom'    => $is_functional_classroom
        ];
        return $this->repositories->updateOrCreate($data,$id);
    }


    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
