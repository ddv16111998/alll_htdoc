<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\SubjectRepository;
use App\Repositories\BlockRepository;
use App\Http\Requests\StoreCreateSubjectPost;
use Yajra\Datatables\Datatables;

class SubjectController extends Controller
{
    protected $repositories;
    function __construct(SubjectRepository $subjectRepository)
    {
        $this->repositories = $subjectRepository;
    }

    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->repositories->getDataIndex();
            return DataTables::of($data)
                ->addColumn('block_id', function ($data) {
                    if($data->block) return $data->block->name;
                    else return '--';
                })
                ->addColumn('action', function ($data) {
                    return '<a href="'.route('admin.subject.edit', $data->id).'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> Edit</a>
                        <a href="javascript:;" data-href="'.route('admin.subject.destroy', $data->id).'" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i> Delete</a>';
                })
                ->rawColumns(['action','block_id'])
                ->make(true);
        }
        return view('subject.index');
    }


    public function create(BlockRepository $blockRepository)
    {
        $data['blocks'] = $blockRepository->getDataIndex();
        return view('subject.create', $data);
    }

    public function saveSubject($request, $id = null)
    {
        $name    = $request->name;
        $block_id= $request->block_id;
        $start_year    = $request->start;
        $end_year    = $request->end;
        $school_year = $start_year . ' - '.$end_year;

        $data = [
            'name'    => $name,
            'block_id'=> $block_id,
            'school_year'    => $school_year,
        ];
        return $this->repositories->updateOrCreate($data,$id);
    }


    public function store(StoreCreateSubjectPost $request)
    {
        $res = $this->saveSubject($request);
        if($res)
        {
            return redirect()->route('admin.subject.index')
                ->with(['message' => 'Thêm mới thành công', 'type' => 'success']);
        }
        else {
            return redirect()->route('admin.subject.index')
                ->with(['message' => 'Thêm mới thất bại', 'type' => 'error']);
        }
    }


    public function show($id)
    {
        //
    }


    public function edit($id, BlockRepository $block)
    {
        $data=[];
        $dataBlock = $this->repositories->getDataEdit($id);
        $block_id= $block->getDataIndex();
        $data['device'] = $dataBlock;
        $data['$block_id'] = $block_id;
        return view('subject.edit',$data);
    }


    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
