<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CategoryRepository;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreCategoryPost;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use Exception;

class CategoryController extends Controller
{
    protected $repositories;

    function __construct(CategoryRepository $categoryRepository)
    {
        $this->repositories = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->repositories->getDataIndex();
            return DataTables::of($data)
            ->addColumn('slug', function ($data) {
                return '<a href="'.url('/').'/'.$data->slug.'">'.url('/').'/'.$data->slug.'</a>';
            })
            ->addColumn('active', function ($data) {
                if($data->active == 1)
                    return '<code class="text-success">Hoạt động</code>';
                if($data->active == 0)
                    return '<code class="text-danger">Ẩn</code>';
            })
            ->addColumn('action', function ($data) {
                $string = '
                <a href="'.route('admin.category.edit', $data->id).'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i>Sửa</a>
                <a href="javascript:void(0)" data-href='.route('admin.category.destroy', $data->id).'" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i>Xóa</a>
                ';
                return $string;
            })
            ->rawColumns(['slug', 'active', 'action'])
            ->toJson();
        }
        return view('category.index');
    }

    /**
     * Show the form for creating a new resource.
     * hiển thị 
     * @param  \App\
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = $this->repositories->getDataIndex();
        return view('category.create', ['data'=>$data]);
    }

    /**
     * Store a newly created resource in storage.
     * xử lý
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Http\Requests\StoreCategoryPost $validator
     * @param  \App\
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCategoryPost $request)
    {
        try {
            if(isset($request->active)){
                $active = 1;
            }else{
                $active = 0;
            }
            $data = [
                'title' => $request->title,
                'slug' => str_slug($request->title),
                'parent_id' => $request->parent_id,
                'active' => $active,
                'created_at' => Carbon::now(),
            ];
            $this->repositories->setCate($data);
            return redirect()->route('admin.category.index')->with(['message' => 'Thêm mới thành công', 'type' => 'success']);
        } catch (Exception $e) {
            return redirect()->route('admin.category.index')->with(['message' => 'Thêm mới thất bại', 'type' => 'error']);
        }
    }

    /**
     * Display the specified resource.
     * hiển thị chi tiêt
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     * giao diện sửa
     * @param  int  $id
     * @param  \App\
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = $this->repositories->getById($id);
        $data = $this->repositories->getDataIndex();
        return view('category.edit', ['cate'=>$category, 'data'=>$data]);
    }

    /**
     * Update the specified resource in storage.
     * xử lý sửa
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @param  \App\
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCategoryPost $request)
    {
        try {
            if(isset($request->active)){
                $active = 1;
            }else{
                $active = 0;
            }
            $data = [
                'title' => $request->title,
                'slug' => str_slug($request->title),
                'parent_id' => $request->parent_id,
                'active' => $active,
                'updated_at' => Carbon::now(),
            ];
            $this->repositories->setUpdate($request->id,$data);
            return redirect()->route('admin.category.index')->with(['message' => 'Cập nhật thành công', 'type' => 'success']);
        } catch (Exception $e) {
            return redirect()->route('admin.category.index')->with(['message' => 'Cập nhật thất bại', 'type' => 'error']);
        }
            
    }

    /**
     * Remove the specified resource from storage.
     * xóa
     * @param  int  $id
     * @param  \App\
     * @return \Illuminate\Http\Response
     */
    public function destroy($parent_id)
    {
        $cate = $this->repositories->getParentId($parent_id);
        foreach ($cate as $list) {
            $this->repositories->destroy($list->id);
        }
        $this->repositories->destroy($parent_id);
        return redirect()->route('admin.category.index')->with(['message' => 'Xóa thành công', 'type' => 'success']);
    }
}
