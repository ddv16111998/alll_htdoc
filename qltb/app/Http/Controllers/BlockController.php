<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests\StoreCreateBlockPost;
use Yajra\Datatables\Datatables;
use App\Repositories\BlockRepository;
use App\Repositories\SchoolRepository;

class BlockController extends Controller
{
		protected $repositories;

		function __construct(BlockRepository $blockRepository)
		{
				$this->repositories = $blockRepository;
		}

		public function index(Request $request)
		{
			if ($request->ajax()) {
				$data = $this->repositories->getDataIndex();
				return DataTables::of($data)
				->editColumn('school_id', function ($data) {
					return $data->school->name;
				})
				->addColumn('count_class', function ($data) {
					return $this->repositories->countClasses($data->id);
				})
				->addColumn('count_std', function ($data) {
					return "--";
				})
				->addColumn('action', function ($data) {
					$string = '
					<a href="'.route('admin.block.edit', $data->id).'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i>Sửa</a>
					<a href="javascript:void(0)" data-href="'.route('admin.block.destroy', $data->id).'" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i>Xóa</a>
					';
					return $string;
				})
				->rawColumns(['action'])
				->toJson();
				}
			return view('block.index');
		}

		public function saveData($request, $id = null)
		{
			$data = [
				'name' => $request->name,
				'code' => $request->code,
				'school_id' => $request->school_id,
			];
			return $this->repositories->updateOrCreate($data, $id);
		}

		/**
		 * Show the form for creating a new resource.
		 *
		 * @return \Illuminate\Http\Response
		 */
		public function create(SchoolRepository $schoolRepository)
		{
				$schools = $schoolRepository->getDataIndex();
				return view('block.create', ['schools'=>$schools]);
		}

		/**
		 * Store a newly created resource in storage.
		 *
		 * @param  \Illuminate\Http\Request  $request
		 * @return \Illuminate\Http\Response
		 */
		public function store(StoreCreateBlockPost $request)
		{
			if ($this->saveData($request)) {
				return redirect()->route('admin.block.index')->with(['message'=>'Thêm mới thành công', 'type'=>'success']);
			}else{
				return redirect()->route('admin.block.index')->with(['message'=>'Thêm mới thất bại', 'type'=>'error']);
			}
		}

		/**
		 * Display the specified resource.
		 *
		 * @param  int  $id
		 * @return \Illuminate\Http\Response
		 */
		public function show($id)
		{
				//
		}

		/**
		 * Show the form for editing the specified resource.
		 *
		 * @param  int  $id
		 * @return \Illuminate\Http\Response
		 */
		public function edit($id, SchoolRepository $schoolRepository)
		{
			$block = $this->repositories->getById($id);
			$schools = $schoolRepository->getDataIndex();
			return view('block.edit', ['block'=>$block, 'schools'=>$schools]);
		}

		/**
		 * Update the specified resource in storage.
		 *
		 * @param  \Illuminate\Http\Request  $request
		 * @param  int  $id
		 * @return \Illuminate\Http\Response
		 */
		public function update(StoreCreateBlockPost $request)
		{
			if ($this->saveData($request, $request->id)) {
				return redirect()->route('admin.block.index')->with(['message'=>'Thêm mới thành công', 'type'=>'success']);
			}else{
				return redirect()->route('admin.block.index')->with(['message'=>'Thêm mới thất bại', 'type'=>'error']);
			}
		}

		/**
		 * Remove the specified resource from storage.
		 *
		 * @param  int  $id
		 * @return \Illuminate\Http\Response
		 */
		public function destroy($id)
		{
			$this->repositories->destroy($id);
			return redirect()->route('admin.block.index')->with(['message'=>'Xóa thành công', 'type'=>'success']);
		}
}
