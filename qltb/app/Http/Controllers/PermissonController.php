<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreCreatePermissonPost;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;
use App\Repositories\PermissonRepository;

class PermissonController extends Controller
{
    protected $repositories;

    function __construct(PermissonRepository $permissonRepository)
    {
        $this->repositories = $permissonRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = $this->repositories->getDataIndex();
            return DataTables::of($data)
            ->addColumn('status', function ($data) {
                if($data->status == 1)
                    return '<code class="text-success">Hoạt động</code>';
                if($data->status == 0)
                    return '<code class="text-danger">Ẩn</code>';
            })
            ->addColumn('action', function ($data) {
                $string = '
                <a href="'.route('admin.permisson.edit', $data->id).'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i>Sửa</a>
                <a href="javascript:void(0)" data-href='.route('admin.permisson.destroy', $data->id).'" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i>Xóa</a>
                ';
                return $string;
            })
            ->rawColumns(['status', 'action'])
            ->toJson();
        }
        return view('permisson.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('permisson.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCreatePermissonPost $request)
    {
        try {
            if(isset($request->status)){
                $status = 1;
            }else{
                $status = 0;
            }
            $permisson = [
                'display_name' => $request->display_name,
                'route' => $request->route_permisson,
                'note' => $request->note,
                'status' => $status,
                'created_at' => Carbon::now(),
            ];
            $this->repositories->setPermisson($permisson);
            return redirect()->route('admin.permisson.index')->with(['message'=>'Thêm mới thành công', 'type'=>'success']);
        } catch (Exception $e) {
            return redirect()->route('admin.permisson.index')->with(['message'=>'Thêm mới thất bại', 'type'=>'error']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\permisson  $permisson
     * @return \Illuminate\Http\Response
     */
    public function show(permisson $permisson)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\permisson  $permisson
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $permisson = $this->repositories->getById($id);
        return view('permisson.edit', ['permisson'=>$permisson]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\permisson  $permisson
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCreatePermissonPost $request)
    {
        try {
            if(isset($request->status)){
                $status = 1;
            }else{
                $status = 0;
            }
            $permisson = [
                'display_name' => $request->display_name,
                'route' => $request->route_permisson,
                'note' => $request->note,
                'status' => $status,
                'updated_at' => Carbon::now(),
            ];
            $this->repositories->setUpdate($request->id, $permisson);
            return redirect()->route('admin.permisson.index')->with(['message'=>'Cập nhật thành công', 'type'=>'success']);
        } catch (Exception $e) {
            return redirect()->route('admin.permisson.index')->with(['message'=>'Cập nhật thất bại', 'type'=>'error']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\permisson  $permisson
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repositories->destroy($id);
        return redirect()->route('admin.permisson.index')->with(['message'=>'Xóa thành công', 'type'=>'success']);
    }
}
