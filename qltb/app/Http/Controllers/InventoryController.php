<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Yajra\Datatables\Datatables;

use App\Repositories\SchoolYearRepository;
use App\Repositories\DeviceRepository;
use App\Repositories\BrokenDeviceRepository;
use App\Repositories\InventoryRepository;
use App\Http\Requests\StoreAddInventoryPost;

use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class InventoryController extends Controller
{
    protected $repository;
    private $stt = 1;
    function __construct(InventoryRepository $inventory)
    {
        $this->repository = $inventory;

    }

    public function index(Request $request)
    {
        if($request->ajax())
        {
            $data = $this->repository->getDataIndex();
            return Datatables::of($data)
                ->addColumn('stt',function ($data){
                    return $this->stt++;
                })
                ->editColumn('device',function ($data){
                    $inventory = $this->repository->getDataEdit($data->id);
                    $devices = $inventory->device()->get();
                    $ss = null;
                    foreach ($devices as $val)
                    {
                        $ss .= $val->name." ( hỏng ".$val->pivot->amount_broken." ".$val->unit." )"."( mất ".$val->pivot->amount_lost." ".$val->unit." )<br/>";
                    }
                    return $ss;
                })
                ->addColumn('action', function ($data) {
                    $string = '
                  <a href="'.route('admin.inventory.edit',$data->id).'" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i>Sửa</a>
                  <a href="javascript:void(0)" data-href="'.route('admin.inventory.destroy',$data->id).'" class="btn btn-xs btn-danger btn-delete"><i class="fa fa-times"></i>Xóa</a>
                  <a href="'.route('admin.inventory.newBroken',$data->id).'" class="btn btn-xs btn-success mt-1"><i class="fa fa-edit"></i>Phiếu hỏng mất</a>
                  ';
                    return $string;
                })
                ->rawColumns(['action', 'device','stt'])
                ->toJson();
        }
        return view('inventory.index');
    }
    public function create(SchoolYearRepository $school_year,DeviceRepository $device,Request $request,BrokenDeviceRepository $brDevice)
    {
        if($request->ajax())
        {
            if($request->device_name)
            {
                $param = [
                    ['name', 'like', '%'.$request->device_name.'%'],
                ];
                $devices = $this->repository->getDataSearch($param);
                $string = '';
                foreach ($devices as $device_one) {
                    $string .= '<tr>
                    <td>'.$device_one->so_hieu.'</td>
                    <td>'.$device_one->code.'</td>
                    <td>'.$device_one->name.'</td>
                    <td>'.$device_one->unit.'</td>
                    <td><button class="btn btn-info select_device" value="'.$device_one->id.'" data-device="'.$device_one->name.'" title="chọn">>>></button></td>
                </tr>';
                }
                return $string;
            }
            if($request->typeData == 0)
            {
                $brokenDevices = $brDevice->getIndex();
                $string ='';
                foreach ($brokenDevices as $brokneDevive)
                {
                    $string .= '<tr>
                <td>'.$brokneDevive->device->so_hieu.'</td>
                <td>'.$brokneDevive->device->code.'</td>
                <td>'.$brokneDevive->device->name.'</td>
                <td>'.$brokneDevive->device->unit.'</td>
                <td><button class="btn btn-info select_device" value="'.$brokneDevive->device_id.'" data-device="'.$brokneDevive->device->name.'" title="chọn">>>></button></td>
            </tr>';
                }
                return $string;
            }
            if($request->typeData == 1)
            {
                // show ra các thiết bị còn sử dụng được
                return 1;
            }
        }
        $school_years = $school_year->getDataIndex();
        $devices      = $device->getDataIndex();
        $data['school_years'] = $school_years;
        $data['devices'] = $devices;
        return view('inventory.create',$data);
    }

    public function save(Request $request,$id)
    {
        $name = $request->name;
        $school_year_id = $request->school_year_id;
        $create_date = Carbon::createFromFormat('d/m/Y', $request->create_date);
        $inventory_date = Carbon::createFromFormat('d/m/Y', $request->inventory_date);
        $inventory_peoples = $request->inventory_peoples;
        $device_id = $request->device_id;
        $amount_broken = $request->amount_broken;
        $amount_lost = $request->amount_lost;

        $data = [
            'name' => $name,
            'school_year_id' => $school_year_id,
            'create_date'    => $create_date->toDateString(),
            'inventory_date'        => $inventory_date,
            'inventory_peoples'    => $inventory_peoples,
        ];
        $param = [
            'device_id' => $device_id,
            'amount_broken'    => $amount_broken,
            'amount_lost'      => $amount_lost
        ];
        return $this->repository->updateOrCreate($data,$param,$id);
    }
    public function store(StoreAddInventoryPost $request,$id=null)
    {
        $data = $this->save($request,$id);
        if($data)
        {
            return redirect()->route('admin.inventory.index')->with(['message'=>'Thêm mới thành công', 'type'=>'success']);
        }
        return redirect()->route('admin.inventory.index')->with(['message'=>'Thêm mới thất bại', 'type'=>'error']);
    }
    public function edit($id,SchoolYearRepository $school_year)
    {
        $inventory = $this->repository->getDataEdit($id);
        $devices = $inventory->device()->get();
        $school_years = $school_year->getDataIndex();
        $data['inventory'] = $inventory;
        $data['devices'] = $devices;
        $data['school_years'] = $school_years;
        return view('inventory.edit',$data);
    }
    public function update($id,StoreAddInventoryPost $request)
    {
        $data = $this->save($request,$id);
        if($data)
        {
            return redirect()->route('admin.inventory.index')->with(['message'=>'Cập nhật thành công', 'type'=>'success']);
        }
        return redirect()->route('admin.inventory.index')->with(['message'=>'Cập nhật thất bại', 'type'=>'error']);
    }
    public function destroy($id)
    {
        $delete = $this->repository->destroy($id);
        if($delete)
        {
            return redirect()->route('admin.inventory.index')->with(['message'=>'Xóa thành công', 'type'=>'success']);
        }
        return redirect()->route('admin.inventory.index')->with(['message'=>'Xóa thất bại', 'type'=>'error']);
    }
    public function newBroken($id,DeviceRepository $device)
    {
        $inventory = $this->repository->getDataEdit($id);
        $devices = $device->getDataIndex();
        $data['abc'] = $inventory->device()->get();
        $data['devices'] = $devices;
        return view('broken.create',$data);

    }
}
