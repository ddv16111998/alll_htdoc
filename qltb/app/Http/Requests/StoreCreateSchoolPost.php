<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class StoreCreateSchoolPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        $validate = [
            'name' => 'required|regex:/^[^\!\@\#\$\%\^\&\*\(\)\-\=\>\<\.\,\?\/\[\]\{\}]{2,}$/',
            'address' => 'required',
            'hotline' => 'regex:/^[0-9]{6,15}$/',
            'email' => 'required|email',
            'principal_name' => 'required|regex:/^\D{2,50}[^\!\@\#\$\%\^\&\*\(\)\-\=\>\<\.\,\?\/\[\]\{\}]$/',
            'city' => 'required',
            'district' => 'required',
            'commune' => 'required',
            'year_link' => 'required',
        ];
        if (!empty($request->principal_email)) {
            $validate += [
                'principal_email' => 'email',
            ];
        }
        if (!empty($request->vice_principal_name)) {
            $validate += [
                'vice_principal_name' => 'regex:/^\D{2,50}[^\!\@\#\$\%\^\&\*\(\)\-\=\>\<\.\,\?\/\[\]\{\}]$/',
            ];
        }
        if (!empty($request->vice_principal_email)) {
            $validate += [
                'vice_principal_email' => 'email',
            ];
        }
        if (!empty($request->vice_principal_phone)) {
            $validate += [
                'vice_principal_phone' => 'regex:/^\d{8,11}$/',
            ];
        }
        if (!empty($request->principal_phone)) {
            $validate += [
                'principal_phone' => 'regex:/^\d{8,11}$/',
            ];
        }
        return $validate;
    }

    public function messages()
    {
        return [
            'name.required' => 'Tên trường không được để trống',
            'name.regex' => 'Tên trường không chứa kí tự đặc biệt',
            'address.required' => 'Địa chỉ không được để trống',
            'email.required' => 'Email trường không được để trống',
            'email.email' => 'Email không hợp lệ',
            'hotline.regex' => 'Hotline không đúng định dạng',
            'principal_name.required' => 'Hiệu trưởng không được để trống',
            'principal_name.regex' => 'Tên không đúng',
            'principal_email.email' => 'Email không hợp lệ',
            'vice_principal_name.regex' => 'Tên không đúng',
            'vice_principal_email.email' => 'Email không hợp lệ',
            'principal_phone.regex' => 'Số điện thoại không hợp lệ',
            'vice_principal_phone.regex' => 'Số điện thoại không hợp lệ',
            'city.required' => 'Chưa chọn thành phố',
            'district.required' => 'Chưa chọn huyện',
            'commune.required' => 'Chưa chọn xã',
            'year_link.required' => 'Năm học không được để trống',
        ];
    }
}
