<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCreateSubjectPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'   => 'required',
            'block_id' =>'required',
            'start'   => 'required',
            'end'   => 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' =>'Bắt buộc phải nhập tên môn học',
            'block_id.required'=>'Bắt buộc phải chọn khối học',
            'start.required' =>'Bắt buộc phải nhập năm học',
            'end.required' =>'Bắt buộc phải nhập năm học',
        ];
    }
}
