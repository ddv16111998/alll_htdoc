<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRegistrationBorrowPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'time_borrow' =>'required',
            'time_return' =>'required',
            'teacher'     =>'required',
            'device_id'   =>'required',
            'subject_id'  =>'required',
            'school_year_id'=>  'required',
            'school_id'   => 'required',
            'block_id'    => 'required',
            'classes_id'  =>'required',
            'lesson_name' =>'required',
            'lesson_use'  =>'required',
        ];
    }
    public function messages()
    {
        return [
            'time_borrow.required' =>'Bắt buộc phải chọn ngày mượn',
            'time_return.required' =>'Bắt buộc phải chọn ngày trả',
            'teacher.required'     =>'Bắt buộc phải nhập giáo viên',
            'device_id.required'   =>'Bắt buộc phải chọn tên thiết bị',
            'subject_id.required'  =>'Bắt buộc phải nhập môn học',
            'school_year_id.required'       =>'Bắt buộc phải chọn năm học',
            'classes_id.required'       =>'Bắt buộc phải chọn lớp',
            'school_id.required'       =>'Bắt buộc phải chọn khối',
            'block_id.required'       =>'Bắt buộc phải chọn trường',
            'lesson_name.required' =>'Bắt buộc phải nhập tên bài dạy',
            'lesson_use.required' =>'Bắt buộc phải nhập số tiết dạy',
        ];
    }
}
