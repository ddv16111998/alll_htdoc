<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAddRepairDevicePost extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ballot' =>'required',
            'start_day'     =>'required',
            'school_year_id'         =>'required',
            'broken_id'     =>'required',
            'state'         => 'required'

        ];
    }

    public function messages()
    {
        return [
            'ballot.required'=>'Số phiếu không được để trống!',
            'start_day.required'    =>'Ngày lập không được để trống!',
            'school_year_id.required'        =>'Năm học không được để trống!',
            'broken_id.required'    =>'Tên phiếu không được để trống!',
            'state.required'    =>'Tình trạng hỏng hóc không được để trống'
        ];
    }
}
