<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAddInventoryPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'school_year_id'=> 'required',
            'create_date'=> 'required',
            'inventory_date'=> 'required',
            'inventory_peoples'=> 'required',
            'device_id'=> 'required',
            'amount_broken'=> 'required',
            'amount_lost'=> 'required',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Số phiếu không được để trống',
            'school_year_id.required' => 'Năm học không được để trống',
            'create_date.required' => 'Ngày lập phiếu không được để trống',
            'inventory_date.required' => 'Ngày kiếm kê không được để trống',
            'inventory_peoples.required' => 'Hội đồng không được để trống',
            'device_id.required' => 'Thiết bị không được để trống',
            'amount_broken.required' => 'Số lượng hỏng không được để trống',
            'amount_lost.required' => 'Số lượng mất không được để trống',
        ];

    }
}
