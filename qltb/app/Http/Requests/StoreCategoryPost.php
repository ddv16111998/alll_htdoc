<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCategoryPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:50',
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Không được để trống danh mục!',
            'title.max' => 'Tên danh mục không được vượt quá 50 ký tự!',
        ];
    }
}
