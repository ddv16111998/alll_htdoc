<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAddSchoolYearPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start' => 'required',
            'end' => 'required',
            'start_HK1' => 'required',
            'start_HK2' => 'required',
            'end_year' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'start.required' =>'Bắt buộc phải chọn năm học bắt đầu',
            'end.required' =>'Bắt buộc phải chọn năm học kết thúc',
            'start_HK1.required' =>'Bắt buộc phải chọn ngày bắt đầu học kì 1',
            'start_HK2.required' =>'Bắt buộc phải chọn ngày bắt đầu học kì 2',
            'end_year.required' =>'Bắt buộc phải chọn ngày kết thúc năm học',
        ];

    }
}
