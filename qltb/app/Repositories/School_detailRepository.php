<?php

namespace App\Repositories;

use App\Models\School_detail;
use App\Models\Schools;

class School_detailRepository
{
    public function getDataIndex()
    {
       return School_detail::all();
    }

    public function getDataCreate()
    {
       $data['category'] = School_detail::all();
        return $data;
    }

    public function updateOrCreate($data, $id = null) {
        $school_detail = School_detail::updateOrCreate([
            'id' =>$id,
         ],$data);
         if($school_detail->id)
         {
            return true;
         }
         return false;
    }

    public function getDataEdit($id)
    {
        return School_detail::where('school_id', $id)->get();
    }

    public function destroy($id)
    {
        
    }
}