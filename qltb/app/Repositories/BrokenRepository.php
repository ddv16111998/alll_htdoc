<?php

namespace App\Repositories;

use App\Models\Broken;
use App\Models\Device;
use App\Models\BrokenDevice;

class BrokenRepository
{
    public function getDataIndex()
    {
       return Broken::whereNull('deleted_at')->get();
    }

    public function getById($id)
    {
        return Broken::find($id);
    }

    public function getDataCreate()
    {

    }

    public function updateOrCreate($data, $rel_data = null, $id = null) {
        $broken = Broken::updateOrCreate(['id' =>$id,],$data);
        if(!empty($id)){

            $broken->Device()->detach();
        }

        if($broken->id){
            foreach ($rel_data['device_id'] as $k => $v) {
                $broken->Device()->attach($broken->id, [
                    'device_id' => $v,
                    'amount' => $rel_data['amount'][$k],
                    'status' => $rel_data['status'][$k]
                ]);
            }
            return true;
        }
        return false;
    }

    public function getDataEdit($id)
    {
        $broken = Broken::select()->findOrFail($id);
        return $broken->toArray();
    }

    public function destroy($id)
    {
        $delete = Broken::find($id);
        $delete->delete();
        $delete->Device()->detach();
        if($delete->trashed())
        {
            return true;
        }
        return false;
    }


}
