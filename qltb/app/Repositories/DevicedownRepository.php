<?php

namespace App\Repositories;

use App\Models\Devicedown;
use App\Models\Device;

class DevicedownRepository
{
    public function getDataIndex()
    {
       return Devicedown::whereNull('deleted_at')->orderBy('id', 'desc')->get();
    }

    public function getDataCreate()
    {

    }

    public function getById($id)
    {
        return Devicedown::find($id);
    }

    public function getDataSearch($param)
    {
        return Device::whereNull('deleted_at')->where($param)->get();
    }

    public function updateOrCreate($data, $rel_data = null, $id = null) {
        $devicedown = Devicedown::updateOrCreate(['id' =>$id,],$data);
        if(!empty($id)){
            $devices = $devicedown->device()->get();
            foreach ($devices as $device) {
                $amount = Device::find($device->id);
                $device_amount = [
                    'amount' => $amount->amount + $device->pivot->amount,
                ];
                Device::updateOrCreate(['id'=>$device->id], $device_amount);
            }
            $devicedown->Device()->detach();
        }

        if($devicedown->id){
            foreach ($rel_data['device_id'] as $k => $v) {
                $amount = Device::find($v);
                if ($amount->amount-$rel_data['amount'][$k]>=0) {
                    $sl = $amount->amount-$rel_data['amount'][$k];
                }else{
                    $sl=0;
                }
                $device_amount = [
                    'amount' => $sl,
                ];
                Device::updateOrCreate(['id'=>$v], $device_amount);
                $devicedown->Device()->attach($devicedown->id, [
                    'device_id' => $v,
                    'amount' => $rel_data['amount'][$k],
                    'note' => $rel_data['note'][$k]
                ]);
            }
            return true;
        }
        return false;
    }

    public function getDataEdit($id)
    {
        $devicedown = Devicedown::select()->findOrFail($id);
        return $devicedown->toArray();
    }

    public function destroy($id)
    {
        $delete = Devicedown::find($id);
        $delete->delete();
        if($delete->trashed())
        {
            return true;
        }
        return false;
    }
}
