<?php

namespace App\Repositories;

use App\Models\Classes;
use App\Models\School;
use App\Models\Block;
use App\Models\School_detail;

class SchoolRepository
{
    public function getDataIndex()
    {
       return School::whereNull('deleted_at')->get();
    }

    public function getById($id)
    {
        return School::find($id);
    }

    public function setSchool($school)
    {
        return School::insert($school);
    }

    public function setUpdate($id, $data)
    {
        $school = School::find($id);
        foreach ($data as $key => $value) {
          $school->$key = $value;
        }
        return $school->save();
    }

    public function getDataCreate()
    {

    }

    public function updateOrCreate($data, $id = null) {
        $school = School::updateOrCreate([
            'id' =>$id,
         ],$data);
         if($school->id)
         {
            return $school->id;
         }
         return false;
    }

    public function getDataEdit($id)
    {

    }

    public function destroy($id)
    {
        try {
            Block::where(['school_id'=>$id])->delete();
            School::find($id)->delete();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    public function getBlockBySchool($school_id)
    {
        return Block::where('school_id',$school_id)->get();
    }
    public function getClassByBlock($block_id)
    {
        return Classes::where('block_id', $block_id)->get();
    }
}
