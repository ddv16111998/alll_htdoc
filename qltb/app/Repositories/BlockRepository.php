<?php

namespace App\Repositories;

use App\Models\Classes;
use App\Models\Block;
use App\Models\School;

class BlockRepository
{
    public function getDataIndex()
    {
       return Block::whereNull('deleted_at')->get();
    }

    public function getById($id)
    {
        return Block::find($id);
    }

    public function getDataSchool()
    {
        
    }

    public function getDataCreate()
    {
        
    }

    public function updateOrCreate($data, $id = null) {
        $block = Block::updateOrCreate(['id' =>$id,],$data);
        if($block->id){
            return true;
        }
        return false;
    }

    public function getDataEdit($id)
    {
        $block = Block::select()->findOrFail($id);
        return $block->toArray();
    }

    public function destroy($id)
    {
        $delete = Block::find($id);
        $delete->delete();
        if($delete->trashed())
        {
            return true;
        }
        return false;
    }

    public function countClasses($block_id)
    {
        return Classes::where('block_id', $block_id)->get()->count();
    }
}