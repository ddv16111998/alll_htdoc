<?php

namespace App\Repositories;

use App\Models\DiarySystem;

class DiarySystemRepository
{
    public function getDataIndex()
    {
       return DiarySystem::orderby('created_at','desc')->get();
    }

    public function getDataCreate()
    {
       
    }

    public static function create($log, $user_id) {
        $data['user_id'] = $user_id;
        $data['ip']      = $_SERVER['REMOTE_ADDR'];
        $data['action']  = $log;
        DiarySystem::create($data);
    }

    public function getDataEdit($id)
    {

    }

    public function destroy($id)
    {
        
    }
}