@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title', 'Danh sách môn học')
@section('css')

@endsection
@section('js')
    <script>
        $(function() {
            $('#listUsers').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{{ route('admin.subject.index') }}',
                columns: [
                    { data: 'name', name: 'name' },
                    {data : 'block_id', name: 'block_id'},
                    { data: 'school_year', name: 'school_year' },
                    { data: 'action', name : 'action',orderable: false },
                ],
                language : {
                    "emptyTable":     "Không có dữ liệu trong bảng",
                    "info":           "Hiển thị _START_ đến _END_ của _TOTAL_ mục",
                    "infoEmpty":      "Hiển thị 0 đến 0 của 0 mục",
                    "infoFiltered":   " (Được lọc từ _MAX_ mục)",
                    "thousands":      ",",
                    "lengthMenu":     "Hiển thị &nbsp; _MENU_ &nbsp; mục",
                    "loadingRecords": "Chờ xíu nhé...",
                    "processing":     "Đang xử lí...",
                    "search":         "Tìm kiếm:",
                    "zeroRecords":    "Không tìm thấy kết quả",
                    "paginate": {
                        "first":      "Đầu",
                        "last":       "Cuối",
                        "next":       "Tiếp",
                        "previous":   "Trước"
                    }
                }
            });
        });
    </script>
@endsection
@section('content')
    <div id="list-crud">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">
                        Danh sách Môn học
                    </h4>
                </div>
            </div>
{{--            @php dd($data); @endphp--}}
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item active"><a href="">@lang('system.manage') Môn học </a></li>
                    </ol>
                    <a class="btn btn-secondary d-none d-lg-block m-l-15" href="{{ route('admin.subject.create') }}"><i class="fa fa-plus-circle"></i> @lang('system.create_new')</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div>
                                    <table class="table table-hover mt-4" id="listUsers">
                                        <thead>
                                        <th>Tên môn học</th>
                                        <th>Khối học</th>
                                        <th>Năm học</th>
                                        <th>Action</th>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
