@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title') @lang('system.create_new') Phòng học @endsection
@section('css')
@endsection
@section('js')

@endsection
@section('content')
    <div class="container-fluid-width">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">@lang('system.create_new') Phòng học</h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.classroom.index') }}">@lang('system.manage') Phòng học</a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('admin.classroom.create') }}">Thêm mới Phòng học</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <form class="form form-submit" method="POST" action="{{ route('admin.classroom.store') }}">
                <div id="main-content">
                    <div class="left-form">
                        <div class="card">
                            <div class="card-body">
                                @csrf
                                <div class="form-group row justify-content-md-center">
                                    <label for="name" class="col-md-2 text-center col-form-label">Tên phòng học <span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <input name="name" value="{{ old('name') }}" id="name" type="text" class="form-control">
                                        @error('name')
                                        <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row justify-content-md-center">
                                    <label for="school_id" class="col-md-2 text-center col-form-label">Trường học<span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <select name="school_id" id="school_id" class="form-control">
                                            <option value="">---None---</option>
                                            @foreach ($school as $val)
                                                <option value="{{ $val->id }}">{{ $val->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('school_id')
                                        <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row justify-content-md-center">
                                    <label for="subject_id" class="col-md-2 text-center col-form-label">Môn học<span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <select name="subject_id" id="subject_id" class="form-control">
                                            <option value="">---None---</option>
                                            @foreach ($subject as $val)
                                                <option value="{{ $val->id }}">{{ $val->name }}</option>
                                            @endforeach
                                        </select>
                                        @error('subject_id')
                                        <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row justify-content-md-center">
                                    <label for="type" class="col-md-2 text-center col-form-label">Loại phòng TN<span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <select name="type" id="type" class="form-control">
                                            <option value="">---None---</option>
                                            <option value="1">Phòng bộ môn</option>
                                        </select>
                                        @error('type')
                                        <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row justify-content-md-center">
                                    <label for="classification" class="col-md-2 text-center col-form-label">Xếp loại phòng TN<span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <select name="classification" id="classification" class="form-control">
                                            <option value="">---None---</option>
                                            <option value="1">Đạt chuẩn</option>
                                        </select>
                                        @error('classification')
                                        <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row justify-content-md-center">
                                    <label for="officers" class="col-md-2 text-center col-form-label">Giáo viên phụ trách<span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <select name="officers" id="officers" class="form-control">
                                            <option value="">---None---</option>
                                            <option value="1">Nguyễn Bá Bình</option>
                                        </select>
                                        @error('officers')
                                        <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row justify-content-md-center">
                                    <label for="acreage" class="col-md-2 text-center col-form-label">Diện tích <span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <input name="acreage" value="{{ old('acreage') }}" type="text" class="form-control">
                                        @error('acreage')
                                        <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row justify-content-md-center">
                                    <label for="is_functional_classroom" class="col-md-2 text-center col-form-label">Là phòng học chức năng </label>
                                    <div class="col-md-5">
                                        <input name="is_functional_classroom" value="1" type="checkbox">
                                    </div>
                                </div>

                                <section id="form-footer" class="text-right">
                                    <div id="form-actions">
                                        <a href="{{ route('admin.classroom.index') }}" class="btn btn-secondary mr-1">@lang('system.cancel')</a>
                                        <input type="submit" class="btn btn-info" value="@lang('system.save')">
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
