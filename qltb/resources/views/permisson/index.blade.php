@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title')Quyền @endsection
@section('css')
@endsection
@section('js')
    <script>
    $(function() {
        $('#listPermission').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('admin.permisson.index') !!}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'display_name', name: 'display_name' },
                { data: 'route', name: 'route'},
                { data: 'note', name: 'note'},
                { data: 'status', name: 'status'},
                { data: 'created_at', name: 'created_at'},
                { data: 'action', name: 'action'}
            ]
        }); 
    });
    </script>
@endsection
@section('content')
	<div id="list-crud">
    <div class="row page-titles">
        <div class="col-xs-6 col-md-7 align-self-center pr-0">
            <div class="d-flex flex-row-start">
                <h4 class="card-title">
                    Quản lý quyền
                </h4>
            </div>
        </div>
        <div class="col-xs-6 col-md-5 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">@lang('system.dashboard')</a></li>
                    <li class="breadcrumb-item active"><a href="">quyền</a></li>
                </ol>
                <a class="btn btn-secondary d-none d-lg-block m-l-15" href="{{ route('admin.permisson.create') }}"><i class="fa fa-plus-circle"></i> @lang('system.create_new')</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="form-list-crud" action="/" method="POST">
                                @csrf
                                <table id="listPermission"class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <td>ID</td>
                                        <th>Tên quyền</th>
                                        <th>Route</th>
                                        <th>Ghi chú</th>
                                        <th>Trạng thái</th>
                                        <th>Ngày tạo</th>
                                        <th>Hành động</th>
                                    </tr>
                                    </thead>
                                </table>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
