@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title') @lang('system.create_new') chuyên mục bài viết @endsection
@section('css')
@endsection
@section('js')
@endsection
@section('content')
    <div class="container-fluid-width">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">@lang('system.create_new') chuyên mục</h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{route('admin.home')}}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item active"><a href="">@lang('system.manage') chuyên mục bài viết</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <form class="form form-submit" method="POST" action="">
                <div id="main-content">
                    <div class="left-form">
                        <div class="card">
                            <div class="card-body">

                                @csrf
                                <div class="form-group row justify-content-md-center">
                                    <label for="example-text-input" class="col-md-2 text-center col-form-label">Tiêu đề <span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        <input class="form-control " type="text" id="" name="title" value="{{ old('title') }}">
                                        {{-- @error('name') --}}
                                    </div>
                                </div>
                                <div class="form-group row justify-content-md-center">
                                    <label for="example-text-input" class="col-md-2 text-center col-form-label">Danh mục cha <span class="text-danger">*</span></label>
                                    <div class="col-md-5">
                                        {{-- <input class="form-control " type="text" id="" name="parent_id" value="{{ old('name') }}"> --}}
                                        <select name="parent_id" id="" class="form-control">
                                            <option value="0">-- Danh mục gốc --</option>
                                        </select>
                                        {{-- @error('name') --}}
                                    </div>
                                </div>

                                <section class="text-right">
                                    <div class="row justify-content-around">
                                        <div class="col-md-7 col-sm-8">
                                        <a href="" class="btn btn-secondary mr-1">@lang('system.cancel')</a>
                                        <input type="submit" class="btn btn-info" value="@lang('system.save')">
                                        </div>
                                    </div>
                                </section>

                                <!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalScrollable">
                                    Launch demo modal
                                </button>
                                
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-scrollable" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalScrollableTitle">Modal title</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        </div>
                                        <div class="modal-body">
                                        ...
                                        </div>
                                        <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Save changes</button>
                                        </div>
                                    </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                
            </form>
        </div>
    </div>
@endsection
