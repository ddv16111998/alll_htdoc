
@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title', 'Cập nhật sửa chữa thiết bị')
@section('css')
@endsection
@section('js')
    <script>
        $('#start').datepicker({
            format: "yyyy",
            autoclose: true,
            minViewMode: "years"
        }).on('changeDate', function(selected){
            startDate =  $("#start").val();
            $('#end').datepicker('setStartDate', startDate);
        }); ;

        $('#end').datepicker({
            format: "yyyy",
            autoclose: true,
            minViewMode: "years"
        });


        $('#broken_id').change(function(event) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                }
            });
            $.ajax({
                url: "{{ route('admin.repair.edit',$repair->id) }}",
                data: {broken_id: $('#broken_id').val()},
            })
                .done(function(data) {
                    $('#content_load_ajax').html(data);
                });
        });
    </script>

@endsection
@section('content')
    <div class="container-fluid-width">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">Cập nhật sửa chữa thiết bị</h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.devices.index') }}">Quản lý thiết bị</a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('admin.repair.edit',$repair->id) }}">Cập nhật phiếu sữa chữa</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <form class="form form-submit" method="POST" action="{{ route('admin.repair.update',$repair->id) }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div id="main-content">
                    <div class="left-form">
                        <div class="card">
                            <div class="card-body">
                                <div class="content">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Số phiếu<span class="text-danger">*</span></label>
                                                <input type="text" name="ballot" class="form-control form-control-line" value="{{$repair->ballot}}" >
                                            </div>
                                            @error('ballot')
                                            <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                            <div class="form-group">
                                                @php
                                                    $start_day = explode('-',$repair->start_day);
                                                @endphp
                                                <label>Ngày lập<span class="text-danger">*</span></label>
                                                <input type="text" id="start_day" name="start_day" autocomplete="off" class="form-control form-control-line datepicker"  value="{{ $start_day[2].'/'.$start_day[1].'/'.$start_day[0] }}">
                                            </div>
                                            @error('start_day')
                                            <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                            <div class="form-group">
                                                <label for="school_year" class="">Năm học<span class="text-danger">*</span></label>
                                                <select name="school_year_id" id="school_year_id" class="form-control form-control-line">
                                                    <option value="">-- Chọn năm học --</option>
                                                    @foreach ($school_years as $school_year)
                                                        <option value="{{ $school_year->id }}" @if($repair['school_year_id']==$school_year->id) selected @endif>{{ $school_year->school_year }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @error('school_year_id')
                                            <div class="text-danger">{{ $message }}</div>
                                            @enderror

                                            <div class="form-group">
                                                <label>Chọn phiếu<span class="text-danger">*</span></label>
                                                <select name="broken_id" id="broken_id" class="form-control form-control-line">
                                                    <option value="">-- None --</option>
                                                    @foreach ($brokens as $broken)
                                                        <option value="{{ $broken->id }}" @if($repair['broken_id']== $broken->id) selected @endif>{{ $broken->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group">
                                                <label>Tình trạng hỏng hóc<span class="text-danger">*</span></label>
                                                <input type="text" name="state" class="form-control form-check-line" value="{{$repair->state}}">
                                            </div>


                                        </div>

                                        <div class="col-sm-6">
                                            <div id="content_load_ajax">
                                                <label for="">Thiết bị cần sửa chữa</label>
                                                @foreach($devices as $device)
                                                    <div class="row form-group">
                                                        <div class="col-4">
                                                            <select class="device-ajax form-control" name="device_id[]">
                                                                <option value="{{$device->id}}">{{$device->name}}</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-4">
                                                            <input type="number" name="amount[]" class="form-control" value="{{$device->pivot->amount}}" placeholder="Số lượng"/>
                                                        </div>
                                                        <div class="col-3">
                                                            <input type="text" name="price[]" class="form-control" placeholder="Giá tiền sửa/thiết bị" value="{{$device->pivot->price}}"/>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="">Ghi chú</label>
                                                <textarea  name="desciption" class="form-control form-control-line ckeditor">
                                                    {{$repair->desciption}}
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <section id="form-footer" class="text-right">
                                    <div id="form-actions">
                                        <a href="{{ route('admin.repair.index') }}" class="btn btn-secondary mr-1">@lang('system.cancel')</a>
                                        <input type="submit" class="btn btn-info" value="@lang('system.save')">
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
