@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title', 'Danh sách sửa chữa thiết bị')
@section('css')

@endsection
@section('js')
     <script>
    $(function() {
        $('#listUsers').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin.repair.index') }}',
            columns: [
                { data: 'stt', name: 'stt' },
                { data: 'ballot', name: 'ballot' },
                { data: 'start_day', name: 'start_day' },
                {data : 'school_year_id', name: 'school_year_id'},
                { data: 'broken_id', name: 'broken_id' },
                { data: 'device', name: 'device' },
                { data: 'state', name: 'state' },
                { data: 'desciption', name: 'desciption' },
                { data: 'action', name : 'action',orderable: false },
            ],
            language : {
                "emptyTable":     "Không có dữ liệu trong bảng",
                "info":           "Hiển thị _START_ đến _END_ của _TOTAL_ mục",
                "infoEmpty":      "Hiển thị 0 đến 0 của 0 mục",
                "infoFiltered":   " (Được lọc từ _MAX_ mục)",
                "thousands":      ",",
                "lengthMenu":     "Hiển thị &nbsp; _MENU_ &nbsp; mục",
                "loadingRecords": "Chờ xíu nhé...",
                "processing":     "Đang xử lí...",
                "search":         "Tìm kiếm:",
                "zeroRecords":    "Không tìm thấy kết quả",
                "paginate": {
                    "first":      "Đầu",
                    "last":       "Cuối",
                    "next":       "Tiếp",
                    "previous":   "Trước"
                }
            }
        });
    });
    </script>
@endsection
@section('content')
    @include('layouts.nav-tabs')
    <div id="list-crud">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">
                        Danh sách sửa chữa thiết bị
                    </h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item active"><a href="{{route('admin.devices.index')}}">@lang('system.manage') thiết bị </a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('admin.repair.index') }}">Danh sách sửa chữa thiết bị </a></li>
                    </ol>
                    <a class="btn btn-secondary d-none d-lg-block m-l-15" href="{{ route('admin.repair.create') }}"><i class="fa fa-plus-circle"></i> @lang('system.create_new')</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div>
                                    <div class="row text-right"><a  href="{{ route('admin.repair.track_book') }}" target=_self accesskey="x" class="btn btn-outline-success">Theo dõi sổ thiết bị sửa chữa</a></div>
                                    <table class="table table-hover mt-4" id="listUsers">
                                        <thead>
                                            <th>Stt</th>
                                            <th>Số phiếu</th>
                                            <th>Ngày lập</th>
                                            <th>Năm học</th>
                                            <th>Mã phiếu hỏng mất</th>
                                            <th>Thiết bị</th>
                                            <th>Tình trạng hỏng hóc</th>
                                            <th>Ghi chú</th>
                                            <th>Thao tác</th>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
