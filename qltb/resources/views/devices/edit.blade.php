@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title', 'Sửa thiết bị')
@section('css')

@endsection
@section('js')
    <script type="text/javascript">
        $('#start').datepicker({
            format: "yyyy",
            autoclose: true,
            minViewMode: "years"
        }).on('changeDate', function(selected){
            startDate =  $("#start").val();
            $('#end').datepicker('setStartDate', startDate);
        }); ;

        $('#end').datepicker({
            format: "yyyy",
            autoclose: true,
            minViewMode: "years"
        });
    </script>
    <script>
        function get_info(info){
            var ma = $(info).val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}',
                }
            });

            if($(info).attr('id')=='school_id'){
                $.ajax({
                    url: "{{ route('get_info') }}",
                    dataType: 'html',
                    data: {school_id: ma},
                })
                    .done(function(data) {
                        $('#block_id').html(data);
                    });
                $('#classes_id').html('<option>--None--</option>');
            }
            if($(info).attr('id')=='block_id'){
                $.ajax({
                    url: "{{ route('get_info') }}",
                    dataType: 'html',
                    data: {block_id: ma},
                })
                    .done(function(data) {
                        $('#classes_id').html(data);
                    });
            }
        }
        $(function () {
            $.ajax({
                url: "{{ route('get_info') }}",
                dataType: 'html',
                data: {school_check: true}
            })
                .done(function(data) {
                    $('#school_id').html(data);
                });
        })
    </script>

@endsection
@section('content')
    <div class="container-fluid-width">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">Thêm mới</h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item"><a href="{{ route('admin.devices.index') }}">Quản lý thiết bị</a></li>
                        <li class="breadcrumb-item active"><a href="">Sửa thiết bị</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <form class="form form-submit" method="POST" action="{{route('admin.devices.update',$device->id)}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div id="main-content">
                    <div class="left-form">
                        <div class="card">
                            <div class="card-body">
                                <div class="col-md-12 row">
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Tên thiết bị<span class="text-danger">*</span></label>
                                            <input type="text" name="name" class="form-control form-control-line" value="{{$device->name}}">
                                        </div>
                                        <div class="form-group">
                                            <label>Mã thiết bị<span class="text-danger">*</span></label>
                                            <input type="text" name="code" class="form-control form-control-line" value="{{$device->code}}">
                                        </div>
                                        <div class="form-group">
                                            <label>Số hiệu</label>
                                            <input type="text" name="so_hieu" class="form-control form-control-line" value="{{$device['so_hieu']}}">
                                        </div>
                                        <div class="form-group">
                                            <label>Quy cách</label>
                                            <input type="text" name="quy_cach" class="form-control form-control-line" value="{{$device['quy_cach']}}">
                                        </div>
                                        @php
                                            $school_year = explode('-',$device->school_year->school_year);
                                        @endphp
                                        <div class="form-group">
                                            <label for="school_year" class="">Năm học<span class="text-danger">*</span></label>
                                            <div class="input-daterange input-group" id="date-range">
                                                <input type="text" class="form-control form-control-line" name="start" id="start" placeholder="Chọn năm bắt đầu học" value="{{$school_year[0]}}" />
                                                <input type="text" class="form-control form-control-line" name="end" id="end" placeholder="Chọn năm kết thúc" value="{{$school_year[1]}}" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="school_id" class="">Trường học<span class="text-danger">*</span></label>
                                            <select name="school_id" id="school_id" class="form-control" onchange="get_info($(this))">
                                                @foreach($dataSchool as $school)
                                                    <option value="{{$school->id}}"> {{$school->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="block_id" class="">Khối học<span class="text-danger">*</span></label>
                                            <select name="block_id" id="block_id" class="form-control" onchange="get_info($(this))">
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Khối lớp<span class="text-danger">*</span></label>
                                            <select name="classes_id" id="classes_id" class="form-control">
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Kho/phòng bộ môn<span class="text-danger">*</span></label>
                                            <select name="classroom_id" class="form-control" id="inputSltCate">
                                                <option value="0">-- Chọn phòng học --</option>
                                                @foreach ($classroom as $val)
                                                    <option value="{{ $val->id }}"> {{ $val->name }} </option>
                                                @endforeach
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Đơn vị</label>
                                            <input type="text" name="unit" class="form-control form-control-line" value="{{ $device['unit'] }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Số lượng</label>
                                            <input type="number" name="amount" class="form-control form-control-line" value="{{ $device['amount'] }}" min=0>
                                        </div>
                                        <div class="form-group">
                                            <label>Đơn giá</label>
                                            <input type="number" name="price" class="form-control form-control-line" value="{{ $device['price'] }}" min=0>
                                        </div>
                                        <div class="form-group">
                                            <label>Thành tiền</label>
                                            <input type="text" name="to_money" class="form-control form-control-line" value="{{ $device['into_money'] }}">
                                        </div>
                                        @php
                                            $time_use = explode('-',$device->time_use);
                                        @endphp
                                        <div class="form-group">
                                            <label>Ngày đưa vào SD</label>
                                            <input type="text" name="time_use" class="form-control form-control-line datepicker" placeholder="dd/mm/yyyy" value="{{$time_use[2].'/'.$time_use[1].'/'.$time_use[0]  }}">
                                        </div>
                                        <div class="form-group">
                                            <label>Năm học đưa vào SD</label>
                                            <input type="text" name="year_use" class="form-control form-control-line date-own" value="{{ $device['year_use'] }}">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Mô tả<span class="text-danger">*</span></label>
                                            <textarea  name="desciption" class="form-control form-control-line ckeditor">
                                            {{ $device['description'] }}
                                        </textarea>
                                        </div>
                                    </div>
                                </div>

                                <section id="form-footer" class="text-right">
                                    <div id="form-actions">
                                        <a href="{{ route('admin.devices.index') }}" class="btn btn-secondary mr-1">@lang('system.cancel')</a>
                                        <input type="submit" class="btn btn-info" value="@lang('system.save')">
                                    </div>
                                </section>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
