@extends('theme.master')
@section('title', 'Quản lý mượn trả thiết bị')
@section('css')
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection
@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script>
    $(function() {
        $('#listUsers').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{{ route('admin.borrow-and-return.index') }}',
            columns: [
                { data: 'time_borrow', name: 'time_borrow' },
                { data: 'time_return', name: 'time_return' },
                { data :'device_id', name: 'device_id'},
                { data: 'lesson_name', name: 'lesson_name' },
                { data: 'classes_id', name: 'classes_id' },
                { data: 'teacher', name: 'teacher' },
                { data:'status',name:'status'},
                {data:'state',name:'state'},
                { data: 'action' , name:'action'},
            ],
            language : {
                "emptyTable":     "Không có dữ liệu trong bảng",
                "info":           "Hiển thị _START_ đến _END_ của _TOTAL_ mục",
                "infoEmpty":      "Hiển thị 0 đến 0 của 0 mục",
                "infoFiltered":   " (Được lọc từ _MAX_ mục)",
                "thousands":      ",",
                "lengthMenu":     "Hiển thị &nbsp; _MENU_ &nbsp; mục",
                "loadingRecords": "Chờ xíu nhé...",
                "processing":     "Đang xử lí...",
                "search":         "Tìm kiếm:",
                "zeroRecords":    "Không tìm thấy kết quả",
                "paginate": {
                    "first":      "Đầu",
                    "last":       "Cuối",
                    "next":       "Tiếp",
                    "previous":   "Trước"
                }
            }
        });
    });
    </script>
@endsection
@section('content')
    <div id="list-crud">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">
                        Danh sách mượn trả thiết bị
                    </h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('admin.devices.index') }}">@lang('system.manage') thiết bị </a></li>
                        <li class="breadcrumb-item active"><a href="{{ route('admin.borrow-and-return.index') }}">Danh sách mượn trả thiết bị </a></li>
                    </ol>
                    <a class="btn btn-secondary d-none d-lg-block m-l-15" href="{{ route('admin.borrow-and-return.create') }}"><i class="fa fa-plus-circle"></i> Đăng kí mượn thiết bị</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div>
                                    <table class="table table-hover mt-4" id="listUsers">
                                        <thead>
                                            <th>Ngày mượn</th>
                                            <th>Ngày hẹn trả</th>
                                            <th>Tên thiết bị</th>
                                            <th>Tên bài dạy</th>
                                            <th>Dạy lớp</th>
                                            <th>Giáo viên</th>
                                            <th>Tình trạng</th>
                                            <th>Trạng thái sau khi trả</th>
                                            <th>Action</th>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
