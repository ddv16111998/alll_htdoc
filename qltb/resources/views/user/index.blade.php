@extends('theme.master')
@section('title', 'Danh sách nhân viên')
@section('js')
    <script>
        
        $(function() {
            $('#listUsers').DataTable({
                // buttons: [ 'copy', 'excel', 'pdf', 'colvis' ],
                processing: true,
                serverSide: true,
                ajax: '{!! route('admin.user.index') !!}',
                columns: [
                    { data: 'avatar', name: 'avatar',orderable: false },
                    { data: 'username', name: 'username' },
                    { data: 'acc', name: 'acc' },
                    { data: 'email', name: 'email' },
                    { data: 'role', name: 'role' },
                    { data: 'status', name: 'status' },
                    { data: 'action', name : 'action',orderable: false },
                ],
            }); 
        });
        $(document).on('click','.btn__role', function(){
            var id = $(this).attr('data-id');
            var role_id = $(this).attr('data-role');
            $('.inp_user_id').val(id);
            $('.select___role option[value="' + role_id + '"]').prop('selected', true);
        });
    </script>
@endsection
@section('content')
    <div id="list-crud">
        <div class="row page-titles">
            <div class="col-xs-6 col-md-7 align-self-center pr-0">
                <div class="d-flex flex-row-start">
                    <h4 class="card-title">
                        Danh sách nhân viên
                    </h4>
                </div>
            </div>
            <div class="col-xs-6 col-md-5 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">@lang('system.dashboard')</a></li>
                        <li class="breadcrumb-item active"><a href="">@lang('system.manage') nhân viên </a></li>
                    </ol>
                    <a class="btn btn-secondary d-none d-lg-block m-l-15" href="{{ route('admin.user.create') }}"><i class="fa fa-plus-circle"></i> @lang('system.create_new')</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <div>
                                    <table class="table table-hover mt-4 gridview" id="listUsers">
                                        <thead>
                                            <th>Ảnh đại diện</th>
                                            <th>Tên nhân viên</th>
                                            <th>Tài khoản</th>
                                            <th>Email</th>
                                            <th>Vai trò</th>
                                            <th>Trạng thái</th>
                                            <th>Action</th>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="selectRole" tabindex="-1" role="dialog" aria-labelledby="selectRoleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Gán quyền</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('admin.user.role') }}" method="POST">@csrf
                <div class="modal-body">
                    <input type="hidden" class="inp_user_id" name="user_id" value="">
                    <select name="role" class="select___role form-control">
                        @foreach ($data_role as $item)
                            <option value="{{ $item->id }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
                    <input type="submit" class="btn btn-info" value="Lưu">
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
