<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>
        CƠ SỞ DỮ LIỆU NGÀNH GIÁO DỤC VÀ ĐÀO TẠO
    </title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link id="iconwweb" rel="shortcut icon" href="&lt;%= StaticValues.ApplicationPathFull%>/img/favicon.ico" />
    <link href="/assets/css/datatables.min.css" rel="stylesheet" />
    <link href="http://csdl.moet.gov.vn/css/jquery.smartmenus.bootstrap.css" rel="stylesheet" />
    <link href="http://csdl.moet.gov.vn/css/style.css" rel="stylesheet" />
    <link href="http://csdl.moet.gov.vn/css/font-awesome.min.css" rel="stylesheet" />
    <script src="http://csdl.moet.gov.vn/js/bootstrap.min.js"></script>
</head>
<body>
<form method="post" action="./" id="form1">
    <div class="aspNetHidden">
        <input type="hidden" name="RadScriptManager1_TSM" id="RadScriptManager1_TSM" value="" />
        <input type="hidden" name="__EVENTTARGET" id="__EVENTTARGET" value="" />
        <input type="hidden" name="__EVENTARGUMENT" id="__EVENTARGUMENT" value="" />
        <input type="hidden" name="__VIEWSTATE" id="__VIEWSTATE" value="UpZwEvC/Q9mvquMEB2FbpD6iHEgME+hlYvmNlq8LRFUNAQ056qehMdxyt7ny/cOJJSuskfGNHraq56dOL/i6NJEZ4Y38BJ3s61aZy6RVekJbxQmvGqgjOVRnKdW2SInbvv84THkxwW5SLUu7liZUxa5l3r4a5CyO6AqcgiKCrMbLKq3voryCgeG3LWM3s/knXgN25XEy8SLVqDJcBREIJ0oWHayir4rAmfed10qRoMAP0HyZQdcBhiNKFJcyXMme8UzXkV6nkkrCGfrMxWaLfVC+2Bl3U7uQw3LehX6QH58jL5i1jfvUYW5DhChtLIAN+W1C8jSIBU4pxr8OmzhAC7F4Feg0LFf1CGG+MG7y7mvSfRB5KhTnTOO5hsSSnnnYyXH4pMWuD2oyYIB2b9ueIIk1Ddqfaw0CPpRjuDyBYb+vxgGhLZcHHOwKpcSrmlwMX25Zdl5mmXopkhiHU8Up3WeoTb4xW9vCVEy7O8IqPWwagUotYZwmtAHIwA+7GxOfOuov+wg7ZETG+KEl3V579voHOhUC8Q/vItQNAMg9bErc0//eqgq/+6BiVAh7ESeNmngheFsEdNNE/xbKJxKdysc5POF9o1GEMXUhH1sO3DlbsjKUdDa5UiOCAfDgxl1PcOyBMPKMk1s3ZctKjf75hwyKeeWxSYFP/IrecmzKSCOvpDuPNxxrMX7Uj9+KdmeLmiunWHUPyC/9S1MXAqgSLNRYY1xmF+xpOMHS/uSuIGGihfplaXSM0enlbQjDLwe+rXl462QMI7+JeHKbME4UQe05Fl6kyXWylNQtp4RvG30TKqDWexoPJBti2CMT59IViSj1D8ip0no4MR6Z9WKtIP32s4zVUEx11XQUnigzyqR3a6bx2aHEeZ/Dcqn/74aq03IWBMiTSZjAK3GoE26Z2fWwuQQ7KDK/zFaEepgTifbBKIwFgoluvshIHaT9n04QRB5ayDqOd1XKaEQLC5VBf6vxAMlqeo4/AGZQOcnhGgb1i3eAwBSVMwly8XHbMG8NO1sLFbxvufahEv2GUeZi09LxykeaQC2iGcl9ue4/9kFT1enTqDskt3DlxdOmFonb2PMvqCPQZuOTyT8ohXKE5pcwh1j2BhT0skOGWDA1J+qkCH/Iy15R5ivP3fIbS7M+JzZrBpLnc2X365neIJW2Xt736SJULMTrVaIrlxNuEaz0pS6hkLbKQ6bD/iX7O0ZUJqbiVd9J+LOuXF64QRG9OMAcXo2j2ZJpGEoDjXn3IHKvSX38of5JEueZodCIRJGRQVWpxxqV6zPXD9sBmljEa7Edi8SivKP8JO8a+Ogn+Vi5yNTIw/kahsVgK+8QeIz7Os2LPeBOlJ0jPluNmYVq2xLWl/YnI5MyT79Jdp5fsulFLckjn+i+fV6Xs4/Pv7CnRUTWWIwL5ynYYNule13WDcR+fdnRqsu/wNP4idbpVuLg8RLtCRQGyr44Bi2qy5GWC6Q9/wGZR4OHpCaIyjcExLS8XOh0uZR9CdtfpDJEetPi0NGgQCGMWxQ0yzcnr5qbbPMVNrhsvT5tRLvVONolVADpObP4WxwszsbOtD6A2gR5EUgjvRQtBsFMC29oAdvH+HK6EJKuFZog20ev39RBpB/BlzCf9r6NFtT/txC20tXzV1O71MGy+84wuuQuWCFETAbJlSCLVOqyPwPXc8lwVLB4HxfkSQjNtCcGpjctGhioXHTBnNLdTXjNVF/IjT1CBA3gEjp0G3/rwyYIay38C9swt+Vglu44OTQxA9RoHXdZjEL/+3vIRt9kF0ypXZFgCyOOyLsAvj04HfsUjViJyvLf47A6AkRAVPN7Z8mCl+QOZA/9ZCXLvI1NQOX+PnnKZdBZicxHeMl8WOnge9hoBw==" />
    </div>

    <div id="header-wrapper" class="header">
        <a href="http://csdl.moet.gov.vn/">
            <img class="logo" src="http://csdl.moet.gov.vn/img/logo.png" /></a>
        <div class="navi-solu">BỘ GIÁO DỤC VÀ ĐÀO TẠO</div>
        <div class="vavi-logo hidden">CƠ SỞ DỮ LIỆU NGÀNH GIÁO DỤC VÀ ĐÀO TẠO</div>

        <div class="supportMoet">
            <p class="hotline">HOTLINE: <b>19004740</b></p>
            <p class="email">EMAIL HỖ TRỢ: <b>csdl@moet.edu.vn</b></p>
        </div>
        <div class="supportMoetMobile">
            <p class="hotline">Hotline:</p>
            <span class="hotlineMobile">19004740</span>
            <p class="email">Email hỗ trợ:</p>
            <span class="hotlineMobile">csdl@moet.edu.vn</span>
        </div>
        <img class="bg-right-hea" src="http://csdl.moet.gov.vn/img/bg.png" />
    </div>
    <div id="content-wrapper" class="main" style="padding-top: 138.5px; padding-bottom: 138.5px;">
        <div class="container main-content">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12 gr-khoiquanly">
                            <div class="title-dcnt">
                                dành cho nhà trường
                            </div>
                            <div class="item-cha-dcnt">
                                <a href="{{ route('cap-truong') }}">
                                    <div class="item-dcnt-1">
                                        <div class="col-md-12 text-dcnt">Quản lý cấp trường</div>
                                        <div class="col-md-12 img-icon-dcnt">
                                            <img src="http://csdl.moet.gov.vn/img/1.png" width="48px" />
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12 gr-khoiquanly">
                            <div class="title-dcnt">
                                dành cho nhà quản lý
                            </div>
                            <div class="item-cha-dcnt">
                                <a href="{{ route('cap-phong') }}">
                                    <div class="item-dcnt-6">
                                        <div class="col-md-12 text-dcnt">Quản lý cấp Phòng GD&ĐT</div>
                                        <div class="col-md-12 img-icon-dcnt">
                                            <img src="http://csdl.moet.gov.vn/img/2.png" width="48px" alt="" />
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="item-cha-dcnt">
                                <a href="{{ route('cap-so') }}">
                                    <div class="item-dcnt-6">
                                        <div class="col-md-12 text-dcnt">Quản lý cấp Sở GD&ĐT</div>
                                        <div class="col-md-12 img-icon-dcnt">
                                            <img src="http://csdl.moet.gov.vn/img/3.png" width="48px">
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="item-cha-dcnt">
                                <a href="{{ route('cap-bo') }}">
                                    <div class="item-dcnt-6">
                                        <div class="col-md-12 text-dcnt">Quản lý cấp Bộ GD&ĐT</div>
                                        <div class="col-md-12 img-icon-dcnt">
                                            <img src="http://csdl.moet.gov.vn/img/4.png" width="48px" alt="" />
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="title-dcnt" style="display: none;">
                                Dành cho phụ huynh, học sinh
                            </div>
                            <div class="item-cha-dcnt" style="display: none;">
                                <a href="#" target="_blank">
                                    <div class="item-dcnt-9">
                                        <div class="col-md-12 text-dcnt text-dcnt-5">Tra cứu kết quả học tập</div>
                                        <div class="col-md-12 img-icon-dcnt">
                                            <img src="http://csdl.moet.gov.vn/img/9.png" width="48px" alt="" />
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <!-- 2015.1.225.45 -->
                    <div id="ContentPlaceHolder1_ctl00_RadAjaxManager1SU">
                        <span id="ctl00_ContentPlaceHolder1_ctl00_RadAjaxManager1" style="display:none;"></span>
                    </div>
                    <div id="ContentPlaceHolder1_ctl00_RadAjaxLoadingPanel1" class="RadAjax RadAjax_Bootstrap" style="display:none;">
                        <div class="raDiv">
                        </div>
                        <div class="raColor raTransp">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-7">
                            <div id="ContentPlaceHolder1_ctl00_listThongBao" class="title-dcnt">Thông báo</div>
                        </div>
                        <div class="col-md-5">
                            <div class="search-container">
                                <div class="input-group stylish-input-group">
                                    <input name="ctl00$ContentPlaceHolder1$ctl00$SearchBox1$txtSearch" type="text" id="ContentPlaceHolder1_ctl00_SearchBox1_txtSearch" class="form-control search-input" placeholder="Tìm kiếm..." onkeydown="if ((event.which &amp;&amp; event.which == 13) || (event.keyCode &amp;&amp; event.keyCode == 13)) {__doPostBack(&#39;ctl00$ContentPlaceHolder1$ctl00$SearchBox1$lnkSearch&#39;,&#39;&#39;);return false;} else return true;" />
                                    <span class="input-group-addon">
                                            <a id="ContentPlaceHolder1_ctl00_SearchBox1_lnkSearch" href="javascript:__doPostBack(&#39;ctl00$ContentPlaceHolder1$ctl00$SearchBox1$lnkSearch&#39;,&#39;&#39;)"><span class="glyphicon glyphicon-search"></span>
                                            </a>
                                            </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 gr-content-thongbao">
                            <div class="gr-thongbao">
                                <div class="RadAjaxPanel" id="ctl00_ContentPlaceHolder1_ctl00_ctl00_ContentPlaceHolder1_ctl00_lstNotifyPanel">
                                    <table id="ContentPlaceHolder1_ctl00_lstNotify" cellspacing="0" style="border-collapse:collapse;">
                                        <tr>
                                            <td>
                                                <div class="item-thongbao">
                                                    <div class="title-thongbao">
                                                        <a href='huong-dan-su-dung-csdl-nganh-gd-cac-cap-13.htm'>
                                                            Hướng dẫn sử dụng CSDL ngành GD các cấp
                                                        </a>
                                                    </div>
                                                    <div class="date-sub">
                                                        <label class="sub-tb">Tin đăng vào</label><label class="date">25/06/2019 16:05:10</label>
                                                    </div>
                                                    <div class="chuthich-tb comment">
                                                        Link truy cập tài liệu, video hướng dẫn nhập liệu, báo cáo trên CSDL ngành giáo dục cho các cấp (đăng lại link truy cập).
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="item-thongbao">
                                                    <div class="title-thongbao">
                                                        <a href='van-ban-so-2199-bgddt-vp-ngay-22-5-2019-12.htm'>
                                                            Văn bản số 2199/BGDĐT-VP ngày 22/5/2019
                                                        </a>
                                                    </div>
                                                    <div class="date-sub">
                                                        <label class="sub-tb">Tin đăng vào</label><label class="date">24/05/2019 15:27:54</label>
                                                    </div>
                                                    <div class="chuthich-tb comment">
                                                        Về việc triển khai báo cáo thống kê giáo dục kỳ cuối năm học 2018-2019
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="item-thongbao">
                                                    <div class="title-thongbao">
                                                        <a href='van-ban-so-2034-bgddt-ngcbqlgd-ngay-13-5-2019-11.htm'>
                                                            Văn bản số 2034/BGDĐT-NGCBQLGD ngày 13/5/2019
                                                        </a>
                                                    </div>
                                                    <div class="date-sub">
                                                        <label class="sub-tb">Tin đăng vào</label><label class="date">24/05/2019 15:25:58</label>
                                                    </div>
                                                    <div class="chuthich-tb comment">
                                                        Về việc rà soát, cập nhật thông tin về đội ngũ nhà giáo và cán bộ quản lý giáo dục
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div class="item-thongbao">
                                                    <div class="title-thongbao">
                                                        <a href='van-ban-so-1688-bgddt-vp-ngay-22-4-2019-9.htm'>
                                                            Văn bản số 1688/BGDĐT-VP ngày 22/4/2019
                                                        </a>
                                                    </div>
                                                    <div class="date-sub">
                                                        <label class="sub-tb">Tin đăng vào</label><label class="date">23/05/2019 09:24:05</label>
                                                    </div>
                                                    <div class="chuthich-tb comment">
                                                        Về việc đôn đốc hoàn thành báo cáo thống kê giáo dục kỳ đầu năm học 2018-2019
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="empty-record">
                                </div>
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="text-align: right;">
                                            <div style="display: table; float: right; margin-top: 6px;">
                                                <div style="display: table-cell;">
                                                    <div class="RadAjaxPanel" id="ctl00_ContentPlaceHolder1_ctl00_ctl00_ContentPlaceHolder1_ctl00_PrevPanel">
                                                        <a id="ContentPlaceHolder1_ctl00_Prev" class="aspNetDisabled btn btn-slide btn-prev"></a>
                                                    </div>
                                                </div>
                                                <div style="display: table-cell; padding-left: 4px;">
                                                    <div class="RadAjaxPanel" id="ctl00_ContentPlaceHolder1_ctl00_ctl00_ContentPlaceHolder1_ctl00_NextPanel">
                                                        <a id="ContentPlaceHolder1_ctl00_Next" class="btn btn-slide btn-next" href="javascript:__doPostBack(&#39;ctl00$ContentPlaceHolder1$ctl00$Next&#39;,&#39;&#39;)"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="footer-wrapper" class="footer">
        <div class="sticky-stopper"></div>
        <div class="f-left">
            <div class="img-vhb">
            </div>
        </div>
        <div class="f-right">
            <div class="grf1">
                <div class="title-f">CƠ SỞ DỮ LIỆU NGÀNH GIÁO DỤC VÀ ĐÀO TẠO</div>
                <div style="float: right; margin-right: 10px;">
                    Hotline <span style="color: #9f224e">19004740</span> <span class="hotro-pc">- Hỗ trợ trực tuyến <span><a href="http://www.quangich.com/home/vn/upload/info/attach/13113069258761_TeamViewer.rar" style="color: #9f224e; text-decoration: none">Teamviewer</a>
                            - <a href="http://ultraviewer.net/vi/download.html" style="color: #9f224e; text-decoration: none">Ultraviewer</a></span>
                </div>
            </div>
        </div>
    </div>
</form>
</body>
</html>
