@extends('theme.master')
@section('meta')
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
@endsection
@section('title','Nhật ký hệ thống')
@section('css')
@endsection
@section('js')
    <script>
    $(function() {
        $('#listCates').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('admin.diary.index') !!}',
            columns: [
                { data: 'user_id', name: 'user_id' },
                { data: 'ip', name: 'tp' },
                { data: 'action', name: 'action'},
                { data: 'created_at', name: 'created_at'},
            ]
        }); 
    });
    </script>
@endsection
@section('content')
	<div id="list-crud">
    <div class="row page-titles">
        <div class="col-xs-6 col-md-7 align-self-center pr-0">
            <div class="d-flex flex-row-start">
                <h4 class="card-title">Nhật ký hệ thống</h4>
            </div>
        </div>
        <div class="col-xs-6 col-md-5 align-self-center text-right">
            <div class="d-flex justify-content-end align-items-center">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="#">@lang('system.dashboard')</a></li>
                    <li class="breadcrumb-item active"><a href="{{ route('admin.diary.index') }}">Nhật ký hệ thống</a></li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form id="form-list-crud" action="/" method="POST">
                                @csrf
                                <table id="listCates"class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Thành viên</th>
                                        <th>IP</th>
                                        <th>Thao tác</th>
                                        <th>Thời gian</th>
                                    </tr>
                                    </thead>
                                </table>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
