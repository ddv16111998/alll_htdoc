<div>
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a href="{{route('admin.devices.index')}}"  class="nav-link {{Request::is('admin/devices') ? "active" : ""}}">
                <i class="fi-monitor mr-2"></i> Danh sách thiết bị
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.deviceup.index') }}" class="nav-link {{Request::is('admin/deviceup') ? "active" : ""}}">
                <i class="fi-head mr-2"></i>Tăng thiết bị
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('admin.inventory.index')}}"  class="nav-link {{Request::is('admin/inventory') ? "active" : ""}}">
                <i class="fi-mail mr-2"></i> Kiểm kê
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.broken.index') }}" class="nav-link {{Request::is('admin/broken') ? "active" : ""}}">
                <i class="fi-cog mr-2"></i> Theo dõi hỏng/mất
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('admin.liquidation.index')}}" class="nav-link {{Request::is('admin/liquidation') ? "active" : ""}}">
                <i class="fi-cog mr-2"></i> Thanh lý
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('admin.repair.index')}}" class="nav-link {{Request::is('admin/devices/repair') ? "active" : ""}}">
                <i class="fi-cog mr-2"></i> Sửa chữa
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('admin.devicedown.index') }}" class="nav-link  {{Request::is('admin/devicedown') ? "active" : ""}}">
                <i class="fi-cog mr-2"></i> Giảm thiết bị
            </a>
        </li>
    </ul>
</div>
