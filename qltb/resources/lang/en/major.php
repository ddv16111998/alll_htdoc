<?php 
return array (
  'major' => 'Major',
  'title_major_index' => 'Major manage',
  'title_major_updated' => 'Major update',
  'major_name' => 'Major name',
  'major_description' => 'Major description',
  'title_major_created' => 'Add Major',
  'guide_input_name' => 'Please enter the Major name',
  'guide_input_description' => 'Please enter a description for the Major',
  'validate_min' => 'The name must contain at least 3 characters',
  'validate_max' => 'The name can only be up to 255 characters',
  'name_unique' => 'Specialized name already exists, enter another name',
);
