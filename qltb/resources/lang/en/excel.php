<?php 
return array (
  'name' => 'Insert excel file',
  'name_1' => 'Import orders from excel (Marketing)',
  'name_2' => 'Import',
  'name_3' => 'Upload excel',
  'name_4' => 'Download excel demo',
  'name_5' => 'Download the excel file that has been adjusted to the column order according to the sample file (very important)',
  'name_6' => 'Click import to upload',
  'step_1' => 'Step 1',
  'step_2' => 'Step 2',
);
