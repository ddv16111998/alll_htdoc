<?php 
return array (
  'success' => '添加新记录成功。',
  'error' => '发生错误。 请稍后再试。',
  'update_success' => '成功更新数据。',
  'translate_success' => '翻译:name公司成功',
  'delete_success' => '成功删除数据',
  'delete_success_counted' => '成功删除了:count条记录。',
  'update_success_counted' => '成功更新了:count条记录。',
  'warning_translate' => '确保翻译所有公司以显示最准确的数据。',
  'login_failed' => ' 用户名或密码错误。',
  'reset_password_title' => '重置密码通知',
  'notification_reason' => '您收到此电子邮件是因为我们收到了您帐户的密码重置请求。',
  'reset' => '重设密码',
  'reset_expired' => '此密码重置链接将在以下时间到期:count分钟。',
  'reset_not_request' => '如果您未请求重置密码，则无需采取进一步措施。',
  'reset_hello' => '你好',
  'reset_problem_button' => '如果您在点击:button时遇到问题，请将以下网址复制并粘贴到您的网络浏览器中：:link',
  'regards' => '问候',
);
