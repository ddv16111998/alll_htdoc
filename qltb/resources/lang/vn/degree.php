<?php 
return array (
  'degree' => 'Trình độ học vấn',
  'title_degree_index' => 'Quản lý học vấn',
  'title_degree_updated' => 'Cập nhật học vấn',
  'degree_name' => 'Tên học vấn',
  'degree_description' => 'Mô tả học vấn',
  'title_degree_created' => 'Thêm mới học vấn',
  'guide_input_name' => 'Vui lòng nhập tên học vấn',
  'guide_input_description' => 'Vui lòng nhập mô tả cho học vấn',
  'validate_min' => 'Tên phải chứa ít nhất 3 ký tự',
  'validate_max' => 'Tên chỉ được tối đa 255 ký tự',
  'name_unique' => 'Tên học vấn đã tồn tại, hãy nhập tên khác',
);
