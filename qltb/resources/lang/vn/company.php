<?php 
return array (
  'name' => 'Tên công ty',
  'affiliated_company' => 'Trực thuộc công ty',
  'description' => 'Giới thiệu công ty',
  'add_company' => 'Thêm mới công ty',
  'select_company' => 'Chọn công ty',
  'nested_company' => 'Cấu trúc đa cấp',
  'warning_save' => 'Cảnh báo! Bạn có thay đổi chưa được lưu.',
  'select_language' => 'Chọn ngôn ngữ',
  'post_code' => 'Mã công ty',
  'prefix_account' => 'Tiền tố tạo tài khoản',
  'noted' => 'Lưu ý',
  'token' => 'Token',
  'change_token' => 'Thay đổi Token',
  'company_type' => 'Loại công ty',
);
