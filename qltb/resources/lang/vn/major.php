<?php 
return array (
  'major' => 'Chuyên nghành',
  'title_major_index' => 'Quản lý chuyên nghành',
  'title_major_updated' => 'Cập nhật chuyên nghành',
  'major_name' => 'Tên chuyên nghành',
  'major_description' => 'Mô tả chuyên nghành',
  'title_major_created' => 'Thêm mới chuyên nghành',
  'guide_input_name' => 'Vui lòng nhập tên chuyên nghành',
  'guide_input_description' => 'Vui lòng mô tả về chuyên nghành',
  'validate_min' => 'Tên phải chứa ít nhất 3 ký tự',
  'validate_max' => 'Tên chỉ được tối đa 255 ký tự',
  'name_unique' => 'Tên chuyên nghành đã tồn tại, hãy nhập tên khác',
);
