<?php 
return array (
  'education' => 'Nơi đào tạo',
  'title_education_index' => 'Quản lý nơi đào tạo',
  'title_education_updated' => 'Cập nhật nơi đào tạo',
  'education_name' => 'Nơi đào tạo',
  'education_description' => 'Mô tả',
  'title_education_created' => 'Thêm nơi đào tạo',
  'guide_input_name' => 'Vui lòng nhập nơi đào tạo',
  'guide_input_description' => 'Vui lòng mô tả về nơi đào tạo',
  'validate_min' => 'Tên phải chứa ít nhất 3 ký tự',
  'validate_max' => 'Tên chỉ được tối đa 255 ký tự',
);
