<?php 
return array (
  'required_custom' => 'Trường này không được để trống.',
  'required' => 'Trường này không được để trống',
  'regex' => 'Định dạng không hợp lệ',
  'min' => 'Trường này số ký tự nhỏ nhất là: ',
  'max' => 'Trường này số ký tự lớn nhất là: ',
  'unique' => 'Đã tồn tại giá trị này',
  'numeric' => 'Trường này Phải là số',
  'string' => 'Trường này Phải là chuỗi',
  'password_not_different' => 'Mật khẩu phải khác mật khẩu hiện tại',
  'not_is_create_data' => 'Bạn không có quyền thao tác với dữ liệu của nhân viên khác',
);
