<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $route_name = array();
        foreach (\Route::getRoutes()->getRoutes() as $route) {
            $action = $route->getAction();
            if(isset($action['as'])) {
                if (array_key_exists('as', $action) AND preg_match("/^(admin.)[a-z]/", $action['as']) AND !preg_match("/(.store)$/", $action['as']) AND in_array('GET', $route->methods) AND !in_array('POST', $route->methods)) {
                    if (preg_match("/(.index)$/", $action['as']) OR preg_match("/(.create)$/", $action['as']) OR preg_match("/(.remove)$/", $action['as']) OR preg_match("/(.destroy)$/", $action['as']) OR preg_match("/(.edit)$/", $action['as'])) {
                        $route_name[] = $action['as'];
                    }
                }
            }
        }
        DB::table('roles')->insert([
            'name' => 'Quản trị hệ thống',
            'permissions' => implode('|', $route_name)
        ]);
        DB::table('users')->insert(
            [
                [
                    'email' => 'develop@gmail.com',
                    'username' => 'Develop',
                    'acc' => 'develop',
                    'password' => bcrypt('123456'),
                    'status' => 1,
                    'role_id' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'email' => 'admin@gmail.com',
                    'username' => 'Admin',
                    'acc' => 'admin',
                    'password' => bcrypt('123456'),
                    'status' => 1,
                    'role_id' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'email' => 'test@gmail.com',
                    'acc' => 'Test',
                    'username' => 'test',
                    'password' => bcrypt('123456'),
                    'status' => 1,
                    'role_id' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
            ]
        );
    }
}
