<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceRepairTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_repair', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ballot')->comment('số phiếu');
            $table->date('start_day');
            $table->unsignedBigInteger('school_year_id');
            $table->foreign('school_year_id')->references('id')->on('school_years');
            $table->unsignedBigInteger('broken_id');
            $table->foreign('broken_id')->references('id')->on('brokens');
            $table->unsignedBigInteger('device_id')->nullable();
            $table->text('state')->comment('tình trạng hỏng hóc');
            $table->tinyInteger('status')->default('0')->comment('trạng thái');
            $table->longText('desciption')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });

        Schema::create('broken_repair', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('repair_id');
            $table->foreign('repair_id')->references('id')->on('device_repair');
            $table->unsignedBigInteger('device_id');
            $table->foreign('device_id')->references('id')->on('devices');
            $table->integer('amount');
            $table->string('price', 100);
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('device_repair');
    }
}
