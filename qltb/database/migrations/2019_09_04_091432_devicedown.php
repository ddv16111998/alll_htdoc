<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Devicedown extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devicedowns', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->date('create_date')->nullable();
            $table->string('year_link')->nullable();
            $table->text('note')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('device_devicedown', function (Blueprint $table) {
            $table->unsignedBigInteger('device_id');
            $table->foreign('device_id')->references('id')->on('devices');
            $table->unsignedBigInteger('devicedown_id');
            $table->foreign('devicedown_id')->references('id')->on('devicedowns');
            $table->integer('amount')->nullable();
            $table->text('note')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('devicedowns');
    }
}
