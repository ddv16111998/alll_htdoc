<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiquidationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('liquidations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->comment('số phiếu');
            $table->date('create_date');
            $table->unsignedBigInteger('school_year_id');
            $table->foreign('school_year_id')->references('id')->on('school_years');
            $table->string('council')->comment('Hội đồng thanh lý');
            $table->string('description')->comment('Nội dung');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
        Schema::create('liquidation_device', function (Blueprint $table) {
            $table->unsignedBigInteger('liquidation_id');
            $table->foreign('liquidation_id')->references('id')->on('liquidations');
            $table->unsignedBigInteger('device_id');
            $table->foreign('device_id')->references('id')->on('devices');
            $table->integer('amount');
            $table->string('note')->comment('lý do thanh toán');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('liquidations');
    }
}
