#!/usr/bin/env bash
echo 'generate autoload'
composer dump-autoload

echo 'clear cache'
php artisan cache:clear

echo 'clear config'
php artisan config:clear

echo 'clear event'
php artisan event:clear

echo 'regenerate event'
php artisan event:generate

echo 'clear route'
php artisan route:clear

echo 'clear view'
php artisan view:clear
